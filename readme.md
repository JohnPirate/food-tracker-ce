# Welcome to FoodTracker Community Edition 📊📈

> The community edition of the FoodTracker is the open source version of the FoodTracker (coming soon).

### 🏠 [Homepage](https://gitlab.com/JohnPirate/food-tracker-ce)

## Install

```bash
yarn install
```

```bash
composer install
```

## Docker

Start FoodTracker with docker-compose
```bash
docker-compose up -d
```

Composer install
```bash
sudo docker run --rm -w /app -v $PWD:/app composer:1.10 bash -c "composer install"
```

Yarn install
```bash
sudo docker run --rm -w /app -v $PWD:/app node:13-alpine ash -c "yarn install"
```

Wait for the database is ready... ~15-30 seconds
Run the database migrations
```bash
sudo docker exec food-tracker-ce_php_1 php artisan migrate --seed
```

FoodTracker: http://localhost:10080/

Mailhog: http://localhost:10081/

### Developement

Build frontend
```bash
sudo docker run --rm -w /app -v $PWD:/app node:13-alpine ash -c "yarn watch"
```

## Author

👤 **Stephan Sandriesser**

* Gitlab: [@JohnPirate](https://gitlab.com/JohnPirate) 💀🦜
* Github: [@StephanSandriesser](https://github.com/StephanSandriesser)
* LinkedIn: [@stephansandriesser](https://linkedin.com/in/stephansandriesser)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/JohnPirate/food-tracker-ce/-/issues). 

## Show your support

Give a ⭐️ if this project helped you!


