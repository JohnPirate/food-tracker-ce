const mix = require('laravel-mix');
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/vue/consume_overview_view/index.js', 'public/js/consume_overview_view.js')
    .js('resources/js/vue/dashboard_view/index.js', 'public/js/dashboard_view.js')
    .js('resources/js/vue/food_catalouge_view/index.js', 'public/js/food_catalouge_view.js')
    .js('resources/js/vue/food_view/index.js', 'public/js/food_view.js')
    .js('resources/js/vue/food_edit_view/index.js', 'public/js/food_edit_view.js')
    .js('resources/js/vue/food_new_view/index.js', 'public/js/food_new_view.js')
    .js('resources/js/vue/templates_overview_view/index.js', 'public/js/templates_overview_view.js')
    .js('resources/js/vue/template_edit_view/index.js', 'public/js/template_edit_view.js')
    .ts('resources/js/vue/template_show_view/index.ts', 'public/js/template_show_view.js')
    .js('resources/js/vue/template_consume_view/index.js', 'public/js/template_consume_view.js')
    .vue({ version: 2 })
    .extract()
    .sass('resources/sass/vendor.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css')
    .copyDirectory('resources/images', 'public/images')
    .postCss("resources/css/tw.css", "public/css", [
        require("tailwindcss"),
    ])
;

mix.version([
    'public/images/favicon.ico',
    'public/images/webicon_foodtracker.svg',
]);
mix.alias({
    '@': path.join(__dirname, 'resources/js')
});
mix.disableNotifications();
