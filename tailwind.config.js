const colors = require('tailwindcss/colors');
/** @type {import('tailwindcss').Config} */
module.exports = {
    prefix: 'tw-',
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.jsx",
        "./resources/**/*.ts",
        "./resources/**/*.vue",
    ],
    theme: {
        extend: {
            colors: {
                gray: colors.neutral,
                primary: colors.blue,
                success: colors.emerald,
                warning: colors.amber,
                danger: colors.red,
            }
        },
    },
    corePlugins: {
        preflight: false,
    },
    plugins: [],
}
