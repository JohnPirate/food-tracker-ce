# TODO's

- [x] Upgrade to PHP 8
- [ ] Upgrade to Laravel 9, 10, 11
- [ ] Migration to vue3 (in longterm...)
- [ ] Refactor Food pages
- [ ] Refactor Template pages
- [x] New internal v2 API

## 2025-02-03 refactor template pages

- [x] Element für Makro-Nährstoffe
- [ ] Nährwertetabelle
    - [ ] Logik und State im Component selbst
    - [ ] Suchfunktion
- [x] Einbinden in Template view und Template new/edit Formular
- [ ] Nicht vorhandene und nicht benutzbare (private) Templates besser handeln


## Migration guide

### SPC

`$tans` in template logic can be replaced by:
```ts
import { trans } from "@/utilities/localization";
```

The `sticky.js` mixin can be replaced by:
```vue
<script setup>
    import Sticky from '@/vue/shared/sticky.vue';
    const isMenuSticking = ref(false);
</script>

<template>
    <Sticky @stuck="isMenuSticking = $event"> ... </Sticky>
</template>

```

### Mixins

Mixins can be replaced by composables.

Example:
`@/vue/mixins/sticky.js` is replaced by `@/vue/composables/useSticky.ts`
