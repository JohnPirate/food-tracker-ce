<?php

namespace App\Manager;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class NutrientManager
 * @package App\Manager
 */
class NutrientManager
{
    /**
     * @var int
     */
    protected $seconds;

    public function __construct()
    {
        // TODO(ssandriesser): move cache minutes to config
        $this->seconds = 18000;
    }

    /**
     * @return array
     */
    public function getNutrients(): array
    {
        $sql = "SELECT
                    `unit`,
                    `group`,
                    `key`
                FROM `nutrients`
                WHERE `usable` = 1";
        return DB::select($sql);
    }

    /**
     * @return array
     */
    public function getNutrientKeys(): array
    {
        return Cache::remember('nutrient_keys', $this->seconds, function () {
            return DB::table('nutrients')->pluck('key')->toArray();
        });
    }

    /**
     * @return array
     */
    public function getNutriensMap(): array
    {
        return Cache::remember('nutrient_map', $this->seconds, function () {
            return DB::table('nutrients')->pluck('id', 'key')->toArray();
        });
    }

    /**
     * @return array
     */
    public function getNutrientUnits(): array
    {
        return Cache::remember('nutrient_units', $this->seconds, function () {
            $amounts = array_keys(config('nutrients.units.amount'));
            $energy = array_keys(config('nutrients.units.energy'));
            return array_merge($amounts, $energy);
        });
    }
}
