<?php

namespace App\Manager;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class TemplateManager
 * @package App\Manager
 */
class TemplateManager extends Manager
{
    /**
     * Check if the user is the owner of the template
     *
     * @param int $userId
     * @param int $templateId
     *
     * @return bool
     */
    public function isUserOwnerOfTemplate(int $userId, int $templateId): bool
    {
        // FIXME(ssandriesser): find a better solution
        $sql = 'SELECT
                    0 AS `o`
                FROM `users` u
                JOIN `templates` t ON (u.`id` = t.`user_id`)
                WHERE u.`id` = :attr_user_id
                AND t.`id` = :attr_template_id
                LIMIT 1
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id' => $userId,
            ':attr_template_id' => $templateId,
        ];
        $stmt->execute($attr);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $data !== false;
    }

    /**
     * Get all Templates for a specific User
     *
     * @param int $userId
     *
     * @return array
     */
    public function getTemplatesForUser(int $userId)
    {
        $sql = 'SELECT
                    `id`,
                    `name`,
                    `portion`,
                    `created_at`,
                    `updated_at`
                FROM `templates`
                WHERE `user_id` = :attr_user_id
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id' => $userId,
        ];
        $stmt->execute($attr);
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $row['portion'] = (int) $row['portion'];
            $data[] = $row;
        }
        return $data;
    }

    /**
     * Get a specific Template
     *
     * @param int $templateId
     *
     * @return array
     */
    public function getTemplate(int $templateId)
    {
        $sql = 'SELECT
                    `id`,
                    `name`,
                    `description`,
                    `portion`,
                    `created_at`,
                    `updated_at`
                FROM `templates`
                WHERE `id` = :attr_template_id
                LIMIT 1
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_template_id' => $templateId,
        ];
        $stmt->execute($attr);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if (!$data) {
            throw new NotFoundResourceException(__('messages.not_found'));
        }
        return $data;
    }

    /**
     * Store a new template
     *
     * @param int         $userId
     * @param string      $name
     * @param string|null $description
     * @param float       $portion
     *
     * @return int
     */
    public function store($userId, $name, $description, $portion)
    {
        return DB::table('templates')
            ->insertGetId([
                'user_id' => $userId,
                'name' => $name,
                'description' => $description,
                'portion' => (float) $portion,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
    }

    /**
     * Update an existing template
     *
     * @param int         $id
     * @param string      $name
     * @param string|null $description
     * @param float       $portion
     *
     * @return int
     */
    public function update($id, $name, $description, $portion)
    {
        return DB::table('templates')
            ->where('id', $id)
            ->update([
                'name' => $name,
                'description' => $description,
                'portion' => (float) $portion,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ])
        ;
    }

    /**
     * Delete an existing template
     *
     * @param int $templateId
     *
     * @return int
     */
    public function delete(int $templateId)
    {
        return DB::table('templates')
            ->where('id', $templateId)
            ->delete()
        ;
    }

    /**
     * Add a food to an existing template
     *
     * @param int   $templateId
     * @param int   $foodId
     * @param float $value
     *
     * @return int
     */
    public function addFoodToTemplate(int $templateId, int $foodId, $value): int
    {
        return DB::table('ingredients')->insertGetId([
            'template_id' => $templateId,
            'food_id' => $foodId,
            'value' => $value,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Copy an existing template
     *
     * @param int $templateId
     *
     * @return int
     * @throws \PDOException
     * @throws \Exception
     */
    public function copy(int $templateId): int
    {
        try {
            DB::beginTransaction();

            // Copy template
            $sql = 'INSERT INTO `templates` (
                        `user_id`,
                        `name`,
                        `description`,
                        `portion`,
                        `created_at`,
                        `updated_at`
                    )
                    SELECT
                        `user_id`,
                        concat(:param_prefix, `name`),
                        `description`,
                        `portion`,
                        :param_created_at,
                        :param_updated_at
                    FROM `templates`
                    WHERE `id` = :param_template_id
                    LIMIT 1
                ';
            $stmt = $this->pdo->prepare($sql);
            $params = [
                'param_template_id' => $templateId,
                'param_prefix' => __('others.copy').' - ',
                'param_created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'param_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
            $stmt->execute($params);
            $templateIdCopy = $this->pdo->lastInsertId();

            // Copy ingredients
            $sql = 'INSERT INTO `ingredients` (
                        `template_id`,
                        `food_id`,
                        `value`,
                        `created_at`,
                        `updated_at`
                    )
                    SELECT
                        :param_template_id_copy,
                        `food_id`,
                        `value`,
                        :param_created_at,
                        :param_updated_at
                    FROM `ingredients`
                    WHERE `template_id` = :param_template_id
                ';
            $stmt = $this->pdo->prepare($sql);
            $params = [
                'param_template_id' => $templateId,
                'param_template_id_copy' => $templateIdCopy,
                'param_created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'param_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
            $stmt->execute($params);

            DB::commit();

            return $templateIdCopy;

        } catch (\Exception $ex) {
            DB::rollBack();
            throw $ex;
        }
    }

    /**
     * Creates a template from a food grouping for a specific user
     *
     * @param int      $userId
     * @param int|null $groupId
     * @param string   $templateName
     * @param string   $consumedAt
     *
     * @return int
     * @throws \Exception
     */
    public function createTemplateFromFoodGrouping(int $userId, ?int $groupId, string $templateName, string $consumedAt): int
    {
        try {

            $now = Carbon::now()->format('Y-m-d H:i:s');

            DB::beginTransaction();

            $templateId = DB::table('templates')->insertGetId([
                'user_id' => $userId,
                'name' => $templateName,
                'portion' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ]);

            // Copy ingredients
            $sql = 'INSERT INTO `ingredients` (
                        `template_id`,
                        `food_id`,
                        `value`,
                        `created_at`,
                        `updated_at`
                    )
                    SELECT
                        :param_template_id,
                        `food_id`,
                        `value`,
                        :param_created_at,
                        :param_updated_at
                    FROM `consumed_foods`
                    WHERE date(`consumed_at`) = :param_consumed_at
                    AND `user_id` = :param_user_id
                ';

            $params = [
                'param_user_id' => $userId,
                'param_consumed_at' => $consumedAt,
                'param_template_id' => $templateId,
                'param_created_at' => $now,
                'param_updated_at' => $now,
            ];

            if ($groupId) {
                $sql.= 'AND `consumed_food_groups_id` = :param_consumed_food_groups_id';
                $params['param_consumed_food_groups_id'] = $groupId;
            } else {
                $sql.= 'AND `consumed_food_groups_id` IS NULL';
            }

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($params);

            DB::commit();

            return $templateId;

        } catch (\Exception $ex) {
            DB::rollBack();
            throw $ex;
        }
    }

    /**
     * Returns the last n used Templates
     *
     * @param int $count
     *
     * @return array
     */
    public function getLastUsedTemplates(int $count): array
    {
        // TODO(ssandriesser): implement feature
        return [];
    }
}
