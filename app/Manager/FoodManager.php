<?php

namespace App\Manager;

use App\Models\Food;
use App\Models\User;
use DateTime;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Arr;
use PDO;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class FoodManager
 * @package App\Manager
 */
class FoodManager extends Manager
{
    use FoodManagerTrait;

    /** @var \PDOStatement */
    public static $getSumNutrientPerDayInDateRangeStmt;

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getFoodsForUser(int $userId): array
    {
        // Get all relevant foods
        $sql = 'SELECT
                    f.`id`,
                    f.`name`,
                    f.`brand`,
                    f.`barcode`,
                    f.`private`,
                    f.`ref_value`,
                    f.`ref_unit`,
                    f.`carb_without_fiber`,
                    IF (fu.`user_id` = :attr_user_id_1, 1, 0) AS `editable`,
                    IF (fi.`id` IS NOT NULL, 1, 0) AS `imported`
                FROM `foods` f
                LEFT JOIN `food_imports` fi ON (fi.`food_id` = f.`id`)
                LEFT JOIN `food_user` fu ON (f.`id` = fu.`food_id`)
                LEFT JOIN `users` u ON (u.`id` = fu.`user_id`)
                WHERE ((
                    u.`id` != :attr_user_id_2 AND f.`private` != 1
                )
                OR u.`id` = :attr_user_id_3
                OR u.`id` IS NULL)
                AND f.`deleted_at` IS NULL
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id_1' => $userId,
            ':attr_user_id_2' => $userId,
            ':attr_user_id_3' => $userId,
        ];
        $stmt->execute($attr);
        $foods = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $row['private'] = $row['private'] == 1;
            $row['carb_without_fiber'] = $row['carb_without_fiber'] == 1;
            $row['imported'] = $row['imported'] == 1;
            $row['editable'] = $row['editable'] == 1  || ($this->isAdminUser($userId) && $row['imported']);
            $foods[$row['id']] = $row;
            $foods[$row['id']]['nutrients'] = [];
        }

        if (!count($foods)) {
            return [];
        }

        $defaultNutrientIds = config('nutrients.default_nutrients');

        // Get all related nutrients
        $sql = 'SELECT
                    fn.`food_id`,
                    fn.`value`,
                    fn.`unit`,
                    n.`group`,
                    n.`key`
                FROM `food_nutrient` fn
                JOIN `nutrients` n ON (
                        fn.`nutrient_id` = n.`id`
                    AND n.`usda_id` IN (' .implode(',', $defaultNutrientIds). ')
                )
                WHERE fn.`food_id` IN (' .implode(',', array_keys($foods)). ')
                ORDER BY NULL';

        $stmt = $this->pdo->query($sql);
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $foodId = $row['food_id'];
            unset($row['food_id']);
            $foods[$foodId]['nutrients'][] = $row;
        }

        return array_values($foods);
    }

    /**
     * @param int    $userId
     * @param string $searchTerm
     *
     * @return array
     */
    public function searchFood(int $userId, string $searchTerm): array
    {
        $searchableTerm = $searchTerm;

        // Get all relevant foods
        $sql = 'SELECT
                    f.`id`,
                    f.`name`,
                    f.`brand`,
                    f.`private`,
                    f.`ref_unit`,
                    IF (fi.`id` IS NOT NULL, 1, 0) AS `imported`
                FROM `foods` f
                LEFT JOIN `food_imports` fi ON (fi.`food_id` = f.`id`)
                LEFT JOIN `food_user` fu ON (f.`id` = fu.`food_id`)
                LEFT JOIN `users` u ON (u.`id` = fu.`user_id`)
                WHERE 1
                AND (
                    f.`name` LIKE :attr_search_term_1
                    OR f.`brand` LIKE :attr_search_term_2
                )
                AND ((
                    u.`id` != :attr_user_id_1 AND f.`private` != 1
                )
                OR u.`id` = :attr_user_id_2
                OR u.`id` IS NULL)
                AND f.`deleted_at` IS NULL
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id_1' => $userId,
            ':attr_user_id_2' => $userId,
            ':attr_search_term_1' => '%'.$searchableTerm.'%',
            ':attr_search_term_2' => '%'.$searchableTerm.'%',
        ];
        $stmt->execute($attr);
        $foods = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $row['private'] = $row['private'] === 1;
            $row['imported'] = $row['imported'] === 1;
            $foods[] = $row;
        }
        return $foods;
    }

    /**
     * @param int $id
     * @param int $userId
     *
     * @return array
     */
    public function getFoodUsingId(int $id, int $userId): array
    {
        // Get all relevant foods
        $sql = 'SELECT
                    f.*,
                    IF (fu.`user_id` = :attr_user_id_1, 1, 0) AS `editable`,
                    IF (fi.`id` IS NOT NULL, 1, 0) AS `imported`,
                    fi.`source` AS `import_source`,
                    fi.`reference` AS `import_reference`,
                    fi.`updated_at` AS `import_datetime`
                FROM `foods` f
                LEFT JOIN `food_imports` fi ON (fi.`food_id` = f.`id`)
                LEFT JOIN `food_user` fu ON (f.`id` = fu.`food_id`)
                LEFT JOIN `users` u ON (u.`id` = fu.`user_id`)
                WHERE f.`id` = :attr_food_id
                AND f.`deleted_at` IS NULL
        ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id_1' => $userId,
            ':attr_food_id' => $id,
        ];
        $stmt->execute($attr);
        $food = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();

        if (!$food) {
            throw new NotFoundResourceException(__('messages.no_food_for_id', ['id' => $id]));
        }


        $food['private'] = $food['private'] == 1;
        $food['carb_without_fiber'] = $food['carb_without_fiber'] == 1;
        $food['imported'] = $food['imported'] == 1;
        $food['editable'] = $food['editable'] == 1  || ($this->isAdminUser($userId) && $food['imported']);


        // Get all related nutrients
        $sql = 'SELECT
                    fn.`value`,
                    fn.`unit`,
                    n.`group`,
                    n.`key`
                FROM `food_nutrient` fn
                JOIN `nutrients` n ON (fn.`nutrient_id` = n.`id` AND n.`usable` = 1)
                WHERE fn.`food_id` = :attr_food_id';

        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_food_id' => $id,
        ];
        $stmt->execute($attr);
        $food['nutrients'] = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $food['nutrients'][] = $row;
        }
        return $food;
    }

    /**
     * @param array            $data
     * @param \App\Models\User $user
     * @param array            $nutrientsMap
     *
     * @return int
     */
    public function store(array $data, User $user, array $nutrientsMap): int
    {
        $food = new Food([
            'name' => $data['name'],
            'description' => $data['description'],
            'brand' => $data['brand'],
            'barcode' => $data['barcode'] ?? null,
            'private' => $data['private'],
            'ref_value' => $data['ref_value'],
            'ref_unit' => $data['ref_unit'],
            'carb_without_fiber' => $data['carb_without_fiber'],
        ]);
        $user->foods()->save($food);

        foreach ($data['nutrients'] as $nutrient) {

            $food->nutrients()->attach($nutrientsMap[$nutrient['key']], [
                'value' => $nutrient['value'],
                'unit' => $nutrient['unit'],
            ]);
        }
        $food->save();
        return $food->id;
    }

    /**
     * @param int              $id
     * @param array            $data
     * @param \App\Models\User $user
     * @param array            $nutrientsMap
     *
     * @return bool
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(int $id, array $data, User $user, array $nutrientsMap): bool
    {
        /** @var \App\Models\Food $food */
        $food = Food::where('id', $id)
            ->whereNull('deleted_at')
            ->first()
        ;
        if (!$food) {
            throw new NotFoundResourceException(__('messages.no_food_for_id', ['id' => $id]));
        }


        // TODO(ssandriesser): check if food is imported for admin user authorisation
        if (!($this->isAdminUser($user->id) || $this->isUserOwnerOfFood($user->id, $id))) {
            throw new AuthorizationException(__('messages.no_food_ownership'));
        }


        if (Arr::has($data, 'name')) {
            $food->name = $data['name'];
        }
        if (Arr::has($data, 'description')) {
            $food->description = $data['description'];
        }
        if (Arr::has($data, 'brand')) {
            $food->brand = $data['brand'];
        }
        if (Arr::has($data, 'barcode')) {
            $food->barcode = $data['barcode'];
        }
        if (Arr::has($data, 'private')) {
            $food->private = $data['private'];
        }
        if (Arr::has($data, 'ref_value')) {
            $food->ref_value = $data['ref_value'];
        }
        if (Arr::has($data, 'ref_unit')) {
            $food->ref_unit = $data['ref_unit'];
        }
        if (Arr::has($data, 'carb_without_fiber')) {
            $food->carb_without_fiber = $data['carb_without_fiber'];
        }


        if (Arr::has($data, 'nutrients')) {

            $nutrients = [];
            foreach ($data['nutrients'] as $nutrient) {
                $nutrients[$nutrientsMap[$nutrient['key']]] = [
                    'value' => $nutrient['value'],
                    'unit' => $nutrient['unit'],
                ];
            }
            $food->nutrients()->sync($nutrients);
        }
        return $food->save();
    }

    /**
     * @param int              $id
     * @param \App\Models\User $user
     *
     * @return bool
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(int $id, User $user): bool
    {
        /** @var \App\Models\Food $food */
        $food = Food::where('id', $id)
            ->whereNull('deleted_at')
            ->first()
        ;
        if (!$food) {
            throw new NotFoundResourceException(__('messages.no_food_for_id', ['id' => $id]));
        }


        // TODO(ssandriesser): check if food is imported for admin user authorisation
        if (!($this->isAdminUser($user->id) || $this->isUserOwnerOfFood($user->id, $id))) {
            throw new AuthorizationException(__('messages.no_food_ownership'));
        }
        return $food->delete();
    }

    /**
     * @return bool|\PDOStatement
     */
    protected function getGetSumNutrientPerDayInDateRangeStmt()
    {
        if (!self::$getSumNutrientPerDayInDateRangeStmt) {
            $sql = 'SELECT
                        date(cf.`consumed_at`) AS `consume_date`,
                        sum(fn.`value` / f.`ref_value` * cf.`value`) AS `res_val`
                    FROM `consumed_foods` cf
                    JOIN `food_nutrient` fn ON (fn.`food_id` = cf.`food_id`)
                    JOIN `foods` f ON (cf.`food_id` = f.`id`)
                    JOIN `nutrients` n ON (
                            n.`id` = fn.`nutrient_id`
                        AND n.`key` = :param_nutrient_key
                    )
                    WHERE 1
                    AND cf.`user_id` = :param_user_id
                    AND date(cf.`consumed_at`) >= :param_from
                    AND date(cf.`consumed_at`) <= :param_to
                    GROUP BY consume_date
                    ';
            self::$getSumNutrientPerDayInDateRangeStmt = $this->pdo->prepare($sql);
        }
        return self::$getSumNutrientPerDayInDateRangeStmt;
    }

    /**
     * @param int       $userId
     * @param string    $nutrinetKey
     * @param \DateTime $from
     * @param \DateTime $to
     *
     * @return array
     * @throws \PDOException
     */
    public function getSumNutrientPerDayInDateRange(int $userId, string $nutrinetKey, DateTime $from, DateTime $to): array
    {
        $params = [
            'param_user_id' => $userId,
            'param_nutrient_key' => $nutrinetKey,
            'param_from' => $from->format('Y-m-d'),
            'param_to' => $to->format('Y-m-d'),
        ];
        $stmt = $this->getGetSumNutrientPerDayInDateRangeStmt();
        $stmt->execute($params);
        $data = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data[$row['consume_date']] = $row['res_val'];
        }
        return $data;
    }

    /**
     * @param int $userId
     * @param int $interval
     * @param int $count
     *
     * @return array
     */
    public function getLastUsedFoods(int $userId, int $interval = 30, int $count = 12): array
    {
        // TODO(ssandriesser): should be sorted by count and consumed_at
        $sql = "SELECT
                    f.`id`,
                    f.`name`,
                    count(cf.`food_id`) AS `count`
                FROM `consumed_foods` cf
                JOIN `foods` f ON (f.`id` = cf.`food_id`)
                WHERE cf.`user_id` = :param_user_id
                AND cf.`consumed_at` > date(date_sub(CURRENT_TIMESTAMP, INTERVAL {$interval} DAY))
                GROUP BY f.`id`, f.`name`
                ORDER BY `count` DESC, f.`id` DESC
                LIMIT {$count}
                ";
        $params = [
            'param_user_id' => $userId,
        ];
        $stmt = $this->pdo()->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
