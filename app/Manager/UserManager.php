<?php

namespace App\Manager;

use App\Models\User;
use Illuminate\Support\Arr;

/**
 * Class UserManager
 * @package App\Manager
 */
class UserManager extends Manager
{
    /**
     * @param \App\Models\User $user
     * @param array            $options
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteUser(User $user, array $options): bool
    {
        try {
            // Start database transaction
            $this->pdo->beginTransaction();

            $attr = [':attr_user_id' => $user->id];


            // Remove consumed food entries
            $sql = 'DELETE FROM `consumed_foods`
                    WHERE `user_id` = :attr_user_id
                   ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($attr);


            // Remove consumed groups
            $sql = 'DELETE FROM `consumed_food_groups`
                    WHERE `user_id` = :attr_user_id
                   ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($attr);


            // Remove ingredients
            $sql = 'DELETE i
                    FROM `ingredients` i
                    JOIN `templates` t ON (t.`id` = i.`template_id`)
                    WHERE t.`user_id` = :attr_user_id
                   ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($attr);


            // Remove templates
            $sql = 'DELETE FROM `templates`
                    WHERE `user_id` = :attr_user_id
                   ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($attr);


            // Soft delete public foods
            if (!Arr::has($options, 'dont_remove_public_foods')) {
                $sql = 'UPDATE `foods` f
                        JOIN `food_user` fu ON (f.`id` = fu.food_id)
                        SET
                            f.`deleted_at` = :attr_deleted_at
                        WHERE fu.`user_id` = :attr_user_id
                        AND f.`private` = 0
                   ';
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute(Arr::collapse([
                    $attr,
                    [':attr_deleted_at' => date('Y-m-d H:i:s')],
                ]));
            }


            // Remove private foods
            $sql = 'SELECT group_concat(`id`) AS `food_ids` FROM `foods` f
                    JOIN `food_user` fu ON (f.`id` = fu.food_id)
                    WHERE `user_id` = :attr_user_id
                    AND f.`private` = 1
                   ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($attr);
            $data = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();

            if ($data['food_ids'] !== null) {
                $sql = "DELETE FROM `food_nutrient`
                        WHERE `food_id` IN ({$data['food_ids']})
                   ";
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($attr);


                $sql = 'DELETE f
                        FROM `foods` f
                        JOIN `food_user` fu ON (f.`id` = fu.food_id)
                        WHERE fu.`user_id` = :attr_user_id
                        AND f.`private` = 1
                   ';
                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($attr);
            }


            // Remove food-user relations
            $sql = 'DELETE FROM `food_user`
                    WHERE `user_id` = :attr_user_id
                   ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($attr);


            // Remove user
            $sql = 'DELETE FROM `users`
                    WHERE `id` = :attr_user_id
                   ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($attr);

            // Commit transaction
            return $this->pdo->commit();

        } catch (\Exception $ex) {
            $this->pdo->rollBack();
            throw $ex;
        }
    }

    /**
     * @param string $userId
     *
     * @return mixed
     */
    public function getUser(string $userId): User
    {
        return User::where('id', $userId)-> first();
    }

    /**
     * @param string $userId
     * @param string $name
     *
     * @return bool
     * @throws \Throwable
     */
    public function updateUserData(string $userId, string $name): bool
    {
        $user = $this->getUser($userId);
        $user->name = $name;
        return $user->saveOrFail();
    }
}
