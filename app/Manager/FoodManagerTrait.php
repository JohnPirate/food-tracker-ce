<?php


namespace App\Manager;


trait FoodManagerTrait
{
    /**
     * Check if user is owner of the food
     *
     * @param int $userId
     * @param int $foodId
     *
     * @return bool
     */
    public function isUserOwnerOfFood(int $userId, int $foodId): bool
    {
        $sql = 'SELECT
                    IF (fu.`food_id` = :attr_food_id_1, 1, 0) AS `is_owner`
                FROM `users` u
                JOIN `food_user` fu ON (u.`id` = fu.`user_id`)
                WHERE u.`id` = :attr_user_id
                AND fu.`food_id` = :attr_food_id_2
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id' => $userId,
            ':attr_food_id_1' => $foodId,
            ':attr_food_id_2' => $foodId,
        ];
        $stmt->execute($attr);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return count($data) > 0;
    }

    /**
     * Check if Food is valid
     *
     * - Food must exists
     * - Food is not soft-deleted
     * - Food is public
     * - Food is private -> User must be the owner
     *
     * @param int $foodId
     * @param int $userId
     *
     * @return bool
     */
    public function isValidFood(int $foodId, int $userId): bool
    {
        $sql = 'SELECT
                    f.id
                FROM `foods` f
                LEFT JOIN `food_user` fu ON (f.`id` = fu.`food_id`)
                LEFT JOIN `users` u ON (u.`id` = fu.`user_id`)
                WHERE f.`id` = :attr_food_id
                AND f.`deleted_at` IS NULL
                AND (
                       f.`private` = 0
                    OR f.`private` = 1 AND u.`id` = :attr_user_id
                )
                LIMIT 1
        ';
        $attr = [
            ':attr_food_id' => $foodId,
            ':attr_user_id' => $userId,
        ];
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($attr);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $data !== false;
    }
}
