<?php

namespace App\Manager\Traits;

use App\Manager\UserManager;

trait UserManagerTrait
{
    /**
     * @return \App\Manager\UserManager
     */
    public function userManager(): UserManager
    {
        return app(UserManager::class);
    }
}
