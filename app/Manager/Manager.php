<?php

namespace App\Manager;

use Illuminate\Support\Facades\DB;

/**
 * Class Manager
 * @package App\Manager
 */
class Manager
{
    /**
     * @var \PDO
     */
    protected $pdo;

    public function __construct()
    {
        $this->pdo = DB::connection()->getPdo();
    }

    /**
     * @return \PDO
     */
    public function pdo(): \PDO
    {
        return $this->pdo;
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    public function isAdminUser($userId): bool
    {
        return in_array($userId, explode(',', env('ADMIN_USERS', '')), false);
    }
}
