<?php

namespace App\Manager;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class IngredientManager
 * @package App\Manager
 */
class IngredientManager extends Manager
{
    use FoodManagerTrait;

    /**
     * Check if the user is owner of the ingredient
     *
     * @param int $userId
     * @param int $ingredientId
     *
     * @return bool
     */
    public function isUserOwnerOfIngredient(int $userId, int $ingredientId): bool
    {
        // FIXME(ssandriesser): find a better solution
        $sql = 'SELECT
                    0 AS `u`
                FROM `ingredients` i
                JOIN `templates` t ON (i.`template_id` = t.`id`)
                JOIN `users` u ON (u.`id` = t.`user_id`)
                WHERE u.`id` = :attr_user_id
                AND i.`id` = :attr_ingredient_id
                LIMIT 1
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id' => $userId,
            ':attr_ingredient_id' => $ingredientId,
        ];
        $stmt->execute($attr);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $data !== false;
    }

    /**
     * Get all Ingredients for a specific Template
     *
     * @param int  $templateId
     * @param bool $full
     *
     * @return array
     */
    public function getIngredientsForTemplate(int $templateId, bool $full = false): array
    {
        $sql = 'SELECT 
                    i.`id`,
                    i.`template_id`,
                    i.`food_id`,
                    i.`value`,
                    i.`created_at`,
                    i.`updated_at`,
                    f.`name`,
                    f.`brand`,
                    f.`private`,
                    f.`ref_value`,
                    f.`ref_unit`,
                    f.`carb_without_fiber`,
                    f.`deleted_at`,
                    IF (fi.`id` IS NOT NULL, 1, 0) AS `imported`,
                    fi.`source` AS `import_source`,
                    fi.`reference` AS `import_reference`,
                    fi.`updated_at` AS `import_datetime`
                FROM `ingredients` i
                JOIN `foods` f ON (f.`id` = i.`food_id`)
                LEFT JOIN `food_imports` fi ON (fi.`food_id` = i.`food_id`)
                WHERE i.`template_id` = :attr_template_id
                ';
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_template_id' => $templateId,
        ];
        $stmt->execute($attr);
        $data = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {


            $food = [
                'id' => $row['food_id'],
                'ingredient_id' => $row['id'],
                'name' => $row['name'],
                'brand' => $row['brand'],
                'private' => $row['private'] === 1,
                'ref_value' => $row['ref_value'],
                'ref_unit' => $row['ref_unit'],
                'carb_without_fiber' => $row['carb_without_fiber'] === 1,
                'deleted_at' => $row['deleted_at'],
                'imported' => $row['imported'] === 1,
                'import_source' => $row['import_source'],
                'import_reference' => $row['import_reference'],
                'import_datetime' => $row['import_datetime'],
                'nutrients' => [],
            ];

            $ingredient = [
                'id' => $row['id'],
                'food_id' => $row['food_id'],
                'template_id' => $row['template_id'],
                'value' => $row['value'],
                'created_at' => $row['created_at'],
                'updated_at' => $row['updated_at'],
                'food' => $food,
            ];
            $data[$food['id']] = $ingredient;
        }

        if (!count($data)) {
            return [];
        }

        if ($full) {
            $sql = 'SELECT
                    fn.`food_id`,
                    fn.`value`,
                    fn.`unit`,
                    n.`group`,
                    n.`key`
                FROM `food_nutrient` fn
                JOIN `nutrients` n ON (fn.`nutrient_id` = n.`id`)
                WHERE fn.`food_id` IN ('.implode(',', array_keys($data)).')
                ORDER BY NULL';
        } else {
            $defaultNutrientIds = config('nutrients.default_nutrients');

            // Get all related nutrients
            $sql = 'SELECT
                    fn.`food_id`,
                    fn.`value`,
                    fn.`unit`,
                    n.`group`,
                    n.`key`
                FROM `food_nutrient` fn
                JOIN `nutrients` n ON (
                        fn.`nutrient_id` = n.`id` 
                    AND n.`usda_id` IN ('.implode(',', $defaultNutrientIds).')
                )
                WHERE fn.`food_id` IN ('.implode(',', array_keys($data)).')
                ORDER BY NULL';
        }


        $stmt = $this->pdo->query($sql);
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $foodId = $row['food_id'];
            $data[$foodId]['food']['nutrients'][] = $row;
        }

        return array_values($data);
    }

    /**
     * @param int   $userId
     * @param int   $templateId
     * @param int   $foodId
     * @param float $value
     *
     * @return bool
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function storeIngredient(int $userId, int $templateId, int $foodId, float $value)
    {
        if (!$this->isValidFood($foodId, $userId)) {
            Log::warning('User is not the owner of the Food', [
                'food_id' => $foodId,
                'user_id' => $userId,
            ]);
            throw new NotFoundHttpException(__('messages.no_food_for_id', ['id' => $foodId]));
        }
        return DB::table('ingredients')->insert([
            'template_id' => $templateId,
            'food_id' => $foodId,
            'value' => $value,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    /**
     * @param int $templateId
     *
     * @return int
     */
    public function deleteIngredientsFromTemplate($templateId)
    {
        return DB::table('ingredients')
            ->where('template_id', $templateId)
            ->delete()
        ;
    }

    /**
     * @param int   $userId
     * @param int   $templateId
     * @param array $ingredients
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function syncIngredients(int $userId, int $templateId, array $ingredients): void
    {
        // Delete all ingredients from template
        $this->deleteIngredientsFromTemplate($templateId);

        // Store all ingredients
        foreach ($ingredients as $ingredient) {
            // FIXME(ssandriesser): handle deleted foods
            $this->storeIngredient($userId, $templateId, $ingredient['foodId'], (float) $ingredient['value']);
        }
    }
}
