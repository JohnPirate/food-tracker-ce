<?php

namespace App\Manager;

use App\Models\Food;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class ConsumeManager
 * @package App\Manager
 */
class ConsumeManager extends Manager
{
    /**
     * Check if user owns consumed the food
     *
     * @param int $userId
     * @param int $consumedId
     *
     * @return bool
     */
    public function ownsUserConsumedFood(int $userId, int $consumedId)
    {
        $sql = "SELECT
                    IF (cf.`food_id` = :attr_consumed_food_id_1, 1, 0) AS `has_consumed`
                FROM `users` u
                LEFT JOIN `consumed_foods` cf ON (u.`id` = cf.`id`)
                WHERE u.`id` = :attr_user_id
                AND cf.`food_id` = :attr_consumed_food_id_2
                ";
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':attr_user_id' => $userId,
            ':attr_consumed_food_id_1' => $consumedId,
            ':attr_consumed_food_id_2' => $consumedId,
        ];
        $stmt->execute($attr);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data[0]['has_consumed'] === 1;
    }

    /**
     * Check if a food exists
     *
     * @param int $foodId
     *
     * @return bool
     */
    public function foodExists($foodId) {
        return Food::where('id', '=', $foodId)
            ->whereNull('deleted_at')
            ->first() !== null
        ;
    }

    /**
     * @param int $userId
     * @param int $groupId
     *
     * @return bool
     */
    public function isUserOwnerOfGroup(int $userId, int $groupId)
    {
        $sql = "SELECT
                    IF (`user_id` = :param_user_id, 1, 0) AS `is_owner`
                FROM `consumed_food_groups`
                WHERE `id` = :param_group_id
                ";
        $stmt = $this->pdo->prepare($sql);
        $attr = [
            ':param_user_id' => $userId,
            ':param_group_id' => $groupId,
        ];
        $stmt->execute($attr);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data[0]['is_owner'] === 1;
    }

    /**
     * @param int  $userId
     * @param null $from
     * @param null $to
     *
     * @return array
     */
    public function getConsumedFoodsForUser(int $userId, $from = null, $to = null, $groupId = null): array
    {
        // Get all relevant foods
        $sql = "SELECT
                    cf.`id`,
                    cf.`value` AS `consumed_value`,
                    cf.`consumed_at`,
                    f.`id` AS `food_id`,
                    f.`name`,
                    f.`brand`,
                    f.`private`,
                    f.`ref_value`,
                    f.`ref_unit`,
                    f.`carb_without_fiber`,
                    cfg.`id` AS `group_id`,
                    cfg.`name` AS `group_name`,
                    IF (f.`deleted_at` IS NOT NULL, 1, 0) AS `deleted`,
                    IF (fi.`id` IS NOT NULL, 1, 0) AS `imported`
                FROM `foods` f
                LEFT JOIN `food_imports` fi ON (fi.`food_id` = f.`id`)
                LEFT JOIN `consumed_foods` cf ON (f.`id` = cf.`food_id`)
                LEFT JOIN `consumed_food_groups` cfg ON (cf.`consumed_food_groups_id` = cfg.`id`)
                WHERE cf.`user_id` = :attr_user_id
                ";
        $attr = [
            ':attr_user_id' => $userId,
        ];

        if ($from) {
            $sql.= "AND cf.`consumed_at` >= :attr_from".PHP_EOL;
            $attr[':attr_from'] = $from.' 00:00:00';
        }

        if ($to) {
            $sql.= "AND cf.`consumed_at` < :attr_to".PHP_EOL;
            $attr[':attr_to'] = $to.' 23:59:59';
        }

        if ($groupId) {
            $sql.= "AND cf.`consumed_food_groups_id` = :attr_group_id".PHP_EOL;
            $attr[':attr_group_id'] = $groupId;
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($attr);
        $foods = [];
        $foodIds = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $row['private'] = $row['private'] == 1;
            $row['carb_without_fiber'] = $row['carb_without_fiber'] == 1;
            $row['deleted'] = $row['deleted'] == 1;
            $row['imported'] = $row['imported'] == 1;
            $row['group'] = null;

            if ($row['group_id']) {
                $row['group'] = [];
                $row['group']['id'] = $row['group_id'];
                $row['group']['name'] = $row['group_name'];
            }

            unset($row['group_id']);
            unset($row['group_name']);

            $foods[$row['id']] = $row;
            $foods[$row['id']]['nutrients'] = [];
            $foodIds[$row['food_id']][] = $row['id'];
        }

        if (!count($foods)) {
            return [];
        }

        // Get all related nutrients
        $sql = "SELECT
                    fn.`food_id`,
                    # fn.`nutrient_id`,
                    fn.`value`,
                    fn.`unit`,
                    n.`group`,
                    n.`key`
                FROM `food_nutrient` fn
                JOIN `nutrients` n ON (fn.`nutrient_id` = n.`id` AND n.`usda_id` IN (203,204,205,208,269,291))
                WHERE fn.`food_id` IN (".implode(',', array_keys($foodIds)).")
                ORDER BY NULL";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $foodId = $row['food_id'];
            unset($row['food_id']);
            foreach ($foodIds[$foodId] as $consumeId) {
                $foods[$consumeId]['nutrients'][] = $row;
            }
        }

        return array_values($foods);
    }

    /**
     * @param int   $userId
     * @param array $data
     *
     * @return int
     */
    public function store(int $userId, array $data): int
    {
        $foodId = $data['food_id'];

        if (!$this->foodExists($foodId)) {
            throw new NotFoundResourceException(__('messages.no_food_for_id', ['id' => $foodId]));
        }

        $consumeData = [
            'user_id' => $userId,
            'food_id' => $foodId,
            'value' => $data['consumed_value'],
            'consumed_at' => Carbon::parse($data['consumed_at'])->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        if (isset($data['group']) && ($group = $data['group'])) {
            $consumeData['consumed_food_groups_id'] = $this->handleConsumedFoodGroup($userId, $group['name'], $group['id']);
        } else {
            $consumeData['consumed_food_groups_id'] = null;
        }

        return DB::table('consumed_foods')->insertGetId($consumeData);
    }

    /**
     * @param int              $id
     * @param array            $data
     * @param \App\Models\User $user
     *
     * @return bool
     * @throws \Symfony\Component\Translation\Exception\NotFoundResourceException
     */
    public function update(int $id, array $data, User $user): bool
    {
        $condition = [
            ['id', $id],
            ['user_id', $user->id],
        ];

        $consumedData = DB::table('consumed_foods')
            ->where($condition)
            ->first();

        if (!$consumedData) {
            throw new NotFoundResourceException(__('messages.no_consumed_data_for_id', ['id' => $id]));
        }

        $consumedData = [
            'value' => $data['consumed_value'],
            'consumed_at' => Carbon::parse($data['consumed_at'])->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        if (isset($data['group']) && ($group = $data['group'])) {
            $consumedData['consumed_food_groups_id'] = $this->handleConsumedFoodGroup(
                $user->id,
                $group['name'],
                $group['id']
            );
        } else {
            $consumedData['consumed_food_groups_id'] = null;
        }

        return DB::table('consumed_foods')
            ->where($condition)
            ->update($consumedData);
    }

    /**
     * @param int              $id
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function delete(int $id, User $user): bool
    {
        $condition = [
            ['id', $id],
            ['user_id', $user->id],
        ];

        $consumedData = DB::table('consumed_foods')
            ->where($condition)
            ->first();

        if (!$consumedData) {
            throw new NotFoundResourceException(__('messages.no_consumed_data_for_id', ['id' => $id]));
        }

        return DB::table('consumed_foods')
            ->where($condition)
            ->delete()
            ;
    }

    /**
     * @param int  $userId
     * @param null $from
     * @param null $to
     *
     * @return array
     */
    public function getConsumedFoodGroupsForUser(int $userId, $from = null, $to = null): array
    {
        // Get all relevant foods
        $sql = "SELECT DISTINCT
                    cfg.`id`,
                    cfg.`name`
                FROM `consumed_food_groups` cfg
                JOIN `consumed_foods` cf ON (cfg.`id` = cf.`consumed_food_groups_id`)
                WHERE 1
                AND cf.`user_id` = :attr_user_id
                ";
        $attr = [
            ':attr_user_id' => $userId,
        ];

        if ($from) {
            $sql.= "AND cf.`consumed_at` >= :attr_from".PHP_EOL;
            $attr[':attr_from'] = $from.' 00:00:00';
        }

        if ($to) {
            $sql.= "AND cf.`consumed_at` <= :attr_to".PHP_EOL;
            $attr[':attr_to'] = $to.' 23:59:59';
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($attr);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param int      $userId
     * @param string   $groupName
     * @param int|null $groupId
     *
     * @return int|null
     */
    protected function handleConsumedFoodGroup(int $userId, string $groupName, ?int $groupId): ?int
    {
        if ($groupName && !$groupId) {
            $groupId = DB::table('consumed_food_groups')
                ->insertGetId([
                    'user_id' => $userId,
                    'name' => $groupName,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
        } else {
            $groupData = DB::table('consumed_food_groups')->where([
                ['user_id', $userId],
                ['id', $groupId],
            ])->first();
            if (!$groupData) {
                throw new NotFoundResourceException(__('messages.no_group_for_id', ['id' => $groupId]));
            }
        }
        return $groupId;
    }

    /**
     * @param \App\Models\User $user
     * @param           $data
     *
     * @return bool
     * @throws NotFoundResourceException
     */
    public function copyGroupForUser(User $user, $data)
    {
        if (!$this->isUserOwnerOfGroup($user->id, $data['group_id_origin'])) {
            throw new NotFoundResourceException(__('messages.no_group_for_id', ['id' => $data['group_id_origin']]));
        }

        $groupId = null;
        if (isset($data['group']) && ($group = $data['group'])) {
            $groupId = $this->handleConsumedFoodGroup($user->id, $group['name'], $group['id']);
        }

        // Copy foods
        $sql = "INSERT INTO `consumed_foods` (
                    `value`,
                    `food_id`,
                    `user_id`,
                    `consumed_food_groups_id`,
                    `consumed_at`,
                    `created_at`,
                    `updated_at`
                )
                SELECT
                    `value`,
                    `food_id`,
                    :param_user_id AS `user_id`,
                    :param_group_id_dest AS `consumed_food_groups_id`,
                    DATE_FORMAT(:param_consumed_at_1, GET_FORMAT(DATETIME,'ISO')) AS `consumed_at`,
                    :param_created_at AS `created_at`,
                    :param_updated_at AS `updated_at`
                FROM `consumed_foods`
                WHERE `consumed_food_groups_id` = :param_group_id
                AND DATE(`consumed_at`) = DATE(:param_consumed_at)
                ";
        $params = [
            ':param_group_id' => $data['group_id_origin'],
            ':param_consumed_at' => $data['from'],
            ':param_user_id' => $user->id,
            ':param_group_id_dest' => $groupId,
            ':param_consumed_at_1' => $data['to'],
            ':param_created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ':param_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute($params);
    }

    /**
     * @param int    $userId
     * @param string $name
     *
     * @return false|array
     */
    public function getConsumedFoodGroupUsingName(int $userId, string $name)
    {
        $sql = 'SELECT
                    *
                FROM `consumed_food_groups`
                WHERE `user_id` = :param_user_id
                AND `name` = :param_name
                LIMIT 1
        ';
        $params = [
            'param_user_id' => $userId,
            'param_name' => $name,
        ];
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $data;
    }

    /**
     * @param int    $userId
     * @param string $name
     *
     * @return int
     */
    public function storeConsumedGroup(int $userId, string $name): int
    {
        return DB::table('consumed_food_groups')
            ->insertGetId([
                'user_id' => $userId,
                'name' => $name,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
    }

    /**
     * Delete a food grouping at a specific date
     *
     * @param int      $userId
     * @param int|null $groupId
     * @param string   $date
     *
     * @return void
     * @throws \PDOException
     */
    public function deleteFoodGrouping(int $userId, ?int $groupId, string $date): void
    {
        $builder = DB::table('consumed_foods')
            ->whereDate('consumed_at', $date)
            ->where('user_id', $userId)
        ;

        if ($groupId) {
            $builder->where('consumed_food_groups_id', $groupId);
        } else {
            $builder->whereNull('consumed_food_groups_id');
        }

        $builder->delete();
    }

    /**
     * @param int $userId
     *
     * @return string|null
     */
    public function getLastConsumeDateTime(int $userId): ?string
    {
        $sql = 'SELECT
                    `consumed_at`
                FROM `consumed_foods`
                WHERE `user_id` = :param_user_id
                AND `consumed_at` < :param_now
                ORDER BY `consumed_at` DESC
                LIMIT 1
        ';
        $params = [
            'param_user_id' => $userId,
            'param_now' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $data['consumed_at'] ?? null;
    }
}
