<?php

namespace App\Models;

use App\Http\Resources\TemplateResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * @property int         $id
 * @property int         $user_id
 * @property string      $name
 * @property string      $description
 * @property int         $portion
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Template extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "description",
        "portion",
    ];

    public function toResource(): TemplateResource
    {
        return new TemplateResource($this);
    }

    public function isCreator(User $user): bool
    {
        return $this->user_id === $user->id;
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function foods(): BelongsToMany
    {
        return $this->belongsToMany(Food::class, "ingredients")
            ->withPivot("id", "value")
            ->withTimestamps()
            ;
    }
}
