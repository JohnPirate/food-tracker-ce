<?php

namespace App\Models;

use App\Http\Resources\SimpleUserResource;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int         $id
 * @property string      $name
 * @property string      $email
 * @property string      $email_verified_at
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function toSimpleResource(): SimpleUserResource
    {
        return new SimpleUserResource($this);
    }

    public function isAdmin(): bool
    {
        return $this->id == env('ADMIN_USERS');
    }

    public function foods(): BelongsToMany
    {
        return $this->belongsToMany(Food::class, 'food_user');
    }

    public function consumedFoods(): BelongsToMany
    {
        return $this->belongsToMany(Food::class, 'consumed_foods');
    }

    public function templates(): HasMany
    {
        return $this->hasMany(Template::class);
    }
}
