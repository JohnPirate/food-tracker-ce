<?php

namespace App\Models;

use Illuminate\Foundation\Application;

/**
 * Class Locale
 * @package App
 */
class Locale
{
    /** @var string */
    const COOKIE_CURRENT_LOCALE = 'app_locale';

    /**
     * Cached copy of the configured supported locales
     *
     * @var array
     */
    protected static $configuredSupportedLocales = [];

    /**
     * Instance of Laravel App
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * Locale constructor.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Retrieve the currently set Locale
     *
     * @return string
     */
    public function current(): string
    {
       return $this->app->getLocale();
    }

    /**
     * Retrieve the configured fallback locale
     *
     * @return string
     */
    public function fallback(): string
    {
        return $this->app->make('config')['app.fallback_locale'];
    }

    /**
     * Set the current Locale
     *
     * @param string $locale
     *
     * @return void
     */
    public function set(string $locale): void
    {
        $this->app->setLocale($locale);
    }

    /**
     * Retrieve the current locale's directionality
     *
     * @return string
     */
    public function dir(): string
    {
        return $this->getConfiguredSupportedLocales()[$this->current()]['dir'];
    }

    /**
     * Retrieve the name of the current locale in the app's default language
     *
     * @param string $locale
     *
     * @return string
     */
    public function nameFor(string $locale): string
    {
        return $this->getConfiguredSupportedLocales()[$locale]['name'];
    }

    /**
     * Retrieve all of our app's supported locale language keys
     *
     * @return array
     */
    public function supported(): array
    {
        return array_keys($this->getConfiguredSupportedLocales());
    }

    /**
     * Determine whether a locale is supported by our app or not
     *
     * @param string $locale
     *
     * @return bool
     */
    public function isSupported(string $locale): bool
    {
        return in_array($locale, $this->supported(), true);
    }

    /**
     * Retrieve our app's supported locale's from configuration
     *
     * @return array
     */
    protected function getConfiguredSupportedLocales(): array
    {
        if (empty(static::$configuredSupportedLocales)) {
            static::$configuredSupportedLocales = $this->app->make('config')['app.supported_locales'];
        }
        return static::$configuredSupportedLocales;
    }
}
