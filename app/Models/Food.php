<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class Food
 * @package App
 *
 * @property int         $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $brand
 * @property string|null $barcode
 * @property boolean     $private
 * @property int         $ref_value
 * @property string      $ref_unit
 * @property boolean     $carb_without_fiber
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Food extends Model
{
    use SoftDeletes;

    protected $table = 'foods';

    protected $casts = ['deleted_at' => 'string'];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'brand', 'barcode', 'private',
        'ref_value', 'ref_unit', 'carb_without_fiber',
    ];

    public function nutrients(): BelongsToMany
    {
        return $this->belongsToMany(Nutrient::class)
            ->withPivot("value", "unit")
            ;
    }

    public function foodImport(): HasOne
    {
        return $this->hasOne(FoodImport::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'food_user');
    }

    public function consumers()
    {
        return $this->belongsToMany(User::class, 'consumed_foods');
    }

    public function scopeAvailableFoods(Builder $query, array $foodIds = []): void
    {
        if ($foodIds) {
            $query->whereIn('id', $foodIds);
        }
        $query->leftJoin("food_user", "foods.id", "=", "food_id")
            ->where(function ($query) {
                $query->where('private', false) // Food is public
                ->orWhere('user_id', Auth::id()); // Or owned by the current user
            });
    }

    public function importedFrom(string $source, string $reference)
    {
        DB::table('food_imports')->insert([
            'food_id' => $this->id,
            'source' => $source,
            'reference' => $reference,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
