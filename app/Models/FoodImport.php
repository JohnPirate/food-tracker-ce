<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int         $id
 * @property int         $food_id
 * @property string      $source
 * @property string      $reference
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class FoodImport extends Model
{
    use HasFactory;

    protected $fillable = [
        "food_id",
        "source",
        "reference",
    ];
}
