<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Nutrient
 * @package App
 */
class Nutrient extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function nutrients()
    {
        return $this->belongsToMany(Food::class);
    }
}
