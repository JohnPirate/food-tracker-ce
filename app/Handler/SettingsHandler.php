<?php

namespace App\Handler;

use App\Manager\Traits\UserManagerTrait;
use App\Models\User;

class SettingsHandler
{
    use UserManagerTrait;

    /**
     * @param string $userId
     *
     * @return \App\Models\User
     */
    public function getUser(string $userId): User
    {
        return $this->userManager()->getUser($userId);
    }
}
