<?php

namespace App\Handler;

use App\Manager\ConsumeManager;
use App\Manager\FoodManager;
use App\Manager\IngredientManager;
use App\Manager\NutrientManager;
use App\Manager\TemplateManager;
use App\Manager\Traits\UserManagerTrait;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ApiHandler
 * @package App\Handler
 */
class ApiHandler
{
    use UserManagerTrait;

    public function __construct()
    {
    }

    /**
     * @return \App\Manager\NutrientManager
     */
    public function nutrientManager(): NutrientManager
    {
        return app(NutrientManager::class);
    }

    /**
     * @return \App\Manager\FoodManager
     */
    public function foodManager(): FoodManager
    {
        return app(FoodManager::class);
    }

    /**
     * @return \App\Manager\ConsumeManager
     */
    public function consumeManager(): ConsumeManager
    {
        return app(ConsumeManager::class);
    }

    /**
     * @return \App\Manager\TemplateManager
     */
    public function templateManager(): TemplateManager
    {
        return app(TemplateManager::class);
    }

    /**
     * @return \App\Manager\IngredientManager
     */
    public function ingredientManager(): IngredientManager
    {
        return app(IngredientManager::class);
    }

    /**
     * @return \App\Models\User
     */
    public function authUser(): User
    {
        return Auth::user();
    }

    /**
     * @param array $data
     *
     * @return int
     */
    public function storeFood(array $data): int
    {
        $nutrientsMap = $this->nutrientManager()->getNutriensMap();
        return $this->foodManager()->store($data, $this->authUser(), $nutrientsMap);
    }

    /**
     * @param int   $id
     * @param array $data
     *
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateFood(int $id, array $data): void
    {
        $nutrientsMap = $this->nutrientManager()->getNutriensMap();
        $this->foodManager()->update($id, $data, $this->authUser(), $nutrientsMap);
    }

    /**
     * @param array $data
     *
     * @return int
     */
    public function storeConsumedFood(array $data)
    {
        $id = $this->consumeManager()->store(Auth::id(), $data);
        // TODO(ssandriesser): return consumed food
        return $id;
    }

    /**
     * @param int   $id
     * @param array $data
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function updateConsumedFood(int $id, array $data)
    {
        $this->consumeManager()->update($id, $data, $this->authUser());
        // TODO(ssandriesser): return consumed food
        return [];
    }

    /**
     * @param int $id
     */
    public function deleteConsumedFood(int $id): void
    {
        $this->consumeManager()->delete($id, $this->authUser());
    }

    /**
     * Get a specific Template for an specific User
     *
     * @param int  $userId
     * @param int  $templateId
     * @param bool $full
     *
     * @return array
     */
    public function getTemplateForUser(int $userId, int $templateId, bool $full = false): array
    {
        $this->isUserOwnerOfTemplate($userId, $templateId);

        $template = $this->templateManager()->getTemplate($templateId);
        $template['ingredients'] = $this->ingredientManager()->getIngredientsForTemplate($template['id'], $full);
        return $template;
    }

    /**
     * @param int   $userId
     * @param array $data
     *
     * @return int
     * @throws \Exception
     */
    public function storeTemplateForUser(int $userId, array $data): ?int
    {
        try {
            DB::beginTransaction();

            $templateId = $this->templateManager()->store(
                $userId,
                $data['name'],
                $data['description'],
                $data['portion']
            );

            $this->ingredientManager()->syncIngredients($userId, $templateId, $data['ingredients']);

            DB::commit();

            return $templateId;

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::warning(__METHOD__ . ': rollback');
            throw $ex;
        }
    }

    /**
     * @param int   $userId
     * @param array $data
     *
     * @throws \Exception
     */
    public function updateTemplateForUser(int $userId, array $data)
    {
        $templateId = $data['id'];
        $this->isUserOwnerOfTemplate($userId, $templateId);
        try {
            DB::beginTransaction();
            $this->templateManager()->update(
                $data['id'],
                $data['name'],
                $data['description'],
                $data['portion']
            );
            $this->ingredientManager()->syncIngredients($userId, $templateId, $data['ingredients']);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::warning(__METHOD__ . ': rollback');
            throw $ex;
        }
    }

    /**
     * @param int $userId
     * @param int $templateId
     *
     * @return void
     * @throws \Exception
     */
    public function deleteTemplateFromUser(int $userId, int $templateId): void
    {
        $this->isUserOwnerOfTemplate($userId, $templateId);

        try {
            DB::beginTransaction();
            $this->templateManager()->delete($templateId);
            $this->ingredientManager()->deleteIngredientsFromTemplate($templateId);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::warning(__METHOD__ . ': rollback');
            throw $ex;
        }
    }

    /**
     * @param int   $userId
     * @param int   $templateId
     * @param array $data
     *
     * @return void
     * @throws \Exception
     */
    public function consumeTemplateForUser(int $userId, int $templateId, array $data): void
    {
        $this->isUserOwnerOfTemplate($userId, $templateId);

        $template = $this->templateManager()->getTemplate($templateId);

        $groupName = $data['group_name'] ?? $template['name'];
        $group = $this->consumeManager()->getConsumedFoodGroupUsingName($userId, $groupName);
        if (!$group) {
            $group = [
                'name' => $groupName,
                'id' => $this->consumeManager()->storeConsumedGroup($userId, $groupName),
            ];
        }
        try {
            DB::beginTransaction();
            foreach ($data['ingredients'] as $ingredient) {

                $ingredientData = [
                    'food_id' => $ingredient['foodId'],
                    'consumed_value' => $ingredient['value'],
                    'group' => $group,
                    'consumed_at' => $data['consumed_at'],
                ];
                $this->consumeManager()->store($userId, $ingredientData);
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::warning(__METHOD__ . ': rollback');
            throw $ex;
        }
    }

    /**
     * @param int   $userId
     * @param int   $templateId
     * @param array $data
     *
     * @return int
     */
    public function addFoodToTemplate(int $userId, int $templateId, array $data): int
    {
        $this->isUserOwnerOfTemplate($userId, $templateId);
        return $this->templateManager()->addFoodToTemplate($templateId, $data['food_id'], $data['value']);
    }

    /**
     * Checks if the user is the owner of an existing template
     *
     * @param int $userId
     * @param int $templateId
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function isUserOwnerOfTemplate(int $userId, int $templateId): void
    {
        if (!$this->templateManager()->isUserOwnerOfTemplate($userId, $templateId)) {
            Log::warning('User is not the owner of the Template', [
                'template_id' => $templateId,
                'user_id' => $userId,
            ]);
            throw new NotFoundHttpException(__('messages.not_found'));
        }
    }

    /**
     * Checks if the user is the owner of an existing food group
     *
     * @param int $userId
     * @param int $groupId
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function isUserOwnerOfFoodGroup(int $userId, int $groupId): void
    {
        if (!$this->consumeManager()->isUserOwnerOfGroup($userId, $groupId)) {
            Log::warning('User is not the owner of the Group', [
                'group_id' => $groupId,
                'user_id' => $userId,
            ]);
            throw new NotFoundHttpException(__('messages.not_found'));
        }
    }

    /**
     * Copy a user specific template
     *
     * @param int $userId
     * @param int $templateId
     *
     * @return int
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \PDOException
     * @throws \Exception
     */
    public function copyTemplate(int $userId, int $templateId): int
    {
        $this->isUserOwnerOfTemplate($userId, $templateId);
        return $this->templateManager()->copy($templateId);
    }

    /**
     * Delete a food grouping at a specific date
     *
     * @param int      $userId
     * @param int|null $groupId
     * @param string   $date
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \PDOException
     */
    public function deleteFoodGrouping(int $userId, ?int $groupId, string $date): void
    {
        if ($groupId) {
            $this->isUserOwnerOfFoodGroup($userId, $groupId);
        }
        $this->consumeManager()->deleteFoodGrouping($userId, $groupId, $date);
    }

    /**
     * Creates a template from a food grouping for a specific user
     *
     * @param int      $userId
     * @param int|null $groupId
     * @param string   $templateName
     * @param string   $consumedAt
     *
     * @return int
     * @throws \Exception
     */
    public function createTemplateFromFoodGrouping(int $userId, ?int $groupId, string $templateName, string $consumedAt): int
    {
        if ($groupId) {
            $this->isUserOwnerOfFoodGroup($userId, $groupId);
        }
        return $this->templateManager()->createTemplateFromFoodGrouping($userId, $groupId, $templateName, $consumedAt);
    }

    /**
     * @param int $userId
     *
     * @return array
     * @throws \Exception
     */
    public function getUserData(int $userId): array
    {
        $now = new DateTime();
        $nowDateTime = $now->format('Y-m-d');
        $from = (clone $now)->sub(new \DateInterval('P14D'));

        $lastMeal = $this->consumeManager()->getLastConsumeDateTime($userId);

        $lastUsedTemplates = $this->templateManager()->getLastUsedTemplates(5);

        $lastUsedFoods = $this->foodManager()->getLastUsedFoods(Auth::id());

        $user = [
            'last_meal' => $lastMeal,
            'consumed' => [],
            'food' => [
                'last_used' => $lastUsedFoods,
            ],
            'templates' => [
                'last_used' => $lastUsedTemplates,
            ],
        ];

        $nutrients = [
            'energy',
            'carbohydrate_by_difference',
            'total_lipid_fat',
            'protein',
            'fiber_total_dietary',
            'sugars_total',
            'salt',
            'caffeine',
        ];

        foreach ($nutrients as $nutrient) {
            $data = $this->foodManager()->getSumNutrientPerDayInDateRange($userId, $nutrient, $from, $now);
            $consumedToday = $data[$nowDateTime] ?? 0;
            $consumed = [
                'consumed' => $consumedToday,
                'last_days' => $data,
            ];
            $user['consumed'][$nutrient] = $consumed;
        }

        return $user;
    }
}
