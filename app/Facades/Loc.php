<?php

namespace App\Facades;

use App\Models\Locale;
use Illuminate\Support\Facades\Facade;

/**
 * Class Loc
 * @package App\Facades
 *
 * @method static string current()
 * @method static string fallback()
 * @method static void set(string $locale)
 * @method static string dir()
 * @method static string nameFor(string $locale)
 * @method static array supported()
 * @method static bool isSupported(string $locale)
 */
class Loc extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Locale::class;
    }
}
