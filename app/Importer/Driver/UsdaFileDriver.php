<?php

namespace App\Importer\Driver;

use App\Models\Food;

/**
 * Class UsdaFileDriver
 * @package App\Importer\Driver
 */
class UsdaFileDriver implements DriverInterface
{
    /**
     * @param string $reference
     *
     * @return \App\Models\Food
     */
    public function import(string $reference): Food
    {
        $file = base_path("database/seeds/usda_files/{$reference}.json");

        $content = json_decode(file_get_contents($file), true);

        // TODO(ssandriesser): do better error handling
        //                     - [ ] check if content is available
        //                     - [ ] check if food is found with reference

        $ref = $content['foods'][0]['food'];
        $food = new Food();
        $food->name = $ref['desc']['name'];
        $food->ref_value = 100;
        $food->ref_unit = $ref['desc']['ru'];
        $food->carb_without_fiber = 1;
        $food->save();

        foreach ($ref['nutrients'] as $nutrientRef) {

            $nutrient = \App\Models\Nutrient::where('usda_id', $nutrientRef['nutrient_id'])->first();

            if (!$nutrient) {
                continue;
            }

            // TODO(ssandriesser): do better error handling
            //                     - [ ] check if nutrient has valid unit

            $food->nutrients()->attach($nutrient->id, [
                'value' => $nutrientRef['value'],
                'unit' => $nutrientRef['unit'],
            ]);
        }

        $food->save();

        $food->importedFrom('usda', $reference);

        return $food;
    }
}
