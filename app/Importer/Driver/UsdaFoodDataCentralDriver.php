<?php

namespace App\Importer\Driver;

use App\Models\Food;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class UsdaFoodDataCentralDriver
 * @package App\Importer\Driver
 */
class UsdaFoodDataCentralDriver implements DriverInterface
{
    /**
     * @param string $reference
     *
     * @return \App\Models\Food
     * @throws \Exception
     */
    public function import(string $reference): Food
    {
        $url = "https://api.nal.usda.gov/fdc/v1/{$reference}?".http_build_query([
            'api_key' => env('USDA_KEY', 'DEMO_KEY'),
        ]);

        $ref = json_decode(file_get_contents($url), true);

        // TODO(ssandriesser): do better error handling
        //                     - [ ] check if content is available
        //                     - [ ] check if food is found with reference

        try {

            DB::beginTransaction();

            $food = new Food();
            $food->name = $ref['description'];
            $food->ref_value = 100;
            $food->ref_unit = 'g';
            $food->carb_without_fiber = 0;
            $food->save();

            $amount = array_keys(config('nutrients.units.amount', ['g']));
            $energy = ['kcal'];
            $units = array_merge($amount, $energy);

            $group = 'others';

            foreach ($ref['foodNutrients'] as $nutrientRef) {

                if (count($nutrientRef) === 2) {
                    $group = Str::snake(Str::slug($nutrientRef['nutrient']['name'], '_'));
                    continue;
                }

                // Skipp Units
                if (!in_array($nutrientRef['nutrient']['unitName'], $units, false)) {
                    continue;
                }

                $nutrient = \App\Models\Nutrient::where('usda_id', $nutrientRef['nutrient']['number'])->first();

                if (!$nutrient) {
                    $nutrientId = DB::table('nutrients')->insertGetId([
                        'usda_id' => $nutrientRef['nutrient']['number'],
                        'unit' => $nutrientRef['nutrient']['unitName'],
                        'group' => $group,
                        'key' => Str::snake(Str::slug($nutrientRef['nutrient']['name'], '_')),
                        'usable' => 0,
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);
                } else {
                    $nutrientId = $nutrient->id;
                }

                // TODO(ssandriesser): do better error handling
                //                     - [ ] check if nutrient has valid unit

                $food->nutrients()->attach($nutrientId, [
                    'value' => (float)$nutrientRef['amount'],
                    'unit' => $nutrientRef['nutrient']['unitName'],
                ]);
            }

            $food->save();

            $food->importedFrom('usda_food_data_central', $reference);

            DB::commit();

        } catch (\Exception $ex) {
            DB::rollBack();
            throw $ex;
        }

        return $food;
    }
}
