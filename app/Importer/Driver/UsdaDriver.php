<?php

namespace App\Importer\Driver;

use App\Models\Food;

/**
 * Class UsdaDriver
 * @package App\Importer\Driver
 */
class UsdaDriver implements DriverInterface
{
    /**
     * @param string $reference
     *
     * @return \App\Models\Food
     */
    public function import(string $reference): Food
    {
        $url = "https://api.nal.usda.gov/ndb/V2/reports?".http_build_query([
            'ndbno' => $reference,
            'type' => 'f',
            'format' => 'json',
            'api_key' => env('USDA_KEY', 'DEMO_KEY'),
        ]);

        $content = json_decode(file_get_contents($url), true);

        // TODO(ssandriesser): do better error handling
        //                     - [ ] check if content is available
        //                     - [ ] check if food is found with reference

        $ref = $content['foods'][0]['food'];
        $food = new Food();
        $food->name = $ref['desc']['name'];
        $food->ref_value = 100;
        $food->ref_unit = $ref['desc']['ru'];
        $food->carb_without_fiber = 0;
        $food->save();

        foreach ($ref['nutrients'] as $nutrientRef) {

            $nutrient = \App\Models\Nutrient::where('usda_id', $nutrientRef['nutrient_id'])->first();

            if (!$nutrient) {
                continue;
            }

            // TODO(ssandriesser): do better error handling
            //                     - [ ] check if nutrient has valid unit

            $food->nutrients()->attach($nutrient->id, [
                'value' => $nutrientRef['value'],
                'unit' => $nutrientRef['unit'],
            ]);
        }

        $food->save();

        $food->importedFrom('usda', $reference);

        return $food;
    }
}
