<?php

namespace App\Importer\Driver;

/**
 * Interface DriverInterface
 * @package App\Importer\Driver
 */
interface DriverInterface
{
    /**
     * @param string $reference
     *
     * @return \App\Models\Food
     */
    public function import(string $reference): \App\Models\Food;
}
