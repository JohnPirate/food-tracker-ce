<?php

namespace App\Importer;

use App\Importer\Driver\DriverInterface;
use App\Importer\Driver\UsdaDriver;
use App\Importer\Driver\UsdaFileDriver;
use App\Importer\Driver\UsdaFoodDataCentralDriver;
use App\Models\Food;

/**
 * Class FoodImporter
 * @package App\Importer
 */
class FoodImporter
{
    /**
     * FoodImporter constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $source
     * @param string $reference
     *
     * @return \App\Models\Food
     */
    public function from(string $source, string $reference): Food
    {
        $driver = $this->driver($source);
        return $driver->import($reference);
    }

    /**
     * @param string $name
     *
     * @return \App\Importer\Driver\DriverInterface
     */
    protected function driver(string $name): DriverInterface
    {
        switch ($name) {
            case 'usda':
                return new UsdaDriver();
            case 'usda_food_data_central':
                return new UsdaFoodDataCentralDriver();
            case 'usda_file':
                return new UsdaFileDriver();
            default:
                throw new \LogicException("Import driver {$name} not found.");
        }
    }
}
