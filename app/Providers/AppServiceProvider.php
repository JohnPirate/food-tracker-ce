<?php

namespace App\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // NOTE(ssandriesser): this piece of code allows you to use @verified and @endverified in blade templates
        Blade::if('verified', function () {
            return Auth::check() && Auth::user()->hasVerifiedEmail();
        });

        Blade::if('isactiveroute', function ($routeNames) {
            return in_array(Route::currentRouteName(), $routeNames, false);
        });

        JsonResource::withoutWrapping();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
