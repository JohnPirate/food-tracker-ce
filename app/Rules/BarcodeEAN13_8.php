<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BarcodeEAN13_8 implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // EAN Barcode https://de.wikipedia.org/wiki/European_Article_Number
        return strlen($value) === 13 || strlen($value) === 8;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.barcode.ean13_8');
    }
}
