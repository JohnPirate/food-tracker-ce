<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (NotFoundHttpException $e, $request) {
            if ($request->is('api/*')) {
                Log::error($e->getMessage(), [
                    'exception_type' => NotFoundHttpException::class,
                ]);
                return response()->json([
                    'message' => $e->getMessage() ?: __('messages.not_found'),
                ], 404);
            }
        });

        $this->renderable(function (AuthenticationException $e, $request) {
            if ($request->is('api/*')) {
                Log::error($e->getMessage(), [
                    'exception_type' => AuthenticationException::class,
                ]);
                return response()->json([
                    'message' => __('messages.not_authenticated'),
                ], 401);
            }
        });

        $this->renderable(function (AuthorizationException $e, $request) {
            if ($request->is('api/*')) {
                Log::error($e->getMessage(), [
                    'exception_type' => AuthorizationException::class,
                ]);
                return response()->json([
                    'message' => __('messages.not_authorized'),
                ], 401);
            }
        });

        // Fallback
//        $this->renderable(function (Throwable $e, $request) {
//            if ($e instanceof ValidationException) {
//                throw $e;
//            }
//            if ($request->is('api/*')) {
//                return response()->json([
//                    'message' => __('messages.internal_error'),
//                ], 500);
//            }
//        });
    }
}
