<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FoodNutrientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "value" => $this->pivot->value,
            "unit" => $this->pivot->unit,
            "group" => $this->group,
            "key" => $this->key,
            "display" => [
                "name" => __("nutrients.".$this->key),
                "group" => __("nutrients.groups.".$this->group),
            ],
        ];
    }
}
