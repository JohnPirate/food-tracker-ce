<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FoodSearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $import = null;
        if ($this->foodImport) {
            $import =[
                "id" => $this->foodImport->id,
                "source" => $this->foodImport->source,
                "reference" => $this->foodImport->reference,
            ];
        }

        return [
            "id" => $this->id,
            "href" => route("food_show", [ "id" => $this->id ]),
            "name" => $this->name,
            "brand" => $this->brand,
            "ref_value" => $this->ref_value,
            "ref_unit" => $this->ref_unit,
            "carb_without_fiber" => $this->carb_without_fiber === 1 ? true : false,
            "is_imported" => (bool)$import,
            "import" => $import,
            "nutrients" => FoodNutrientResource::collection($this->nutrients()->whereIn("key", [ "energy", "carbohydrate_by_difference", "total_lipid_fat", "protein" ])->get()),
        ];
    }
}
