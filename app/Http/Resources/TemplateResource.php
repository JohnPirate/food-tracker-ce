<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TemplateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "href" => route("template_view", $this->id),
            "name" => $this->name,
            "description" => $this->description,
            "portion" => $this->portion,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "creator" => $this->creator->toSimpleResource(),
            "foods" => TemplateFoodResource::collection($this->foods()
                ->select([
                    "foods.id",
                    "name",
                    "brand",
                    "ref_value",
                    "ref_unit",
                    "carb_without_fiber",
                    "deleted_at",
                ])
                ->with([ "nutrients" , "foodImport" ])
                ->get()
            ),
        ];
    }
}
