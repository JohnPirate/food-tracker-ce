<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConsumeTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dateFormat = config('config.consume_datetime_format');

        return [
            'group_name' => 'string|between:1,255|nullable',
            'consumed_at' => "required|date|date_format:{$dateFormat}",
            'ingredients' => 'required|array|between:1,50',
            'ingredients.*.value' => 'required|numeric|between:0,10000',
            'ingredients.*.foodId' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'ingredients.*.value.between' => __('validation.ingredient_amount_range'),
        ];
    }
}
