<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteFoodGroupingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dateFormat = config('config.date_format');
        return [
            'group_id' => 'nullable|int',
            'consumed_at' => "required|date|date_format:{$dateFormat}",
        ];
    }
}
