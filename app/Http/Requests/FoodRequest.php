<?php

namespace App\Http\Requests;

use App\Rules\BarcodeEAN13_8;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class FoodRequest
 * @package App\Http\Requests
 */
class FoodRequest extends FormRequest
{
    /**
     * List of all nutrient keys
     *
     * @var array
     */
    protected $nutrients;

    /**
     * List of all valid units
     *
     * @var array
     */
    protected $units;

    /**
     * FoodRequest constructor.
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null  $content
     */
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->init();
    }

    protected function init()
    {
        $this->nutrients = app(\App\Manager\NutrientManager::class)->getNutrientKeys();
        $this->units = app(\App\Manager\NutrientManager::class)->getNutrientUnits();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // TODO(ssandriesser): move 'ref_value' 'between' rule to config
        //                     move 'ref_unit' 'in' rule to config
        return [
            'name' => 'required|string|between:1,255',
            'description' => 'nullable|string',
            'brand' => 'nullable|string|between:1,255',
            'barcode' => ['nullable', 'string', new BarcodeEAN13_8],
            'private' => 'boolean',
            'ref_value' => 'required|numeric|between:0,10000',
            'ref_unit' => 'required|in:g,ml',
            'carb_without_fiber' => 'required|boolean',
            'nutrients' => 'required|array|min:1',
            'nutrients.*.key' => [
                'required',
                'string',
                'distinct',
                Rule::in($this->nutrients)
            ],
            'nutrients.*.unit' => [
                'required',
                'string',
                Rule::in($this->units)
            ],
            'nutrients.*.value' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            // TODO(ssandriesser): special validation message for between
            'nutrients.required' => __('You are not able to create a food without nutrients.'),
        ];
    }
}
