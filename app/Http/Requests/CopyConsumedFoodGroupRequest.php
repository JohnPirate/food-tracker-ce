<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CopyConsumedFoodGroupRequest
 * @package App\Http\Requests
 */
class CopyConsumedFoodGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dateFormat = config('config.consume_datetime_format');

        return [
            'group' => 'nullable',
            'group.id' => 'nullable|int',
            'group.name' => 'string|max:255',
            'group_id_origin' => 'required|int',
            'from' => "required|date|date_format:{$dateFormat}",
            'to' => 'required|date',
        ];
    }
}
