<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFoodConsumeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'consumed_value' => 'required|numeric|between:0,10000',
            'group' => 'nullable',
            'group.id' => 'nullable|int',
            'group.name' => 'string|max:255',
        ];
    }

    public function messages()
    {
        return [
            'consumed_value.between' => __('validation.ingredient_amount_range')
        ];
    }
}
