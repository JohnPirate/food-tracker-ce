<?php

namespace App\Http\Controllers;

use App\Handler\ApiHandler;
use App\Http\Requests\FoodRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FoodController extends Controller
{
    protected $apiHandler;

    public function __construct(
        ApiHandler $apiHandler
    ) {
        $this->apiHandler = $apiHandler;
    }

    public function catalogue(Request $request)
    {
        return view('food.catalogue');
    }

    public function newData(Request $request)
    {
        $templateData = [];
        return view('food.new', $templateData);
    }

    public function show(Request $request, $id)
    {
        $templateData = [
            'foodId' => $id,
        ];
        return view('food.show', $templateData);
    }

    public function edit(Request $request, $id)
    {
        $templateData = [
            'foodId' => $id,
        ];
        return view('food.edit', $templateData);
    }

    public function listData(Request $request)
    {
        try {

            $data = $this->apiHandler->foodManager()->getFoodsForUser(Auth::id());
            return response()->json($data);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\FoodRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FoodRequest $request)
    {
        try {

            $requestAll = $request->all();

            Log::info('The user is trying to store a new food', [
                'request_data' => $requestAll,
                'user_id' => Auth::id(),
            ]);

            // TODO(ssandriesser): validate if "must have" nutrients are available


            $foodId = $this->apiHandler->storeFood($request->all());
            $data = $this->apiHandler->foodManager()->getFoodUsingId($foodId, Auth::id());

            Log::info('Storing the new food successful', [
                'food_id' => $foodId,
            ]);

            return response()->json([
                'message' => __('messages.success_store_data', ['data' => $data['name']]),
                'data' => $data,
            ]);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request, $id)
    {
        try {

            $data = $this->apiHandler->foodManager()->getFoodUsingId($id, Auth::id());
            return response()->json($data);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\FoodRequest $request
     * @param int                            $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(FoodRequest $request, $id)
    {
        try {

            $requestAll = $request->all();

            Log::info('The user is trying to update the food', [
                'request_data' => $requestAll,
                'user_id' => Auth::id(),
                'food_id' => $id,
            ]);

            // TODO(ssandriesser): validate if "must have" nutrients are available

            $this->apiHandler->updateFood($id, $requestAll);
            $data = $this->apiHandler->foodManager()->getFoodUsingId($id, Auth::id());

            Log::info('The user updated the food');

            return response()->json([
                'message' => __('messages.success_update_data', ['data' => $data['name']]),
                'data' => $data,
            ]);
        } catch (\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $id)
    {
        try {

            Log::info('The user is trying to delete the food', [
                'user_id' => Auth::id(),
                'food_id' => $id,
            ]);

            // TODO(ssandriesser): Has user permission to delete the food?

            $data = $this->apiHandler->foodManager()->getFoodUsingId($id, Auth::id());
            $this->apiHandler->foodManager()->delete($id, Auth::user());

            Log::info("The user deleted the food");

            return response()->json([
                'message' => __('messages.success_deleted_data', ['data' => $data['name']]),
            ]);
        } catch (\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        try {

            $searchTerm = $request->get('q');

            if (!$searchTerm) {
                return response()->json();
            }

            $data = $this->apiHandler->foodManager()->searchFood(Auth::id(), $searchTerm);

            return response()->json($data);
        } catch (\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }
}
