<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Handler\ApiHandler;

/**
 * Class NutrientController
 * @package App\Http\Controllers
 */
class NutrientController extends Controller
{
    /**
     * @var \App\Handler\ApiHandler
     */
    protected $apiHandler;

    /**
     * NutrientController constructor.
     *
     * @param \App\Handler\ApiHandler $apiHandler
     */
    public function __construct(
        ApiHandler $apiHandler
    ) {
        $this->apiHandler = $apiHandler;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listData()
    {
        try {

            $data = $this->apiHandler->nutrientManager()->getNutrients();
            return response()->json($data);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listNutrientUnits()
    {
        try {

            $data = $this->apiHandler->nutrientManager()->getNutrientUnits();
            return response()->json($data);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }
}
