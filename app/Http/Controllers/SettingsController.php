<?php

namespace App\Http\Controllers;

use App\Handler\ApiHandler;
use App\Handler\SettingsHandler;
use App\Http\Requests\SettingsUpdateUserDataRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class SettingsController
 * @package App\Http\Controllers
 */
class SettingsController extends Controller
{
    /** @var \App\Handler\ApiHandler */
    protected $apiHandler;

    /** @var \App\Handler\SettingsHandler */
    protected $settingsHandler;

    public function __construct(
        ApiHandler $apiHandler,
        SettingsHandler $settingsHandler
    ) {
        $this->apiHandler = $apiHandler;
        $this->settingsHandler = $settingsHandler;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = $this->settingsHandler->getUser(Auth::id());
        return view('settings/settings', compact('user'));
    }

    public function updateUserData(SettingsUpdateUserDataRequest $request)
    {
        try {

            $data = $request->validated();

            $this->settingsHandler->userManager()->updateUserData(Auth::id(), $data['name']);

            return redirect()->route('settings');

        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return redirect()->route('settings')->with([
                'internalError' => __('messages.internal_error'),
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAccount(Request $request)
    {
        try {

            $this->apiHandler->userManager()->deleteUser(Auth::user(), $request->only([
                'dont_remove_public_foods',
            ]));

            Auth::logout();

            return redirect()->route('login');

        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return redirect()->route('settings')->with([
                'internalError' => __('messages.internal_error'),
            ]);
        }
    }
}
