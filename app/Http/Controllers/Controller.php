<?php

namespace App\Http\Controllers;

use App\Handler\ApiHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\ValidationException;
use LogicException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param \Exception $ex
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    protected function handleApiException(\Exception $ex)
    {
        // Skip validation Exceptions and use Laravel's in build validation handling
        if ($ex instanceof ValidationException) {
            throw $ex;
        }
        $exClass = get_class($ex);
        $res = [
            'message' => $ex->getMessage(),
        ];
        $status = 500;
        if (in_array($exClass, [NotFoundResourceException::class], false)) {
            $status = 404;
        } elseif (in_array($exClass, [LogicException::class], true)) {
            $status = 400;
        } elseif (in_array($exClass, [AuthorizationException::class], true)) {
            $status = 403;
        } else {
            $res['message'] = 'Internal Error';
        }

        Log::error($ex->getMessage(), [
            'exception_type' => $exClass,
            'response_status' => $status,
        ]);

        return response()->json($res, $status);
    }

    /**
     * @return \App\Handler\ApiHandler
     */
    public function apiHandler(): ApiHandler
    {
        return app(ApiHandler::class);
    }
}
