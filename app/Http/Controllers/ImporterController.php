<?php

namespace App\Http\Controllers;

use App\Importer\FoodImporter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ImporterController extends Controller
{
    public function __construct()
    {
    }

    public function importView(Request $request)
    {
        if (!in_array(Auth::id(), explode(',', env('ADMIN_USERS', '')))) {

            Log::warning("The user has no permission to import a food.", [
                'user_id' => Auth::id(),
                'server_data' => $request->server->all(),
            ]);

            return redirect('/foods');
        }
        // TODO(ssandriesser): check if user has permission
        return view('importer/index');
    }

    public function import(Request $request)
    {
        try {

            if (!in_array(Auth::id(), explode(',', env('ADMIN_USERS', '')))) {

                Log::warning("The user has no permission to import a food.", [
                    'user_id' => Auth::id(),
                    'server_data' => $request->server->all(),
                ]);

                return redirect('/foods');
            }

            Log::info("The user try to import a food.", [
                'user_id' => Auth::id(),
                'driver' => $request->get('driver'),
                'reference' => $request->get('reference'),
            ]);

            // TODO(ssandriesser): check if user has permission
            $foodImport = new FoodImporter();
            $food = $foodImport->from($request->get('driver'), $request->get('reference'));
            $id = $food->id;

            Log::info("The user imported a food.", [
                'food_id' => $id,
            ]);

            return redirect('/foods/'.$id);

        } catch (\Exception $ex) {
            Log::error($ex->getMessage(), [
                'exception_type' => get_class($ex),
            ]);
        }
        return redirect('/foods');
    }
}
