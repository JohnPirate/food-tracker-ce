<?php

namespace App\Http\Controllers;

use App\Facades\Loc;
use App\Models\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = $this->apiHandler()->getUserData(Auth::id());
        return view('home', compact('user'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $locale
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function localization(Request $request, $locale)
    {
        if (config('app.debug') || $request->get('reset', false) === 'true') {
            foreach (Loc::supported() as $configuredLocale) {
                Cache::forget("lang.{$configuredLocale}.js");
            }
        }

        $strings = Cache::rememberForever("lang.{$locale}.js", function () use ($locale) {
            $lang = $locale;

            $files   = glob(app()->langPath($lang . '/*.php'));
            $strings = [];

            foreach ($files as $file) {
                $name           = basename($file, '.php');
                $strings[$name] = require $file;
            }

            return json_encode($strings);
        });

        return response('window.i18n = ' . $strings . ';')
            ->withHeaders([
                'Content-Type' => 'application/javascript',
            ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $locale
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeLocalization(Request $request, $locale)
    {
        if (!Loc::isSupported($locale)) {
            // TODO(ssandriesser): display a error message if locale is not supported
            return redirect(route('dashboard'));
        }
        return redirect()->back()->cookie(Locale::COOKIE_CURRENT_LOCALE, $locale, strtotime('+10years'));
    }
}
