<?php

namespace App\Http\Controllers;

use App\Handler\ApiHandler;
use App\Http\Requests\CopyConsumedFoodGroupRequest;
use App\Http\Requests\CreateTemplateFromFoodGroupingRequest;
use App\Http\Requests\DeleteFoodGroupingRequest;
use App\Http\Requests\StoreFoodConsumeRequest;
use App\Http\Requests\UpdateFoodConsumeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class ConsumeController
 * @package App\Http\Controllers
 */
class ConsumeController extends Controller
{
    /**
     * @var \App\Handler\ApiHandler
     */
    protected $apiHandler;

    public function __construct(
        ApiHandler $apiHandler
    ) {
        $this->apiHandler = $apiHandler;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function overview(Request $request)
    {
        return view('consume.overview', [
            'from' => $request->get('from', date('Y-m-d')),
            'to' => $request->get('to', date('Y-m-d')),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listData(Request $request)
    {
        try {

            $data = $this->apiHandler
                ->consumeManager()
                ->getConsumedFoodsForUser(
                    Auth::id(),
                    $request->get('from'),
                    $request->get('to')
                );
            return response()->json($data);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\StoreFoodConsumeRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreFoodConsumeRequest $request)
    {
        try {

            $id = $this->apiHandler->storeConsumedFood($request->all([
                'consumed_at',
                'consumed_value',
                'group',
                'food_id',
            ]));
            return response()->json([
                'message' => __('messages.success_store'),
                'data' => $id,
            ]);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\UpdateFoodConsumeRequest $request
     * @param int                                         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateFoodConsumeRequest $request, $id)
    {
        try {

            $data = $this->apiHandler->updateConsumedFood(
                $id,
                $request->all([
                    'consumed_at',
                    'consumed_value',
                    'group',
                ])
            );
            return response()->json([
                'message' => __('messages.success_update'),
                'data' => $data,
            ]);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $id)
    {
        try {

            $this->apiHandler->deleteConsumedFood($id);
            return response()->json([
                'message' => __('messages.success_deleted'),
            ]);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listGroups(Request $request)
    {
        try {

            $data = $this->apiHandler
                ->consumeManager()
                ->getConsumedFoodGroupsForUser(
                    Auth::id(),
                    $request->get('from'),
                    $request->get('to')
                );
            return response()->json($data);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\CopyConsumedFoodGroupRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function copyGroup(CopyConsumedFoodGroupRequest $request)
    {
        try {

            $data = [
                'group' => $request->json('group'),
                'group_id_origin' => $request->json('group_id_origin'),
                'from' => $request->json('from'),
                'to' => $request->json('to'),
            ];

            $this->apiHandler
                ->consumeManager()
                ->copyGroupForUser(
                    Auth::user(),
                    $data
                );
            return response()->json([
                'message' => __('messages.success_copied'),
            ]);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\DeleteFoodGroupingRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFoodGrouping(DeleteFoodGroupingRequest $request)
    {
        try {

            $groupId = $request->get('group_id');
            $consumedAt = $request->get('consumed_at');

            Log::info('The user is trying to delete a food grouping', [
                'group_id' => $groupId,
                'delete_date' => $consumedAt,
                'user_id' => Auth::id(),
            ]);

            $this->apiHandler->deleteFoodGrouping(Auth::id(), $groupId, $consumedAt);

            Log::info('Delete the food grouping successful');

            return response()->json([
                'message' => __('messages.success_delete_group_entries'),
            ]);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\CreateTemplateFromFoodGroupingRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTemplateFromFoodGrouping(CreateTemplateFromFoodGroupingRequest $request)
    {
        try {

            $groupId = $request->get('group_id');
            $templateName = $request->get('template_name');
            $consumedAt = $request->get('consumed_at');

            Log::info('The user is trying to create a template from a food grouping', [
                'group_id' => $groupId,
                'consumed_at' => $consumedAt,
                'template_name' => $templateName,
                'user_id' => Auth::id(),
            ]);

            $templateId = $this->apiHandler->createTemplateFromFoodGrouping(Auth::id(), $groupId, $templateName, $consumedAt);

            Log::info('Create template from food grouping successful');

            return response()->json([
                'message' => __('messages.success_create_template_from_food_grouping'),
                'data' => [
                    'id' => $templateId,
                ],
            ]);

        } catch(\Exception $ex) {
            return $this->handleApiException($ex);
        }
    }
}
