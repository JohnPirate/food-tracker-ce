<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddFoodToTemplateRequest;
use App\Http\Requests\ConsumeTemplateRequest;
use App\Http\Requests\StoreTemplateRequest;
use App\Http\Requests\UpdateTemplateRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class TemplateController
 * @package App\Http\Controllers
 */
class TemplateController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function overview()
    {
        return view('food_templates.overview');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        $templateData = [
            'type' => 'new',
        ];
        return view('food_templates.form', $templateData);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $templateData = [
            'templateId' => $id,
        ];
        return view('food_templates.show', $templateData);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $templateData = [
            'templateId' => $id,
            'type' => 'edit',
        ];
        return view('food_templates.form', $templateData);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function consume($id)
    {
        $templateData = [
            'templateId' => $id,
        ];
        return view('food_templates.consume', $templateData);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listTemplates()
    {
        try {

            $data = $this->apiHandler()
                ->templateManager()
                ->getTemplatesForUser(Auth::id())
            ;

            return response()->json($data);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTemplate(Request $request, $id)
    {
        try {

            $full = $request->get('full') === 'true';

            $data = $this->apiHandler()->getTemplateForUser(Auth::id(), $id, $full);

            return response()->json($data);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\UpdateTemplateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeTemplate(StoreTemplateRequest $request)
    {
        try {

            $data = $request->all([
                'name',
                'description',
                'portion',
                'ingredients',
            ]);

            Log::info('The user is trying to store a template', [
                'request_data' => $data,
                'user_id' => Auth::id(),
            ]);

            $id = $this->apiHandler()->storeTemplateForUser(Auth::id(), $data);
            $template = $this->apiHandler()->getTemplateForUser(Auth::id(), $id);

            Log::info('Storing the new template successful', [
                'template_id' => $id,
            ]);

            return response()->json([
                'data' => $template,
                'message' => __('messages.success_store'),
            ]);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\UpdateTemplateRequest $request
     * @param int                                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTemplate(UpdateTemplateRequest $request, $id)
    {
        try {

            $data = $request->all([
                'id',
                'name',
                'description',
                'portion',
                'ingredients',
            ]);

            Log::info('The user is trying to update a template', [
                'template_id' => $id,
                'request_data' => $data,
                'user_id' => Auth::id(),
            ]);

            $data['id'] = $id;

            $this->apiHandler()->updateTemplateForUser(Auth::id(), $data);

            return response()->json([
                'message' => __('messages.success_update'),
            ]);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTemplate(Request $request, $id)
    {
        try {

            Log::info('The user is trying to delete a template', [
                'template_id' => $id,
                'user_id' => Auth::id(),
            ]);

            $this->apiHandler()->deleteTemplateFromUser(Auth::id(), (int)$id);

            return response()->json([
                'message' => __('messages.success_deleted'),
            ]);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\ConsumeTemplateRequest $request
     * @param int                                       $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function consumeTemplate(ConsumeTemplateRequest $request, $id)
    {
        try {

            $data = $request->all([
                'group_name',
                'ingredients',
                'consumed_at',
            ]);

            Log::info('The user is trying to consume a template', [
                'template_id' => $id,
                'request_data' => $data,
                'user_id' => Auth::id(),
            ]);

            $this->apiHandler()->consumeTemplateForUser(Auth::id(), $id, $data);

            return response()->json([
                'message' => __('messages.success_consumed'),
            ]);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param \App\Http\Requests\AddFoodToTemplateRequest $request
     * @param int                                         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addFoodToTemplate(AddFoodToTemplateRequest $request, $id)
    {
        try {

            $data = $request->all([
                'food_id',
                'value',
            ]);

            Log::info('The user is trying to add a food to a template', [
                'template_id' => $id,
                'request_data' => $data,
                'user_id' => Auth::id(),
            ]);

            $this->apiHandler()->addFoodToTemplate(Auth::id(), $id, $data);

            return response()->json([
                'message' => __('messages.success_food_added_to_template'),
            ]);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function copyTemplate($id): JsonResponse
    {
        try {

            Log::info('The user is trying to copy a template', [
                'template_id' => $id,
                'user_id' => Auth::id(),
            ]);

            $templateId = $this->apiHandler()->copyTemplate(Auth::id(), $id);

            Log::info('Coping the new template successful', [
                'template_id' => $templateId,
            ]);

            return response()->json([
                'message' => __('messages.success_food_added_to_template'),
                'data' => [
                    'template_id' => $templateId,
                ]
            ]);

        } catch(Exception $ex) {
            return $this->handleApiException($ex);
        }
    }
}
