<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\FoodSearchResource;
use App\Models\Food;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class TemplateController
 * @package App\Http\Controllers
 */
class FoodController extends Controller
{
    /**
     * @param Request  $request
     *
     * @return JsonResponse
     */
    public function overview(Request $request): JsonResponse
    {
        $formData = $request->validate([
            "search_field" => "nullable|min:2|max:255",
        ]);
        $foodQuery = Food::where("name", "like", "%{$formData['search_field']}%")
            ->orWhere("brand", "like", "%{$formData['search_field']}%")
        ;

        // TODO(ssandriesser): use pagination!!
        return response()->json([ "data" => FoodSearchResource::collection($foodQuery->get()) ]);
    }
}
