<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTemplateRequest;
use App\Models\Food;
use App\Models\Template;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TemplateController
 * @package App\Http\Controllers
 */
class TemplateController extends Controller
{
    /**
     * @param Request  $request
     * @param Template $template
     *
     * @return JsonResponse
     */
    public function view(Request $request, Template $template): JsonResponse
    {
        if ($request->user()->cannot('view', $template)) {
            throw new NotFoundHttpException();
        }
        return response()->json($template->toResource());
    }

    public function store(StoreTemplateRequest $request): JsonResponse
    {
        // TODO(ssandriesser): check if user is allowed to store a new template
        $formFields = $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:4000',
            'portion' => 'required|numeric|between:1,999',
            'foods' => 'array|between:0,50',
            'foods.*.value' => 'numeric',
            'foods.*.food_id' => 'numeric',
        ]);

        try {

            $foodIds = array_column($formFields['foods'], 'food_id');
            $foodModels = Food::availableFoods($foodIds)->get()->keyBy('id');

            foreach ($foodIds as $idx => $foodId) {
                /** @var Food $food */
                $food = $foodModels->get($foodId);

                // Food must not be deleted or must not be private and owned by another user
                if ($food->deleted_at || ($food->private && $food->user_id !== Auth::id())) {
                    throw ValidationException::withMessages([
                        "foods.{$idx}.food_id" => [ __('validation.invalid_food') ],
                    ]);
                }
            }

            DB::beginTransaction();

            /** @var Template $template */
            $template = Auth::user()->templates()->create([
                "name" => $formFields["name"],
                "description" => $formFields["description"],
                "portion" => $formFields["portion"],
            ]);

            $template->foods()->sync($formFields["foods"]);

            DB::commit();

            return response()->json([
                "message" => __('messages.success_store'),
                "id" => $template->id,
            ]);

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::warning("DB rollback");
            return $this->handleApiException($ex);
        }
    }

    public function update(Request $request, Template $template): JsonResponse
    {
        // TODO(ssandriesser): check if user is allowed to update a new template
        $formFields = $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:4000',
            'portion' => 'required|numeric|between:1,999',
            'foods' => 'array|between:0,50',
            'foods.*.value' => 'numeric',
            'foods.*.food_id' => 'numeric',
        ]);

        try {

            $foodIds = array_column($formFields[ 'foods' ], 'food_id');
            $foodModels = Food::availableFoods($foodIds)->get()->keyBy('id');

            foreach ($foodIds as $idx => $foodId) {
                /** @var Food $food */
                $food = $foodModels->get($foodId);

                // Food must not be deleted or must not be private and owned by another user
                if ($food->deleted_at || ($food->private && $food->user_id !== Auth::id())) {
                    throw ValidationException::withMessages([
                        "foods.{$idx}.food_id" => [ __('validation.invalid_food') ],
                    ]);
                }
            }

            DB::beginTransaction();

            $template->fill($formFields);
            $template->save();

            $template->foods()->sync($formFields["foods"]);

            DB::commit();

            return response()->json([
                "message" => __('messages.success_update'),
            ]);

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::warning("DB rollback");
            return $this->handleApiException($ex);
        }
    }
}
