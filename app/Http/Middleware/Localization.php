<?php

namespace App\Http\Middleware;

use App\Facades\Loc;
use App\Models\Locale;
use Closure;

/**
 * Class Localization
 * @package App\Http\Middleware
 */
class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->cookie(Locale::COOKIE_CURRENT_LOCALE);
        if ($locale && Loc::isSupported($locale)) {
            Loc::set($locale);
        }
        return $next($request);
    }
}
