<?php

namespace App\Policies;

use App\Models\Template;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TemplatePolicy
{
    use HandlesAuthorization;

    public function view(User $user, Template $template): bool
    {
        return $user->isAdmin() || $template->isCreator($user);
    }
}
