SHELL := /bin/bash

PROJECT_HOME=${PWD}

WWWUSER := $(shell id -u)
WWWGROUP := $(shell id -g)

CONTAINER_PREFIX="food-tracker-ce"
CONTAINER_USER="${WWWUSER}:${WWWGROUP}"

PHP_IMAGE="${CONTAINER_PREFIX}-php8"
PHP_CONTAINER="${PHP_IMAGE}-1"

COMPOSER_HOME="${HOME}/.composer"


build:
	echo "Container User: ${CONTAINER_USER}"

up:
	env WWWUSER=${WWWUSER} WWWGROUP=${WWWGROUP} docker compose -f docker-compose.yml up -d --remove-orphans

up-build:
	env WWWUSER=${WWWUSER} WWWGROUP=${WWWGROUP} docker compose -f docker-compose.yml up -d --remove-orphans --no-deps --build

down:
	env WWWUSER=${WWWUSER} WWWGROUP=${WWWGROUP} docker compose -f docker-compose.yml down --remove-orphans

logs:
	env WWWUSER=${WWWUSER} WWWGROUP=${WWWGROUP} docker compose -f docker-compose.yml logs -f

# =============================================================================
# Artisan actions

artisan-migrate:
	docker exec -it "${PHP_CONTAINER}" php artisan migrate

artisan-migrate-rollback:
	docker exec -it "${PHP_CONTAINER}" php artisan migrate:rollback

artisan-migrate-refresh:
	docker exec -it "${PHP_CONTAINER}" php artisan migrate:refresh

artisan-clear-all:
	docker exec -it "${PHP_CONTAINER}" php artisan view:clear
	docker exec -it "${PHP_CONTAINER}" php artisan cache:clear
	docker exec -it "${PHP_CONTAINER}" php artisan optimize:clear
	docker exec -it "${PHP_CONTAINER}" php artisan route:clear
	docker exec -it "${PHP_CONTAINER}" php artisan event:clear
	docker exec -it "${PHP_CONTAINER}" php artisan config:clear
	docker exec -it "${PHP_CONTAINER}" php artisan clear-compiled

artisan-seed:
	docker exec -it "${PHP_CONTAINER}" php artisan db:seed

artisan-package-discover:
	docker exec -it "${PHP_CONTAINER}" php artisan package:discover --ansi

# =============================================================================
# Docker actions

docker-check:
	docker container ls --all

docker-build:
	docker build -t "${CONTAINER_PREFIX}_php" ${PROJECT_HOME}/docker/php

docker-clean-images:
	docker image rm "${CONTAINER_PREFIX}_php"

docker-clean-volumes:
	docker volume rm "${CONTAINER_PREFIX}_mariadb_db" \
	 "${CONTAINER_PREFIX}_mariadb_test_db" \
	 "${CONTAINER_PREFIX}_nginxlogs" \
	 "${CONTAINER_PREFIX}_redis_store"

docker-rebuild: docker-clean-images docker-build
