<?php

use App\Http\Controllers\ConsumeController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImporterController;
use App\Http\Controllers\NutrientController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\V2;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);


Route::middleware(['auth'])->group(function () {

    Route::get('/settings', [SettingsController::class, 'index'])->name('settings');
    Route::patch('/settings/update-user-data', [SettingsController::class, 'updateUserData'])->name('settings_update_user_data');
    Route::delete('/settings/delete-account', [SettingsController::class, 'deleteAccount'])->name('settings_delete_account');

    Route::middleware(['verified'])->group(function () {

        Route::get('/', [HomeController::class , 'index'])->name('dashboard');

        Route::get('/foods', [FoodController::class, 'catalogue'])->name('food_catalouge');
        Route::get('/foods/new', [FoodController::class, 'newData'])->name('food_new');
        Route::get('/foods/{id}', [FoodController::class, 'show'])->name('food_show');
        Route::get('/foods/{id}/edit', [FoodController::class, 'edit'])->name('food_edit');

        Route::get('/consumed-foods', [ConsumeController::class, 'overview'])->name('consume_overview');
        Route::get('/import', [ImporterController::class, 'importView'])->name('import_view');
        Route::post('/import', [ImporterController::class, 'import'])->name('import_action');

        Route::get('/templates', [TemplateController::class, 'overview'])->name('templates_overview');
        Route::get('/templates/new', [TemplateController::class, 'new'])->name('template_new');
        Route::get('/templates/{id}', [TemplateController::class, 'show'])->name('template_view');
        Route::get('/templates/{id}/edit', [TemplateController::class, 'edit'])->name('template_edit');
        Route::get('/templates/{id}/consume', [TemplateController::class, 'consume'])->name('template_consume');
    });
});

Route::middleware(['verified', 'auth'])->prefix('/api/v1')->name('api.v1.')->group(function () {

    Route::get('/nutrients', [NutrientController::class, 'listData'])->name('list_nutrients');
    Route::get('/nutrients/units', [NutrientController::class, 'listNutrientUnits'])->name('list_nutrient_units');

    Route::get('/foods', [FoodController::class, 'listData'])->name('list_foods');
    Route::get('/foods/search', [FoodController::class, 'search'])->name('search_foods');
    Route::post('/foods', [FoodController::class, 'store'])->name('store_food');
    Route::get('/foods/{id}', [FoodController::class, 'getData'])->name('get_food');
    Route::put('/foods/{id}', [FoodController::class, 'update'])->name('update_food');
    Route::delete('/foods/{id}', [FoodController::class, 'delete'])->name('delete_food');

    Route::get('/consumed-foods', [ConsumeController::class, 'listData'])->name('list_consumed_foods');
    Route::post('/consumed-foods', [ConsumeController::class, 'store'])->name('store_consumed_food');
    Route::put('/consumed-foods/{id}', [ConsumeController::class, 'update'])->name('update_consumed_food');
    Route::delete('/consumed-foods/{id}', [ConsumeController::class, 'delete'])->name('delete_consumed_food');
    Route::get('/consumed-foods/groups', [ConsumeController::class, 'listGroups'])->name('list_consumed_food_groups');

    Route::post('/consumed-food-groups/copy', [ConsumeController::class, 'copyGroup'])->name('copy_group');
    Route::post('/consumed-food-groups/delete-grouping', [ConsumeController::class, 'deleteFoodGrouping'])->name('delete_food_grouping');
    Route::post('/consumed-food-groups/create-template', [ConsumeController::class, 'createTemplateFromFoodGrouping'])->name('create_template_from_food_grouping');

    Route::get('/templates', [TemplateController::class, 'listTemplates'])->name('list_templates');
    Route::post('/templates', [TemplateController::class, 'storeTemplate'])->name('store_template');
    Route::get('/templates/{id}', [TemplateController::class, 'getTemplate'])->name('get_template');
    Route::put('/templates/{id}', [TemplateController::class, 'updateTemplate'])->name('update_template');
    Route::delete('/templates/{id}', [TemplateController::class, 'deleteTemplate'])->name('delete_template');
    Route::post('/templates/{id}/consume', [TemplateController::class, 'consumeTemplate'])->name('consume_template');
    Route::post('/templates/{id}/add-food', [TemplateController::class, 'addFoodToTemplate'])->name('add_food_to_template');
    Route::post('/templates/{id}/copy', [TemplateController::class, 'copyTemplate'])->name('copy_template');

});

Route::middleware(['verified', 'auth'])->prefix('/api/v2')->name('api.v2.')->group(function () {

    Route::get('/foods', [V2\FoodController::class, 'overview'])->name('food_overview');

    Route::post('/templates', [V2\TemplateController::class, 'store'])->name('store_template');
    Route::get('/templates/{template}', [V2\TemplateController::class, 'view'])->name('get_template');
    Route::put('/templates/{template}', [V2\TemplateController::class, 'update'])->name('update_template');

});

Route::get('/js/lang/{locale}', [HomeController::class, 'localization'])->name('location_translations');
Route::get('/change-localization/{locale}', [HomeController::class, 'changeLocalization'])->name('change_localization');
