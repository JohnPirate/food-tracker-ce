<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNutrientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `nutrients` MODIFY `group` varchar(45) NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $groups = config('nutrients.groups', []);
        $proximates = config('nutrients.proximates', []);
        $groups = array_merge($groups, $proximates);
        DB::statement('ALTER TABLE `nutrients` MODIFY `group` enum(\''.implode('\',\'', $groups).'\') NULL');
    }
}
