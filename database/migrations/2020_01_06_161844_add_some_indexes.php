<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `consumed_foods` ADD INDEX `consumed_foods_user_id_consumed_at_idx` (`user_id` ASC, `consumed_at` DESC)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `consumed_foods` DROP INDEX `consumed_foods_user_id_consumed_at_idx`');
    }
}
