<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutrientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $amount = array_keys(config('nutrients.units.amount', ['g']));
        $energy = array_keys(config('nutrients.units.energy', ['kcal']));
        $units = array_merge($amount, $energy);
        $groups = config('nutrients.groups', []);
        $proximates = config('nutrients.proximates', []);
        $groups = array_merge($groups, $proximates);

        Schema::create('nutrients', function (Blueprint $table) use ($units, $groups) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('usda_id')->nullable();
            $table->enum('unit', $units)->nullable();
            $table->enum('group', $groups)->nullable();
            $table->string('key', 100);
            $table->boolean('usable')->default(0);
            $table->timestamps();

            $table->unique('key');
            $table->index('group');
            $table->index('usable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrients');
    }
}
