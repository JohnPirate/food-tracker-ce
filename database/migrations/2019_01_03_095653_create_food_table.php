<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $amount = array_keys(config('nutrients.units.amount', ['g']));
        $energy = array_keys(config('nutrients.units.energy', ['kcal']));
        $units = array_merge($amount, $energy);

        Schema::create('foods', function (Blueprint $table) use ($units) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('private')->default(0);
            $table->float('ref_value')->default(100);
            $table->enum('ref_unit', $units)->default('g');
            $table->boolean('carb_without_fiber')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->index('name');
        });

        Schema::create('food_imports', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->unsignedInteger('food_id');
            $table->enum('source', ['usda']);
            $table->text('reference');
            $table->timestamps();

            $table->foreign('food_id')->references('id')->on('foods')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('food_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->unsignedInteger('food_id');
            $table->unsignedInteger('user_id');

            $table->primary(['food_id', 'user_id']);

            $table->foreign('food_id')->references('id')->on('foods')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('food_nutrient', function (Blueprint $table) use ($units) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->unsignedInteger('food_id');
            $table->unsignedInteger('nutrient_id');
            $table->float('value');
            $table->enum('unit', $units);

            $table->primary(['food_id', 'nutrient_id']);

            $table->foreign('food_id')->references('id')->on('foods')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nutrient_id')->references('id')->on('nutrients')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_nutrient');
        Schema::dropIfExists('food_user');
        Schema::dropIfExists('food_imports');
        Schema::dropIfExists('foods');
    }
}
