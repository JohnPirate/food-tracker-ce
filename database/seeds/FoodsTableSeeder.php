<?php

use Illuminate\Database\Seeder;
use App\Importer\FoodImporter;
use Illuminate\Support\Facades\DB;

class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $foodImport = new FoodImporter();
        $foodImport->from('usda_file', '11090'); // Broccoli, raw
        $foodImport->from('usda_file', '09302'); // Raspberries, raw
        $foodImport->from('usda_file', '11529'); // Tomatoes, red, ripe, raw, year round average
        $foodImport->from('usda_file', '11205'); // Cucumber, with peel, raw
        $foodImport->from('usda_file', '09003'); // Apples, raw, with skin

        DB::table('food_user')->insert([
            [
                'food_id' => 1,
                'user_id' => 1,
            ],
            [
                'food_id' => 4,
                'user_id' => 1,
            ],
            [
                'food_id' => 2,
                'user_id' => 2,
            ],
            [
                'food_id' => 3,
                'user_id' => 2,
            ],
        ]);

        DB::table('foods')->whereIn('id', [2,4])->update(['private' => 1]);
        DB::table('foods')->whereIn('id', [1])->update(['deleted_at' => \Illuminate\Support\Carbon::now()->format("Y-m-d H:i:s")]);
    }
}
