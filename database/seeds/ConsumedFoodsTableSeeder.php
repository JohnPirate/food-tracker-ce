<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ConsumedFoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var \App\Models\User $user */
        $user = \App\Models\User::find(1);

        $groupId = DB::table('consumed_food_groups')->insertGetId([
            'name' => 'Fresh vegetables',
            'user_id' => $user->id,
            'created_at' => Carbon::now()->format("Y-m-d H:i:s"),
            'updated_at' => Carbon::now()->format("Y-m-d H:i:s"),
        ]);

        foreach (\App\Models\Food::get() as $food) {

            if ($food->id === 2) {
                continue;
            }

            for ($i = 0; $i < 5; ++$i) {

                $consume = [
                    'value' => rand(1,100),
                    'consumed_at' => Carbon::now()->subDays($i)->format("Y-m-d H:i:s"),
                    'created_at' => Carbon::now()->format("Y-m-d H:i:s"),
                    'updated_at' => Carbon::now()->format("Y-m-d H:i:s"),
                ];

                if ($i === 0 && ($food->id % 2) === 0) {
                    $consume['consumed_food_groups_id'] = $groupId;
                }

                $user->consumedFoods()->attach($food->id, $consume);
            }
        }
    }
}
