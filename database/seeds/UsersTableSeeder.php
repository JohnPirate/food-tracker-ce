<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'webdev',
            'email' => 'webdev@food-tracker.com',
            'password' => bcrypt('web12345678'),
        ]);

        DB::table('users')->insert([
            'name' => 'webdev2',
            'email' => 'webdev2@food-tracker.com',
            'password' => bcrypt('web12345678'),
        ]);
    }
}
