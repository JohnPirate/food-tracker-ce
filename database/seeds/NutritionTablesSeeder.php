<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class NutritionTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nutrients = require "nutrients_data.php";

        foreach ($nutrients as $nutrient) {

            $nutrient['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $nutrient['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            DB::table('nutrients')->insert($nutrient);
        }
    }
}
