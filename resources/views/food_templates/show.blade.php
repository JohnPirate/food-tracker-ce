@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="template-show-view"
         data-template-id="{{ $templateId }}"
    ></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/template_show_view.js') }}"></script>
@endpush
