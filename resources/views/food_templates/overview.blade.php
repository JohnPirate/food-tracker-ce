@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="templates-overview-view"></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/templates_overview_view.js') }}"></script>
@endpush
