@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    @if($type === 'new')
        <div id="template-edit-view"
             data-template-id="0"
        ></div>
    @else
        <div id="template-edit-view"
             data-template-id="{{ $templateId }}"
        ></div>
    @endif
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/template_edit_view.js') }}"></script>
@endpush
