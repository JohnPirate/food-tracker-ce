@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="template-consume-view"
         data-template-id="{{ $templateId }}"
    ></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/template_consume_view.js') }}"></script>
@endpush
