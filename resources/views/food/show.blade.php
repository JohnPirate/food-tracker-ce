@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="food-view"
         data-food-id="{{ $foodId }}"
    ></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/food_view.js') }}"></script>
@endpush
