@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="food-new-view"></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/food_new_view.js') }}"></script>
@endpush
