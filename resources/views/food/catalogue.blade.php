@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div id="food-catalouge-view"></div>
            </div>
        </div>
    </div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/food_catalouge_view.js') }}"></script>
@endpush
