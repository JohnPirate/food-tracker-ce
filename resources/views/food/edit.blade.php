@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="food-edit-view"
         data-food-id="{{ $foodId }}"
    ></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/food_edit_view.js') }}"></script>
@endpush
