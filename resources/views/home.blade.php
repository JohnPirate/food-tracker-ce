@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="dashboard-view"
         data-user='@json($user)'
    ></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/dashboard_view.js') }}"></script>
@endpush
