@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div id="consume-overview-view"
         data-from="{{ $from }}"
         data-to="{{ $to }}"
    ></div>
@endsection

@push('page-scripts')
    <script src="{{ mix('/js/consume_overview_view.js') }}"></script>
@endpush
