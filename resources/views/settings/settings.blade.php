@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-lg-8">
                <h4 class="mb-3">{{ __('pages.settings.title') }}</h4>

                <form method="POST"
                      action="{{ route('settings_update_user_data') }}"
                      id=""
                >
                    @csrf
                    @method('patch')
                    @component('components.card')
                        <div class="card-body">
                            <h5 class="mb-3">{{ __('pages.settings.block_save_user_data.title') }}</h5>
                            <p class="mb-0">{{ __('pages.settings.block_save_user_data.text.general') }}</p>
                        </div>

                        <div class="card-body border-top">

                            @include('components.text-field', ['textField' => [
                                'name' => 'name',
                                'id' => 'name',
                                'value' => old('name', $user->name),
                                'type' => 'text',
                                'required' => true,
                                'title' => __('forms.username.title'),
                                'placeholder' => __('forms.username.placeholder'),
                                'input_props' => [
                                    'aria-label' => __('forms.username.aria_label'),
                                    'tabindex' => 1,
                                    'autocomplete' => 'off',
                                ]
                            ]])

                        </div>

                        <div class="card-body border-top">
                            <button class="btn btn-success"
                                    type="submit"
                                    title="{{ __('components.button.update_personal_data.title') }}"
                                    aria-label="{{ __('components.button.update_personal_data.title') }}"
                            >{{ __('components.button.update_personal_data.text') }}</button>
                        </div>
                    @endcomponent
                </form>

                <form method="POST"
                      action="{{ route('settings_delete_account') }}"
                      id="delete-account"
                >
                    @csrf
                    @method('delete')
                    @component('components.card')
                        <div class="card-body">
                            <h5 class="mb-3">{{ __('pages.settings.block_delete_account.title') }}</h5>
                            <p class="mb-0">{{ __('pages.settings.block_delete_account.text.general') }}</p>
                        </div>
                        <div class="card-body border-top">
                                <h6>{{ __('pages.settings.block_delete_account.subtitle.opt') }}</h6>
                                @include('components.checkbox-field', ['checkboxField' => [
                                    'name' => 'dont_remove_public_foods',
                                    'id' => 'dont-remove-public-foods',
                                    'title' => __('forms.dont_remove_public_foods.title'),
                                    'required' => true,
                                    'input_props' => [
                                        'aria-label' => __('forms.dont_remove_public_foods.aria_label'),
                                        'tabindex' => 6,
                                    ],
                                    'help' => __('forms.dont_remove_public_foods.help'),
                                ]])
                        </div>
                        <div class="card-body border-top">
                            <button class="btn btn-danger"
                                    type="submit"
                                    title="{{ __('components.button.dont_remove_public_foods.title') }}"
                                    aria-label="{{ __('components.button.dont_remove_public_foods.title') }}"
                            >{{ __('components.button.dont_remove_public_foods.text') }}</button>
                        </div>
                    @endcomponent
                </form>
            </div>
        </div>
    </div>
@endsection

@push('page-scripts')
<script>
    function deleteAccount(e) {
        e.preventDefault();
        if (!window.confirm("{{__('components.modals.dont_remove_public_foods.message')}}")) {
            return;
        }
        this.submit();
    }
    document.getElementById('delete-account').addEventListener('submit', deleteAccount);
</script>
@endpush
