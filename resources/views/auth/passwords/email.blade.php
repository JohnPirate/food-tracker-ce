@extends('layouts.app')

@section('page-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            @component('components.card')
                <div class="card-body">

                    <h4 class="card-title">{{ __('pages.reset_password.title') }}</h4>


                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        @include('components.text-field', ['textField' => [
                            'name' => 'email',
                            'id' => 'email',
                            'value' => old('email'),
                            'type' => 'email',
                            'required' => true,
                            'title' => __('forms.email.title'),
                            'placeholder' => __('forms.email.placeholder'),
                            'input_props' => [
                                'autofocus',
                                'aria-label' => __('forms.email.aria_label'),
                                'tabindex' => 1,
                            ]
                        ]])

                        <button type="submit"
                                class="btn btn-primary"
                                tabindex="2"
                                title="{{ __('components.button.send_password_reset_link.title') }}"
                                aria-label="{{ __('components.button.send_password_reset_link.title') }}"
                        >
                            {{ __('components.button.send_password_reset_link.text') }}
                        </button>
                    </form>
                </div>
            @endcomponent
        </div>
    </div>
</div>
@endsection
