@extends('layouts.app')

@section('page-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            @component('components.card')
                <div class="card-body">

                    <h4 class="card-title">{{ __('pages.password_update.title') }}</h4>

                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        @include('components.text-field', ['textField' => [
                            'name' => 'email',
                            'id' => 'email',
                            'value' => $email ?? old('email'),
                            'type' => 'email',
                            'required' => true,
                            'title' => __('forms.email.title'),
                            'placeholder' => __('forms.email.placeholder'),
                            'input_props' => [
                                'autofocus',
                                'aria-label' => __('forms.email.aria_label'),
                                'tabindex' => 1,
                                'autocomplete' => 'off',
                            ]
                        ]])

                        @include('components.text-field', ['textField' => [
                            'name' => 'password',
                            'id' => 'password',
                            'value' => null,
                            'type' => 'password',
                            'required' => true,
                            'title' => __('forms.password.title'),
                            'placeholder' => __('forms.password.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.password.aria_label'),
                                'tabindex' => 2,
                                'autocomplete' => 'off',
                            ]
                        ]])

                        @include('components.text-field', ['textField' => [
                            'name' => 'password_confirmation',
                            'id' => 'password_confirmation',
                            'value' => null,
                            'type' => 'password',
                            'required' => true,
                            'title' => __('forms.password_confirmation.title'),
                            'placeholder' => __('forms.password_confirmation.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.password_confirmation.aria_label'),
                                'tabindex' => 3,
                            ]
                        ]])

                        <button type="submit"
                                class="btn btn-primary"
                                tabindex="4"
                                title="{{ __('components.button.password_reset.title') }}"
                                aria-label="{{ __('components.button.password_reset.title') }}"
                        >
                            {{ __('components.button.password_reset.text') }}
                        </button>
                    </form>
                </div>
            @endcomponent
        </div>
    </div>
</div>
@endsection
