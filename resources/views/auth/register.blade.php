@extends('layouts.app')

@section('page-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            @component('components.card')
                <div class="card-body">

                    <h4 class="card-title">{{ __('links.register.text') }}</h4>

                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        @include('components.text-field', ['textField' => [
                            'name' => 'name',
                            'id' => 'name',
                            'value' => old('name'),
                            'type' => 'text',
                            'required' => true,
                            'title' => __('forms.username.title'),
                            'placeholder' => __('forms.username.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.username.aria_label'),
                                'tabindex' => 1,
                                'autocomplete' => 'off',
                            ]
                        ]])

                        @include('components.text-field', ['textField' => [
                            'name' => 'email',
                            'id' => 'email',
                            'value' => old('email'),
                            'type' => 'text',
                            'required' => true,
                            'title' => __('forms.email.title'),
                            'placeholder' => __('forms.email.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.email.aria_label'),
                                'tabindex' => 2,
                                'autocomplete' => 'off',
                            ]
                        ]])

                        @include('components.text-field', ['textField' => [
                            'name' => 'password',
                            'id' => 'password',
                            'value' => null,
                            'type' => 'password',
                            'required' => true,
                            'title' => __('forms.password.title'),
                            'placeholder' => __('forms.password.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.password.aria_label'),
                                'tabindex' => 3,
                                'autocomplete' => 'off',
                            ]
                        ]])

                        @include('components.text-field', ['textField' => [
                            'name' => 'password_confirmation',
                            'id' => 'password_confirmation',
                            'value' => null,
                            'type' => 'password',
                            'required' => true,
                            'title' => __('forms.password_confirmation.title'),
                            'placeholder' => __('forms.password_confirmation.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.password_confirmation.aria_label'),
                                'tabindex' => 4,
                            ]
                        ]])

                        <button type="submit"
                                class="btn btn-primary"
                                tabindex="7"
                                title="{{ __('components.button.register.title', ['appname' => config('app.name')]) }}"
                                aria-label="{{ __('components.button.register.title', ['appname' => config('app.name')]) }}"
                        >
                            {{ __('components.button.register.text') }}
                        </button>
                    </form>
                </div>
            @endcomponent
        </div>
    </div>
</div>
@endsection
