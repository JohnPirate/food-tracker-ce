@extends('layouts.app')

@section('page-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            @component('components.card')
                <div class="card-body">

                    <h4 class="card-title">{{ __('links.sign_in.text') }}</h4>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        @include('components.text-field', ['textField' => [
                            'name' => 'email',
                            'id' => 'email',
                            'value' => old('email'),
                            'type' => 'text',
                            'required' => true,
                            'title' => __('forms.email.title'),
                            'placeholder' => __('forms.email.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.email.aria_label'),
                                'tabindex' => 1,
                                'autocomplete' => 'username',
                            ]
                        ]])

                        @include('components.text-field', ['textField' => [
                            'name' => 'password',
                            'id' => 'password',
                            'value' => null,
                            'type' => 'password',
                            'required' => true,
                            'title' => __('forms.password.title'),
                            'placeholder' => __('forms.password.placeholder'),
                            'input_props' => [
                                'aria-label' => __('forms.password.aria_label'),
                                'tabindex' => 2,
                                'autocomplete' => 'current-password',
                            ]
                        ]])

                        @include('components.checkbox-field', ['checkboxField' => [
                            'name' => 'remember',
                            'id' => 'remember',
                            'title' => __('forms.remember.title'),
                            'input_props' => [
                                'aria-label' => __('forms.remember.aria_label'),
                                'tabindex' => 3,
                            ]
                        ]])

                        <div class="d-block d-sm-flex align-items-sm-center">
                            <div>
                                <button type="submit"
                                        class="btn btn-primary mr-3"
                                        title="{{ __('components.button.sign_in.title', ['appname' => config('app.name')]) }}"
                                        aria-label="{{ __('components.button.sign_in.title', ['appname' => config('app.name')]) }}"
                                >
                                    {{ __('components.button.sign_in.text') }}
                                </button>
                            </div>

                            @if (Route::has('password.request'))
                                <div class="pt-3 py-sm-0">
                                    <a class=""
                                       href="{{ route('password.request') }}"
                                       title="{{ __('links.forgot_password.title') }}"
                                       title="{{ __('links.forgot_password.title') }}"
                                    >
                                        {{ __('links.forgot_password.text') }}
                                    </a>
                                </div>
                            @endif
                        </div>

                    </form>
                </div>
            @endcomponent
        </div>
    </div>
</div>
@endsection
