@extends('layouts.app')

@section('page-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            @component('components.card')
                <div class="card-body">

                    <h4 class="card-title">{{ __('pages.verify_email.title') }}</h4>

                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('messages.fresh_verification_link_has_been_sent') }}
                        </div>
                    @endif

                    <form name="verificationResendForm" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        @method("POST")
                        {{ __('pages.verify_email.text.verify_1') }}
                        {{ __('pages.verify_email.text.verify_2') }}
                        <a href="javascript:document.forms.verificationResendForm.submit()"
                           title="{{ __('links.verification_resend.title') }}"
                           aria-label="{{ __('links.verification_resend.title') }}"
                        >{{ __('pages.verify_email.text.verify_3') }}</a>
                    </form>
                </div>
            @endcomponent
        </div>
    </div>
</div>
@endsection
