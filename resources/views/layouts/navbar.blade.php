<nav class="navbar navbar-expand-md navbar-light border-bottom bg-white fixed-top">
    <div class="container">
        <img src="{{ mix('/images/webicon_foodtracker.svg') }}" class="logo mr-2">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name') }}
        </a>
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="{{ __('others.toggle_navigation') }}"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth
                    @verified
                    @if (Route::has('consume_overview'))
                        <li class="nav-item @isactiveroute(['consume_overview']) active @endisactiveroute">
                            <a class="nav-link"
                               href="{{ route('consume_overview') }}"
                               title="{{ __('links.consumed_foods.title') }}"
                               aria-label="{{ __('links.consumed_foods.title') }}"
                            >{{ __('links.consumed_foods.text') }}</a>
                        </li>
                    @endif
                    @if (Route::has('food_catalouge'))
                        <li class="nav-item @isactiveroute(['food_catalouge', 'food_new', 'food_show', 'food_edit']) active @endisactiveroute">
                            <a class="nav-link"
                               href="{{ route('food_catalouge') }}"
                               title="{{ __('links.catalogue.title') }}"
                               aria-label="{{ __('links.catalogue.title') }}"
                            >{{ __('links.catalogue.text') }}</a>
                        </li>
                    @endif
                    @if (Route::has('templates_overview'))
                        <li class="nav-item @isactiveroute(['templates_overview', 'template_new', 'template_view', 'template_edit', 'template_consume']) active @endisactiveroute">
                            <a class="nav-link"
                               href="{{ route('templates_overview') }}"
                               title="{{ __('links.templates_overview.title') }}"
                               aria-label="{{ __('links.templates_overview.title') }}"
                            >{{ __('links.templates_overview.text') }}</a>
                        </li>
                    @endif
                    @endverified
                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link"
                           href="{{ route('login') }}"
                           title="{{ __('links.sign_in.title') }}"
                           aria-label="{{ __('links.sign_in.title') }}"
                        >{{ __('links.sign_in.text') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link"
                               href="{{ route('register') }}"
                               title="{{ __('links.register.title') }}"
                               aria-label="{{ __('links.register.title') }}"
                            >{{ __('links.register.text') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('settings') }}"
                               title="{{ __('links.settings.title') }}"
                               aria-label="{{ __('links.settings.title') }}"
                            >
                                {{ __('links.settings.text') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"
                               title="{{ __('links.sign_out.title') }}"
                               aria-label="{{ __('links.sign_out.title') }}"
                            >
                                {{ __('links.sign_out.text') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<div class="navbar border-top border-white"><div class="logo"></div></div>
<div id="global-alert"></div>
