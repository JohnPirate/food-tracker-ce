<!DOCTYPE html>
<html lang="{{ Loc::current()  }}" dir="{{ Loc::dir() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, viewport-fit=cover">
    <meta http-equiv="Content-Language" content="{{ Loc::current() }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ mix('/images/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ mix('/images/favicon.ico') }}" type="image/x-icon">
    <title>{{ config('app.name') }}</title>

    @routes

    <!-- Styles -->
    <link href="{{ mix('css/tw.css') }}" rel="stylesheet">
    <link href="{{ mix('css/vendor.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @stack('page-styles')

</head>
<body class="tw-bg-gray-100">

    @include('layouts.navbar')

    <main class="py-4">
        @yield('page-content')
    </main>

    @include('layouts.footer')

    <script src="{{ mix('/js/manifest.js') }}"></script>
    <script src="{{ mix('/js/vendor.js') }}"></script>
    <script src="{{ route('location_translations', ['locale' => Loc::current()]) }}"></script>
    <script src="{{ mix('/js/app.js') }}"></script>
    @stack('page-scripts')
    @if(session('internalError'))
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            window.$alert.error('{{ session('internalError') }}');
        });
    </script>
    @endif
</body>
</html>
