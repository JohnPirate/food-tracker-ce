<footer class="border-top mt-2 pt-3 mb-5">
    <div class="container">
        <div class="footer__link-list text-center mb-2">
            <div>
                <a href="https://gitlab.com/JohnPirate/food-tracker-ce"
                   target="_blank"
                   title="{{ __('links.repo.title') }}"
                   aria-label="{{ __('links.repo.title') }}"
                >{{ __('links.repo.text') }}</a>
            </div>
            <div>
                <a href="https://gitlab.com/JohnPirate/food-tracker-ce/blob/master/LICENSE"
                   target="_blank"
                   title="{{ __('links.licence.title') }}"
                   aria-label="{{ __('links.licence.title') }}"
                >{{ __('links.licence.text') }}</a>
            </div>
        </div>
        <div class="text-center mb-2">
            <div class="text-muted">FoodTracker Community Edition</div>
            <div class="text-muted"><small>{{ config('app.version') }}</small></div>
        </div>
        <div class="text-center">
            @foreach(Loc::supported() as $configuredLocale)
            <small>
                <a href="{{ route('change_localization', ['locale' => $configuredLocale]) }}"
                   class="text-black-50"
                   title="{{ __('others.change_translation', ['name' => Loc::nameFor($configuredLocale)]) }}"
                   aria-label="{{ __('others.change_translation', ['name' => Loc::nameFor($configuredLocale)]) }}"
                >{{ Loc::nameFor($configuredLocale) }}</a>
            </small>
            @endforeach
        </div>
    </div>
</footer>
