@extends('layouts.app')

@push('page-styles')
@endpush

@section('page-content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h4>Import foods</h4>

                <form method="POST" action="{{ route('import_action') }}">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="form-group">
                                <label for="importDriverForm">Import driver</label>
                                <select class="form-control"
                                        id="importDriverForm"
                                        name="driver"
                                >
                                    <option value="usda_food_data_central">USDA FoodData Central</option>
                                    <option value="usda">USDA</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="form-group">
                                <label for="referenceForm">Reference</label>
                                <input class="form-control"
                                       id="referenceForm"
                                       name="reference"
                                >
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 d-flex align-items-end">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Import</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('page-scripts')
@endpush
