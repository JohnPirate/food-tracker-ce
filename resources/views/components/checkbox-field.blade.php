<div class="form-group">
    <label class="custom-control custom-checkbox">
        <input type="checkbox"
               class="custom-control-input{{ isset($checkboxField['name']) && $errors->has($checkboxField['name']) ? ' is-invalid' : ''}}"
               @if ($errors->has($checkboxField['name'])) aria-invalid="true" aria-errormessage="{{ $checkboxField['name'] }}ErrorBlock" @endif
               @if (! $errors->has($checkboxField['name']) && $errors->any()) aria-invalid="false" @endif
               @if (isset($checkboxField['required'])) aria-required="true" @endif
               @if (isset($checkboxField['help'])) aria-describedby="{{ $checkboxField['name'] }}HelpBlock" @endif
               @if (isset($checkboxField['id'])) id="{{ $checkboxField['id']  }}" @endif
               @if (isset($checkboxField['name'])) name="{{ $checkboxField['name'] }}" @endif
               @if (isset($checkboxField['value'])) value="{{ $checkboxField['value'] }}" @endif
               @if (isset($checkboxField['checked']) && $checkboxField['checked']) checked @endif
               @if (isset($checkboxField['input_props']) && count($checkboxField['input_props']))
                   @foreach($checkboxField['input_props'] as  $key => $value)
                       {{ ' ' }}
                       @if (is_int($key))
                           {{ $value }}
                       @else
                           {{ $key }}="{{ $value }}"
                       @endif
                   @endforeach
               @endif
        >
        <span id="lbl_{{ $checkboxField['name'] }}" class="custom-control-label">{{ isset($checkboxField['title']) ? $checkboxField['title'] : '' }}</span>
    </label>
    @if (isset($checkboxField['name']) && $errors->has($checkboxField['name']))
        <div class="invalid-feedback"
              aria-live="assertive"
              id="{{ $checkboxField['name'] }}ErrorBlock"
        >{{ $errors->get($checkboxField['name'])[0] }}</div>
    @elseif (isset($checkboxField['help']) && ! $errors->has($checkboxField['name']))
        <div><small id="{{ $checkboxField['name'] }}HelpBlock" class="form-text text-muted">{{ $checkboxField['help'] }}</small></div>
    @endif
</div>