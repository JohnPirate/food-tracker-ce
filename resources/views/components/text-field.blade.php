<div class="form-group">
    @if (isset($textField['title']))
    <label class="form-label" {{ isset($textField['id']) ? ' for="'.$textField['id'].'"' : '' }}>
    {{ $textField['title'] }}
    @if (isset($textField['required']))
    <span class="text-warning">*</span>
    @endif
    @if (isset($textField['title_additional']))
    {!! $textField['title_additional'] !!}
    @endif
    </label>
    @endif
    <input
        class="form-control{{ isset($textField['name']) && $errors->has($textField['name']) ? ' is-invalid' : ''}}"
        @if ($errors->has($textField['name'])) aria-invalid="true" aria-errormessage="{{ $textField['name'] }}ErrorBlock" @endif
        @if (! $errors->has($textField['name']) && $errors->any()) aria-invalid="false" @endif
        @if (isset($textField['required'])) aria-required="true" @endif
        @if (isset($textField['help'])) aria-describedby="{{ $textField['name'] }}HelpBlock" @endif
        @if (isset($textField['type'])) type="{{ $textField['type'] }}" @endif
        @if (isset($textField['id'])) id="{{ $textField['id']  }}" @endif
        @if (isset($textField['name'])) name="{{ $textField['name'] }}" @endif
        @if (isset($textField['value'])) value="{{ $textField['value'] }}" @endif
        @if (isset($textField['placeholder'])) placeholder="{{ $textField['placeholder'] }}" @endif
        @if (isset($textField['input_props']) && count($textField['input_props']))
        @foreach($textField['input_props'] as  $key => $value)
        {{ ' ' }}
        @if (is_int($key))
        {{ $value }}
        @else
        {{ $key }}="{{ $value }}"
        @endif
        @endforeach
        @endif
    >
    @if (isset($textField['name']) && $errors->has($textField['name']))
    <span class="invalid-feedback"
          aria-live="assertive"
          id="{{ $textField['name'] }}ErrorBlock"
    >{{ $errors->get($textField['name'])[0] }}</span>
    @elseif (isset($textField['help']) && ! $errors->has($textField['name']))
    <small id="{{ $textField['name'] }}HelpBlock" class="form-text text-muted">{{ $textField['help'] }}</small>
    @endif
</div>
