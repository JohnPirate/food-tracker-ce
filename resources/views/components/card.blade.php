<div class="card border-0 mb-3 shadow-card rounded-{{ $rounded ?? 'lg' }} @isset($class){{$class}}@endisset">{{$slot}}</div>
