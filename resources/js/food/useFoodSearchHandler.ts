import { reactive, ref} from "vue";
import type { FoodSearch } from "@/types/v2/FoodSearch";
import { httpClient } from "@/utilities/httpClient";
import { useTableCtrl } from "@/form/useTableCtrl";

function useFoodSearchHandler({ minCntSearchChar = 1 }: { minCntSearchChar?: number } = {}) {

    let processing = ref<boolean>(false);

    let foods = ref<FoodSearch[]>([]);

    const tblCtrl = useTableCtrl({
        filter: { search_field: "" },
        async onChange(urlParams: Record<string, any>) {

            foods.value = [];

            if (!urlParams) {
                return;
            }

            if (!urlParams["search_field"]) {
                return;
            }

            if (!(urlParams["search_field"].length > 1 && urlParams["search_field"].length > minCntSearchChar)) {
                return;
            }

            try {

                processing.value = true;

                // TODO(ssandrieser) Debounce only search requests
                const { data } = await httpClient.get<{ data: FoodSearch[] }>("/api/v2/foods", { params: urlParams });

                foods.value = data.data;

            } catch(e) {

                // TODO(ssandriesser): error handling
                console.log(e);

            } finally {
                processing.value = false;
            }
        },
    });

    return reactive({
        reset() {
            tblCtrl.filter.search_field = "";
            foods.value = [];
        },
        get processing() { return processing.value },
        get foods() { return foods.value },
        tblCtrl,
    });
}

export { useFoodSearchHandler }
