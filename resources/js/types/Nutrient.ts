import { Id } from "./Id";
import { Timestamp } from "./Timestamp";

interface Nutrient {
    id: Id
    usda_id?: Id
    group: string
    key: string
    unit: string
    value: number
    created_at?: Timestamp
    updated_at?: Timestamp
}

export { Nutrient }
