import { Id } from "./Id";
import { Food } from "./Food";
import { Timestamp } from "./Timestamp";

interface Ingredient {
    id: Id
    value: number
    created_at: Timestamp
    updated_at: Timestamp
    food: Food
}

export { Ingredient }
