import { Id } from "./Id";
import { Nutrient } from "./Nutrient";
import { Timestamp } from "./Timestamp";

interface Food {
    id: Id
    name: string
    description?: string
    brand: string | null
    private: true
    carb_without_fiber: boolean
    import_datetime: Timestamp | null
    import_reference: string | null
    import_source: string | null
    imported: boolean
    ref_unit: string | null
    ref_value: number | null
    created_at: Timestamp
    updated_at: Timestamp
    deleted_at: Timestamp | null
    nutrients: Array<Nutrient>
}

export { Food }
