import { Id } from "./Id";
import { Ingredient } from "./Ingredient";
import { Timestamp } from "./Timestamp";

interface Template {
    id: Id
    name: string
    description: string | null
    portion: number
    created_at: Timestamp
    updated_at: Timestamp
    ingredients: Array<Ingredient>
}

export { Template }
