import type { Id } from "@/types/v2/Id";
import type { SimpleUser } from "@/types/v2/SimpleUser";
import type { TemplateFood } from "@/types/v2/TemplateFood";
import type { Timestamp } from "@/types/v2/Timestamp";
import type { Url } from "@/types/v2/Url";

interface Template {
    id: Id
    href: Url
    name: string
    description: string | null
    portion: number
    created_at: Timestamp
    updated_at: Timestamp
    creator: SimpleUser
    foods: Array<TemplateFood>
}

export type { Template }
