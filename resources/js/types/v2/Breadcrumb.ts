import type { Url } from "@/types/v2/Url";

interface Breadcrumb {
    text: string
    href?: Url
    active?: boolean,
}

export type { Breadcrumb }
