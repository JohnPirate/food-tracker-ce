import type { FoodImport } from "@/types/v2/FoodImport";
import type { Id } from "@/types/v2/Id";
import type { Url } from "@/types/v2/Url";
import type { FoodNutrient } from "@/types/v2/FoodNutrient";

interface FoodSearch {
    id: Id
    href: Url
    name: string
    brand: string | null
    ref_value: number
    ref_unit: string
    carb_without_fiber: boolean
    is_imported: boolean
    import: FoodImport | null
    nutrients: Array<FoodNutrient>
}

export type { FoodSearch }
