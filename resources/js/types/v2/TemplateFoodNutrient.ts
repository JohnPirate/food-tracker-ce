import type { Nutrient } from "@/types/v2/Nutrient";

type TemplateFoodNutrient = Nutrient;

export type { TemplateFoodNutrient }
