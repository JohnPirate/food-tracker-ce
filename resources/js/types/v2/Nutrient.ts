import type { Id } from "@/types/v2/Id";

interface Nutrient {
    id?: Id
    value: number
    unit: string
    group: string
    key: string
    display: {
        name: string
        group: string
    }
}

export type { Nutrient }
