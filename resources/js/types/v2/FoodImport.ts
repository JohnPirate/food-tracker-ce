import type { Id } from "@/types/v2/Id";

interface FoodImport {
    id: Id
    source: string
    reference: string
}

export type { FoodImport }
