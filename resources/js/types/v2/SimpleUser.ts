import type { Id } from "@/types/v2/Id";
import type { Url } from "@/types/v2/Url";

interface SimpleUser {
    id: Id
    username: string
    profile_photo_path: Url | null
}

export type { SimpleUser }
