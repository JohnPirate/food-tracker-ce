import type { FoodImport } from "@/types/v2/FoodImport";
import type { Id } from "@/types/v2/Id";
import type { TemplateFoodNutrient } from "@/types/v2/TemplateFoodNutrient";
import type { Url } from "@/types/v2/Url";

interface TemplateFood {
    id: Id
    href: Url
    name: string
    brand: string | null
    value: number
    ref_value: number
    ref_unit: string
    carb_without_fiber: boolean
    is_deleted: boolean
    is_imported: boolean
    import: FoodImport | null
    nutrients: Array<TemplateFoodNutrient>
}

export type { TemplateFood }
