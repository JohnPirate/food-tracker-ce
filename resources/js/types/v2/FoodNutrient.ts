import type { Nutrient } from "@/types/v2/Nutrient";

type FoodNutrient = Nutrient;

export type { FoodNutrient }
