/**
 * Pattern: Y-m-d H:i:s
 */
type Timestamp = string;

export type { Timestamp };
