import {COLOR_PRIMARY} from '../config';
import {trans} from '../utilities/localization';

export const default_data = [null, null, null, null, null, null, null];

const labelsWeekdays = {
    firstChar: trans('others.weekdays.first_char'),
    short: trans('others.weekdays.short'),
};

export const gridLeftMinWidth = 50;

export const createBasicConfig = () => {
    return {
        baseOption: {
            grid: {
                right: 0,
                top: 10,
                bottom: 20,
                left: gridLeftMinWidth,
            },
            xAxis: {
                type: 'category',
                axisLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
            },
            yAxis: {
                type: 'value',
                splitNumber: 2,
                axisLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
                splitLine: {
                    show: false,
                },
            },
            series: [
                {
                    data: default_data,
                    type: 'bar',
                    barWidth: 10,
                    itemStyle: {
                        color: COLOR_PRIMARY,
                        barBorderRadius: 100,
                    },
                    // markLine: {
                    //     data: [
                    //         {type: 'average'},
                    //     ],
                    //     animation: false,
                    //     lineStyle: {
                    //         type: 'solid',
                    //         color: '#999',
                    //         width: 2,
                    //     },
                    //     symbol: 'none',
                    // },
                },
            ],
        },
        media: [
            {
                option: {
                    xAxis: {
                        data: labelsWeekdays.firstChar,
                    },
                },
            },
            {
                query: {minWidth: 576},
                option: {
                    xAxis: {
                        data: labelsWeekdays.short,
                    },
                },
            },
        ],
    };
};
