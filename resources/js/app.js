/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

import {redirect_to} from './utilities/url_helpers';
window.redirect_to = redirect_to;

$(window).scroll(() => {
    if ($(window).scrollTop() > 0) {
        $('.navbar.fixed-top').addClass('shadow');
    } else {
        $('.navbar.fixed-top').removeClass('shadow');
    }
});
