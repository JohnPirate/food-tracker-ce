/*
 |---------------------------------------------------------------------------
 | Frontend settings
 |---------------------------------------------------------------------------
 */

export const LOGIN_REDIRECT = '/login';

export const URL_REDIRECT_FOOD_CATALOGUE = '/foods';

export const URL_FOOD_NEW = '/foods/new';
export const URL_FOOD_SHOW = '/foods/:id';
export const URL_FOOD_EDIT = '/foods/:id/edit';
export const URL_CONSUMED_FOODS_SHOW = '/consumed-foods';

export const URL_TEMPLATES_OVERVIEW = '/templates';
export const URL_TEMPLATE_NEW = '/templates/new';
export const URL_TEMPLATE_SHOW = '/templates/:id';
export const URL_TEMPLATE_EDIT = '/templates/:id/edit';
export const URL_TEMPLATE_CONSUME = '/templates/:id/consume';

export const MOMENT_JS_DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const MOMENT_JS_DATE_FORMAT = 'YYYY-MM-DD';
export const MOMENT_JS_TIME_FORMAT = 'HH:mm';
export const MOMENT_JS_TIME_FORMAT_EXTENDED = 'HH:mm:ss';
export const MOMENT_JS_CONSUMED_DATE_OVERVIEW_FORMAT = MOMENT_JS_DATE_FORMAT;
export const MOMENT_JS_CONSUMED_DATE_FORMAT = 'YYYY-MM-DD HH:mm';

export const FLATPICKR_DEFAULT_CONFIG = {
    time_24hr: true,
    locale: {
        firstDayOfWeek: 1
    }
};
export const FLATPICKR_CONSUMED_DATE_OVERVIEW_FORMAT = 'Y-m-d';
export const FLATPICKR_CONSUMED_DATE_FORMAT = 'Y-m-d H:i';
export const FLATPICKR_TIME_ONLY_FORMAT = 'H:i';

export const USDA_FOOD_REPORT_LINK = 'https://fdc.nal.usda.gov/fdc-app.html#/?query=ndbNumber::id';
export const USDA_FOOD_DATA_CENTRAL_REPORT_LINK = 'https://fdc.nal.usda.gov/fdc-app.html#/food-details/:id/nutrients';

export const FOOD_REF_UNIT_VALUES = ['g', 'ml'];

export const DEFAULT_REDIRECT_TIME = 580;
export const DEFAULT_REDIRECT_TIME_EXTENDED = 1000;

export const DEFAULT_DELAY_REQUEST_ACTIONS = 360;
export const REQUEST_DELAY = 120;

export const NO_GROUP_KEY = 'noGroup';

export const VALID_AMOUNT_MIN = 0;
export const VALID_AMOUNT_MAX = 10000;

export const NAMESPACE_GLOBAL_ALERT = 'gl';

export const MUST_HAVE_NUTRIENTS = [
    'energy',
    'total_lipid_fat',
    'carbohydrate_by_difference',
    'fiber_total_dietary',
    'protein',
];


export const COLOR_PRIMARY = '#007bff';
export const COLOR_CARB = '#dc3545';
export const COLOR_FAT = '#ffc107';
export const COLOR_PROTEIN = '#17a2b8';


export const PROGRESS_COLOR_ALERT = 'rgba(220,53,69,1)';
export const PROGRESS_COLOR_WARNING = 'rgba(252,176,69,1)';
export const PROGRESS_COLOR_SUCCESS = 'rgba(40,167,69,1)';
