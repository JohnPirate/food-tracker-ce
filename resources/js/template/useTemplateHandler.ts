import { AxiosError, isAxiosError } from "axios";
import { httpClient } from "@/utilities/httpClient";
import { computed, reactive } from "vue";
import { createAsyncDelay } from "styriabytes-javascript-lib";
import { createEventHandler } from "@/events/createEventHandler";
import type { MessageResponse } from "@/response/MessageResponse";
import type { Template } from "@/types/v2/Template";
import type { TemplateFood } from "@/types/v2/TemplateFood";
import type { TemplateFoodNutrient } from "@/types/v2/TemplateFoodNutrient";

function createNutrientsMap({ foods = [] }: { foods: TemplateFood[] }) {
    const nutrientsKeyMap: Record<string, TemplateFoodNutrient> = {};
    foods.forEach((food) => {
        food.nutrients.forEach((nutrient) => {
            if (!nutrientsKeyMap[nutrient.key]) {
                nutrientsKeyMap[nutrient.key] = { ...nutrient };
                nutrientsKeyMap[nutrient.key].value = 0;
            }
            // FIXME(ssandriesser): check if nutrients has same unit, otherwise harmonize to next
            //                      highest unit. Example: from 'mg' to 'g'

            // Avoid unnecessary javascript calculations
            if (food.ref_value === food.value) {
                nutrientsKeyMap[nutrient.key].value += nutrient.value;
            } else {
                nutrientsKeyMap[nutrient.key].value += (nutrient.value / food.ref_value * food.value);
            }
        });
    });
    return nutrientsKeyMap;
}

function useTemplateHandler({ templateId, template = null }: { templateId: string | number, template?: Template | null }) {

    const temp = template ? { ...template } : null;

    const eventHandler = createEventHandler<any>();

    const templateHandler = reactive({

        on: eventHandler.on,
        emit: eventHandler.emit,

        template: temp,
        initialized: false,
        processing: false,
        requesting: {
            fetchTemplate: false,
            deleteTemplate: false,
            copyTemplate: false,
        },
        get isInitialized(): boolean {
            return this.initialized;
        },

        get nutrientsKeyMap() {
            return computed(() => {
                if (!this.template) {
                    return {};
                }
                return createNutrientsMap({ foods: this.template.foods });
            });
        },

        async init(): Promise<void> {
            if (!this.initialized) {

                await this.fetchTemplate({ templateId });

                this.initialized = true;
            }
        },

        async fetchTemplate({ templateId }: { templateId: string | number }): Promise<void> {
            try {
                this.processing = true;
                this.requesting.fetchTemplate = true;

                const requestDelayer = createAsyncDelay();

                const { data } = await httpClient.get<Template>(route("api.v2.get_template", templateId));

                await requestDelayer.waitMin(250);
                this.template = data;

                this.emit("success");
                this.emit("fetchTemplate::success", data);

            } catch(e) {
                this.emit("error", e);
                this.emit("fetchTemplate::error", e);
                if (isAxiosError(e)) {
                    this.emit("request-error", e as AxiosError);
                    this.emit("fetchTemplate::request-error", e as AxiosError);
                }
            } finally {
                this.processing = false;
                this.requesting.fetchTemplate = false;
            }
        },

        async reFetchTemplate(): Promise<void> {
            if (this.initialized) {
                // @ts-expect-error if initialized, a template is there
                await this.fetchTemplate({ templateId: this.template.id });
            }
        },

        async deleteTemplate({ templateId }: { templateId: string | number }): Promise<void> {
            try {

                this.processing = true;
                this.requesting.deleteTemplate = true;

                const requestDelayer = createAsyncDelay();

                const { data} = await httpClient.delete<MessageResponse>(route("api.v1.delete_template", templateId));

                await requestDelayer.waitMin(250);

                this.emit("success");
                this.emit("deleteTemplate::success", data);

            } catch(e) {
                this.emit("error", e);
                this.emit("deleteTemplate::error", e);
                if (isAxiosError(e)) {
                    this.emit("request-error", e as AxiosError);
                    this.emit("deleteTemplate::request-error", e as AxiosError);
                }
            } finally {
                this.processing = false;
                this.requesting.deleteTemplate = false;
            }
        },

        async copyTemplate({ templateId }: { templateId: string | number }): Promise<void> {
            try {

                this.processing = true;
                this.requesting.copyTemplate = true;

                const requestDelayer = createAsyncDelay();

                const { data} = await httpClient.post<MessageResponse>(route("api.v1.copy_template", templateId));

                await requestDelayer.waitMin(250);

                this.emit("success");
                this.emit("copyTemplate::success", data);

            } catch(e) {
                this.emit("error", e);
                this.emit("copyTemplate::error", e);
                if (isAxiosError(e)) {
                    this.emit("request-error", e as AxiosError);
                    this.emit("copyTemplate::request-error", e as AxiosError);
                }
            } finally {
                this.processing = false;
                this.requesting.copyTemplate = false;
            }
        },
    });

    return templateHandler;
}

export { useTemplateHandler, createNutrientsMap };
