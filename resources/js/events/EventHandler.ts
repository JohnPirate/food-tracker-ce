interface EventHandler<T> {
    on<DType>(eventKey: string, cb: (data: DType) => void): T & EventHandler<T>
    emit<DType = any>(eventKey: string, data?: DType | null): void
}

export type { EventHandler }
