import type { EventHandler } from "@/events/EventHandler";
import { has } from "@/utilities/helpers";

function createEventHandler<T>(): EventHandler<T> {

    const callbackFunctions: { [p: string]: Array<Function> } = {};

    return {
        on(eventKey, cb): T & EventHandler<T> {
            if (!has.call(callbackFunctions, eventKey)) {
                callbackFunctions[eventKey] = [];
            }
            callbackFunctions[eventKey].push(cb);

            return this as T & EventHandler<T>;
        },
        emit(eventKey, data = null) {
            if (has.call(callbackFunctions, eventKey)) {
                for (const cb of callbackFunctions[eventKey]) {
                    cb(data);
                }
            }
        },
    };
}

export { createEventHandler };
