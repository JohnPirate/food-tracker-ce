import { reactive, watch } from "vue";

type CtrlParams = object;

function createTableAttributes<TFilter extends CtrlParams, TSort extends CtrlParams>({ sort, filter }: { sort: TSort; filter: TFilter }): Record<keyof TFilter | keyof TSort, any> {
    const ctrlAttributes: Record<keyof TFilter | keyof TSort, any> = {} as Record<keyof TFilter | keyof TSort, any>;
    for (const sortKey in sort) {
        if (sort[sortKey]) {
            ctrlAttributes[sortKey] = sort[sortKey];
        }
    }
    for (const filterKey in filter) {
        if (filter[filterKey]) {
            ctrlAttributes[filterKey] = filter[filterKey];
        }
    }
    return ctrlAttributes;
}

type UseTableCtrlParams<TFilter extends CtrlParams, TSort extends CtrlParams> = {
    filter?: TFilter
    sort?: TSort
    onChange?: (p: Record<keyof TFilter | keyof TSort, any>) => void | Promise<void>
    onRefresh?: (p: Record<keyof TFilter | keyof TSort, any>) => void | Promise<void>
}

function useTableCtrl<TFilter extends CtrlParams, TSort extends CtrlParams>({ filter = {} as TFilter, sort = {} as TSort, onChange = () => {}, onRefresh = () => {} }: UseTableCtrlParams<TFilter, TSort> = {}) {

    let onChangeHandler: (p: Record<keyof TFilter | keyof TSort, any>) => void = onChange
    let onRefreshHandler: (p: Record<keyof TFilter | keyof TSort, any>) => void = onRefresh

    const tableCtrl = reactive({

        filter,
        sort,

        onChange(cb: (p: Record<keyof TFilter | keyof TSort, any>) => {}): void | Promise<void> {
            onChangeHandler = cb;
        },

        onRefresh(cb: (p: Record<keyof TFilter | keyof TSort, any>) => {}): void | Promise<void> {
            onRefreshHandler = cb;
        },

        refresh(): void | Promise<void> {
            onRefreshHandler(createTableAttributes<TFilter, TSort>({
                sort: this.sort,
                filter: this.filter,
            }));
        }
    });

    watch(tableCtrl, value => {
        onChangeHandler(createTableAttributes<TFilter, TSort>({
            sort: value.sort as TSort,
            filter: value.filter as TFilter,
        }));
    });

    return tableCtrl;
}

export { useTableCtrl };
