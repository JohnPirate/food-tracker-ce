import { createAsyncDelay } from "styriabytes-javascript-lib";
import { isFormError } from "@/form/isFormError";
import { Axios, AxiosRequestConfig, isAxiosError } from "axios";
import { cloneDeep, isEqual } from "lodash-es";
import { reactive, UnwrapNestedRefs, watch} from "vue";
import { useFormErrors } from "@/form/useFormErrors";
import type { VueFormErrors } from "@/form/VueFormErrors";

type FormRecord = object;

interface VueForm<TForm extends FormRecord> {
    isDirty: boolean
    errors: VueFormErrors,
    processing: boolean,
    data(): TForm,
    transform(callback: (data: TForm) => object): this,
    defaults(): this,
    defaults(field: keyof TForm, value: string): this,
    defaults(fields:  Partial<TForm>): this,
    reset(...fields: (keyof TForm)[]): this,
    submit<TResponse>(method: string, url: string, options?: AxiosRequestConfig): Promise<TResponse>,
    get<TResponse>(url: string, options?: AxiosRequestConfig): Promise<TResponse>
    post<TResponse>(url: string, options?: AxiosRequestConfig): Promise<TResponse>
    put<TResponse>(url: string, options?: AxiosRequestConfig): Promise<TResponse>
    patch<TResponse>(url: string, options?: AxiosRequestConfig): Promise<TResponse>
    delete<TResponse>(url: string, options?: AxiosRequestConfig): Promise<TResponse>
}

function _getDefaultTransform<T extends FormRecord>() {
    return (data: T): object => { return { ...data } };
}

function useForm<T extends FormRecord>({ httpClient, data }: { httpClient: Axios, data: T }): UnwrapNestedRefs<T & VueForm<T>> {

    let defaults = cloneDeep(data);
    let transform = _getDefaultTransform<T>();

    let form = reactive<T & VueForm<T>>({
        ...data,
        isDirty: false,
        errors: useFormErrors(),
        processing: false,
        data() {
            return (Object.keys(defaults) as Array<keyof T>).reduce((carry, key) => {
                // @ts-expect-error
                carry[key] = this[key];
                return carry;
            }, {} as Partial<T>) as T;
        },
        transform(callback) {
            transform = callback;
            return this;
        },
        defaults(fieldOrFields?: keyof T | Partial<T>, maybeValue?: string) {
            if (typeof fieldOrFields === 'undefined') {
                defaults = this.data();
            } else {
                defaults = Object.assign(
                    {},
                    cloneDeep(defaults),
                    typeof fieldOrFields === 'string' ? { [fieldOrFields]: maybeValue } : fieldOrFields,
                );
            }
            this.isDirty = false;
            return this;
        },
        reset(...fields: unknown[]) {
            let clonedDefaults = cloneDeep(defaults)
            if (fields.length === 0) {
                Object.assign(this, clonedDefaults)
            } else {
                Object.assign(
                    this,
                    Object.keys(clonedDefaults)
                        .filter((key) => fields.includes(key))
                        .reduce((carry, key) => {
                            // @ts-expect-error
                            carry[key] = clonedDefaults[key]
                            return carry
                        }, {}),
                )
            }

            return this
        },
        async submit<TResponse>(method: string, url: string, options: AxiosRequestConfig = {}): Promise<TResponse> {
            try {

                const data = transform(this.data());

                // reset transform
                transform = _getDefaultTransform();

                this.processing = true;

                const delayer = createAsyncDelay();

                let resp = await httpClient.request({
                    ...options,
                    method,
                    url,
                    data: method === 'get' ? {} : data,
                    params: method === 'get' ? data : {},
                });

                await delayer.waitMin(350);

                return resp.data;

            } catch (e) {
                if (isAxiosError(e)) {
                    if (isFormError(e)) {
                        this.errors.set(e.response?.data.errors);
                        //throw Error("validation errors");
                    }
                }
                throw e;
            } finally {
                this.processing = false;
            }
        },
        async get<TResponse>(url: string, options: AxiosRequestConfig = {}): Promise<TResponse> {
            return this.submit<TResponse>('get', url, options);
        },
        async post<TResponse>(url: string, options: AxiosRequestConfig = {}): Promise<TResponse> {
            return this.submit<TResponse>('post', url, options);
        },
        async put<TResponse>(url: string, options: AxiosRequestConfig = {}): Promise<TResponse> {
            return this.submit<TResponse>('put', url, options);
        },
        async patch<TResponse>(url: string, options: AxiosRequestConfig = {}): Promise<TResponse> {
            return this.submit<TResponse>('patch', url, options);
        },
        async delete<TResponse>(url: string, options: AxiosRequestConfig = {}): Promise<TResponse> {
            return this.submit<TResponse>('delete', url, options);
        },
    });

    watch(
        form,
        (newValue) => {
            form.isDirty = !isEqual(form.data(), defaults);
        },
        { immediate: true, deep: true },
    )

    return form;
}

export {
    useForm,
    VueForm,
    FormRecord,
}
