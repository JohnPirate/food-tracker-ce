type ErrorBag = { [p: string]: Array<string> };

export type { ErrorBag }
