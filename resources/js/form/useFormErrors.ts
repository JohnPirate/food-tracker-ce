import Vue, { reactive } from "vue";
import { has } from "@/utilities/helpers";
import { VueFormErrors } from "@/form/VueFormErrors";

function useFormErrors() {
    return reactive<VueFormErrors>({
        errors: {},
        any() {
            return Object.keys(this.errors).length > 0;
        },
        has(key) {
            return has.call(this.errors, key);
        },
        state(key) {
            return this.has(key) ? false : null;
        },
        get(key): string | null {
            if (this.errors[key]) {
                return this.errors[key][0];
            }
            return null;
        },
        add(key, message) {
            if (!has.call(this.errors, key)) {
                this.errors[key] = [];
            }
            this.errors[key].push(message);
        },
        set(errors) {
            this.errors = { ...errors };
        },
        clear(key = null) {
            if (key !== null) {
                Vue.delete(this.errors, key);
                return;
            }
            this.errors = {};
        }
    });
}

export { useFormErrors }
