import type { ErrorBag } from "@/form/ErrorBag";

interface VueFormErrors {
    errors: ErrorBag,
    any(): boolean
    has(key: string): boolean
    state(key: string): false | null
    get(key: string): string | null
    add(key: string, message: string): void
    set(errors: ErrorBag): void
    clear(key?: string | null): void
}

export type { VueFormErrors }
