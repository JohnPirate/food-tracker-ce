import type { AxiosError } from "axios";
import { ValidationErrorsResponse } from "@/response/ValidationErrorsResponse";

function isFormError(e: AxiosError<ValidationErrorsResponse>) {
    return e.response && e.response.status === 422 && e.response.data && e.response.data.errors && e.response.data.errors;
}

export { isFormError };
