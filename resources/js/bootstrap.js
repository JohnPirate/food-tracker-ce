import * as config from './config';
//window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');
    require('bootstrap');

} catch (e) {}

import { glHub } from './vue/utilities/EventHub';
window.$hub = glHub;

import Vue from 'vue';

Vue.prototype.$route = route;

import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

import Sticky from 'vue-sticky-directive';
Vue.use(Sticky);

import Multiselect from 'vue-multiselect';
Vue.component('multiselect', Multiselect);

window.Vue = Vue;

// Register global translation function
import { trans } from './utilities/localization';
window.$trans = trans;
Vue.prototype.$trans = trans;

// Register moment js as Vue extension
import moment from 'moment';
moment.locale(document.documentElement.lang);
Vue.prototype.$moment = moment;
window.$moment = moment;

// Register global Alert
import './vue/shared/global_alert/index';
import { glAlert } from './vue/utilities/Alert';
window.$alert = glAlert;
Vue.prototype.$alert = glAlert;
Vue.prototype.$locale = document.documentElement.lang || 'de';

import flatpickr from 'flatpickr';
flatpickr.setDefaults(config.FLATPICKR_DEFAULT_CONFIG);


/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
