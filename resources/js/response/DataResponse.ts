interface DataResponse<Type> {
    data: Array<Type>
}

export type { DataResponse }
