import type { MessageResponse } from "@/response/MessageResponse";

interface CopyTemplateResponse extends MessageResponse {
    data: {
        template_id: string | number;
    }
}

export type { CopyTemplateResponse };
