interface MessageResponse {
    message: string
}

export type { MessageResponse }
