interface ValidationErrorsResponse {
    errors: Array<{[key: string]: string[] }>
}

export type { ValidationErrorsResponse }
