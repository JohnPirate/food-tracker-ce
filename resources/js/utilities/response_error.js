/**
 * Handles the response errors of an axios request
 *
 * @link https://github.com/axios/axios#handling-errors
 *
 * @param error
 */
import {redirect_to} from './url_helpers';
import {
    LOGIN_REDIRECT,
    DEFAULT_REDIRECT_TIME_EXTENDED,
} from '../config';

export function handle_response_error(error) {

    if (error.response) {
        const response = error.response;
        // Authentication expired
        if (response.status === 419) {

            // Visual message (alert)
            window.$alert.warning($trans('messages.authentication_expired'));
            redirect_to({
                url: LOGIN_REDIRECT,
                delay: DEFAULT_REDIRECT_TIME_EXTENDED,
            });
        }
        else if (response.status === 401) {

            // Visual message (alert)
            window.$alert.warning(response.data.message);
            redirect_to({
                url: LOGIN_REDIRECT,
                delay: DEFAULT_REDIRECT_TIME_EXTENDED,
            });
        }
        // Data not found
        else if (response.status === 404) {

            // Visual message (alert)
            window.$alert.warning($trans('messages.not_found'));
        }
        // Visual messages for client errors
        else if (response.status >= 400 && response.status < 500) {

            // Visual message (alert)
            window.$alert.warning(response.data.message);
        }
        // Visual messages for server errors
        else if (response.status >= 500) {

            // Visual message (alert)
            window.$alert.error(response.data.message);
        }
    } else if (error.request) {
        console.error(error.request);
    } else {
        console.error(error.message);
    }

}
