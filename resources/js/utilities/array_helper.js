/**
 * Checks if a value exists in an array
 *
 * @todo support for arrays and objects
 *
 * @param {String|Number} value
 * @param {Array}         array
 * @returns {Boolean}
 */
export function in_array(value, array) {
    return array.indexOf(value) !== -1;
}

/**
 * @param {array} arr
 *
 * @returns {array}
 */
export function array_remove_empty_items (arr) {
    return ! arr.length ? arr : arr.filter(function (item) {
        if (item) {
            return item;
        }
    });
}