export default class {
    /**
     * Create a new Errors instance.
     */
    constructor(errors = {}) {
        this.record(errors);
    }

    /**
     * Determine if an errors exists for the given field.
     *
     * @param {string} field
     */
    has(field) {
        return Object.prototype.hasOwnProperty.call(this.errors, field);
    }

    /**
     * vue-bootstrap wrapper for has()
     *
     * @param {string} field
     *
     * @returns {boolean|null}
     */
    state(field) {
        return this.has(field) ? false : null;
    }

    /**
     * Determine if we have any errors.
     */
    any() {
        return Object.keys(this.errors).length > 0;
    }

    /**
     * Retrieve the error message for a field.
     *
     * @param {string} field
     */
    get(field) {
        if (this.has(field)) {
            return this.errors[field][0];
        }
    }

    /**
     * Record the new errors.
     *
     * @param {object} errors
     */
    record(errors) {
        this.errors = errors;
    }

    /**
     * Clear one or all error fields.
     *
     * @param {string|null} field
     */
    clear(field = null) {
        if (field) {
            delete this.errors[field];
            return;
        }
        const _self = this;
        Object.keys(this.errors).forEach((key) => {
            delete _self.errors[key];
        });
    }
}
