import { eachRight, get, replace } from 'lodash';

/**
 * @param {string} str
 * @param {Record<string, unknown>} args
 *
 * @return {string}
 */
function trans(str, args = {}) {
    let value = get(window.i18n, str, str);
    eachRight(args, (paramVal, paramKey) => {
        value = replace(value, `:${paramKey}`, paramVal);
    });
    return value;
}

export { trans }
