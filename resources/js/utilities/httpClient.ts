import axios from 'axios';

const httpClient = axios.create({
    timeout: 10000,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
    },
});

export { httpClient };
