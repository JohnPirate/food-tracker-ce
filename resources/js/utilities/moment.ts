import moment from "moment";

moment.locale(document.documentElement.lang);

export { moment }
