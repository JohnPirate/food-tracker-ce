import { AxiosResponse } from "axios";

function isFormError(response: AxiosResponse): boolean {
    return response.status === 422 && response.data?.errors?.length > 0;
}

export { isFormError }
