import { isFunction } from 'lodash-es';

function wrapMsg (msg: string): string {
    return "============\n\n" + msg.trim() + "\n\n============";
}

interface Modal {
    confirm: ({ msg, fn = null }: { msg: string, fn?: Function | null}) => boolean
    alert: ({ msg }: { msg: string }) => void
}

function createModal(): Modal {
    return {
        confirm({ msg, fn = null }: { msg: string, fn?: Function | null}): boolean {
            if (!window.confirm(wrapMsg(msg))) {
                if (isFunction(fn)) {
                    fn();
                }
                return false;
            }
            return true;
        },
        alert({ msg }: { msg: string }): void {
            window.alert(wrapMsg(msg));
        }
    };
}

export { createModal }
