/**
 * https://github.com/airbnb/javascript#objects--prototype-builtins
 *
 * @type {(v: PropertyKey) => boolean}
 */
export const has = Object.prototype.hasOwnProperty;

/**
 * @param {function} item
 *
 * @returns {boolean}
 */
export function is_function(item) {
    if (typeof item === 'function') {
        return true;
    }
    const type = Object.prototype.toString(item);
    return type === '[object Function]' || type === '[object GeneratorFunction]';
}

/**
 * Check if the given value is an instance of Error
 * @param {*} val
 *
 * @return {boolean}
 */
export function is_error(val) {
    return val instanceof Error;
}

/**
 * Check if the given value is a Number
 * @param {*} n
 *
 * @return {boolean}
 */
export function is_number(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Check if the given value is a lower then a specific value
 * @param {Number} n
 * @param {Number} val
 *
 * @return {boolean}
 */
export function is_lt(n, val) {
    return n < val;
}

/**
 * Check if the given value is a lower then a specific value or equal
 * @param {Number} n
 * @param {Number} val
 *
 * @return {boolean}
 */
export function is_lte(n, val) {
    return n <= val;
}

/**
 * Check if the given value is a greater then a specific value
 * @param {Number} n
 * @param {Number} val
 *
 * @return {boolean}
 */
export function is_gt(n, val) {
    return n > val;
}

/**
 * Check if the given value is a greater then a specific value or equal
 * @param {Number} n
 * @param {Number} val
 *
 * @return {boolean}
 */
export function is_gte(n, val) {
    return n >= val;
}

/**
 * @param {number|string} selector
 * @param {array|number}  offsets
 *
 * @returns {void}
 */
export function scroll_to(selector, offsets) {
    let elem = $(selector);
    let elem_offset = elem.offset();

    if (! Array.isArray(offsets)) {
        offsets = [offsets];
    }

    offsets.forEach(function (offset) {
        if (! isNaN(offset)) {
            elem_offset.top -= offset;
        }
    });

    $('html, body').animate({
        scrollTop: elem_offset.top
    }, 200);
}

/**
 * @param {Object} obj
 * @param {String} prop
 * @param {*}      def
 *
 * @return {*}
 */
export function get_prop_value(obj, prop, def = null) {
    return has.call(obj, prop) ? obj[prop] : def;
}

/**
 * @param {*} value
 * @param {Number|String|Boolean|Array} primitive Javascript primitives
 * @param {Boolean} nullable
 * @return {*}
 */
export function cast({value = null, primitive = null, nullable = false} = {}) {
    if (nullable && value === null) {
        return null;
    }
    return primitive(value);
}

/**
 * @param {Number} value
 * @param {Number} decimals
 *
 * @return {Number}
 */
export function round(value, decimals = 2) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

/**
 * Calls a function with a minimum of delay
 *
 * @param {Number} refTime
 * @param {Number} minDelay
 * @param {Function} callback
 */
export function minDelay(refTime, minDelay, callback) {
    const diff = Date.now() - refTime;
    if (diff >= minDelay) {
        callback();
        return;
    }
    setTimeout(() => {
        callback();
    }, minDelay - diff);
}
