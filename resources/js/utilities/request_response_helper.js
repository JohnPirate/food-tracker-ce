import { AxiosResponse } from "axios"
/**
 * @param {AxiosResponse} response
 * @return {boolean}
 */
function is_validation_error(response) {
    return response.status === 422;
}

export { is_validation_error }
