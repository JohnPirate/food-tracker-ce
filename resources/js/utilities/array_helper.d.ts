declare function in_array(value: any, arr: Array<any>): boolean;
declare function array_remove_empty_items(arr: Array<any>): any[];

export {
    array_remove_empty_items,
    in_array,
}
