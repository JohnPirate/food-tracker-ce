import _ from 'lodash';

/**
 * Create a url. It replaces the placeholder in the url
 * with the value in options.
 *
 * Bsp.:
 * let url = 'hello/:world';
 * let opt = { world: 1};
 * create_url(url, opt); // 'hello/1'
 *
 * @param {string} url
 * @param {Record<string, unknown>} options
 *
 * @returns {string}
 */
function create_url(url, options = {}) {
    // check if url is a string
    if (! _.isString(url)) { // TODO: use this without lodash library
        throw new Error('url must be a string');
    }

    // get all option keys
    let keys = Object.keys(options);

    keys.forEach((item) => {
        url = url.replace(new RegExp(`:${item}`), options[item]);
    });
    return url;
}

/**
 * @param {Record<string, unknown>} obj
 *
 * @returns {string}
 */
function create_url_params(obj) {
    const keys = Object.keys(obj);
    if (! keys.length) {
        return '';
    }
    let params = '?';
    keys.forEach((item) => {
        params += item + '=' + obj[item] + '&';
    });
    return params.slice(0, -1); // Remove the last '&'
}

/**
 * @param {string} url
 * @param {Record<string, any>} [options]
 * @param {Record<string, any>} [params]
 * @param {number} [delay] Time in ms
 */
function redirect_to({url = '', options = {}, params = {}, delay = 0} = {}) {

    let destination = create_url(url, options);
    if (Object.keys(params).length > 0) {
        destination += create_url_params(params);
    }
    setTimeout(() => {
        window.location = destination;
    }, delay);
}

export {
    create_url,
    create_url_params,
    redirect_to,
}
