import {
    MOMENT_JS_DATE_FORMAT,
} from '../config';
import moment from 'moment';

/**
 * @param {Number} daysFromNow
 * @returns {Object}
 */
export function create_date_map(daysFromNow = 7) {
    return create_date_map_from(moment(), daysFromNow);
}

/**
 *
 * @param {moment.Moment} from
 * @param {Number} days
 * @returns {Object}
 */
export function create_date_map_from(from, days = 7) {
    const dateMap = {};
    for (let iter = 0; iter < days; iter++) {
        dateMap[from.format(MOMENT_JS_DATE_FORMAT)] = null;
        from.subtract(1, 'days');
    }
    return dateMap;
}

/**
 * Creates a date map for a specific week
 * 0 := current week
 * > 0 := weeks in future
 * < 0 := weeks in past
 *
 * @param {Number} currentWeek
 * @returns {Object}
 */
export function create_week_date_map(currentWeek = 0) {
    const lastDayOfWeek = moment().isoWeekday(7 + currentWeek * 7);
    return create_date_map_from(lastDayOfWeek, 7);
}
