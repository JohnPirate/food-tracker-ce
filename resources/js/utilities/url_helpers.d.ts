declare function create_url(url: string, options?: Record<string, any>): string;

declare function create_url_params(obj: Record<string, any>): string;

declare function redirect_to({ url, options, params, delay }: {
    url: string;
    options?: Record<string, any>;
    params?: Record<string, any>;
    delay?: number;
} = {}): void;

export {
    create_url,
    create_url_params,
    redirect_to,
}
