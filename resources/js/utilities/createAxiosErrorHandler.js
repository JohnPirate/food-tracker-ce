import { AxiosError } from "axios";

/**
 * Resolves the HTTP status code handler based on the provided status code and handler mapping.
 *
 * @param {number|string} statusCode The HTTP status code.
 * @param {object} httpStatusCodeHandler A mapping of status code patterns to handler functions.
 *
 * @returns {function} The resolved handler function.
 */
const resolveHttpStatus = (statusCode, httpStatusCodeHandler) => {
    const statusCodeString = statusCode.toString();

    // 1. Exact match
    if (httpStatusCodeHandler[statusCode]) {
        return httpStatusCodeHandler[statusCode];
    }

    // 2. One wildcard
    const oneWildcardPattern = statusCodeString.slice(0, 2) + 'x';
    if (httpStatusCodeHandler[oneWildcardPattern]) {
        return httpStatusCodeHandler[oneWildcardPattern];
    }


    // 3. Two wildcard characters
    const twoWildcardPattern = statusCodeString.slice(0, 1) + 'xx';
    if (httpStatusCodeHandler[twoWildcardPattern]) {
        return httpStatusCodeHandler[twoWildcardPattern];
    }

    // 4. Default function
    return httpStatusCodeHandler.default;
};

const defaultHttpStatusCodeHandler = {
    default: (err) => {},
    onRequestError: (err) => {},
};

function createAxiosErrorHandler (commonActionHandlers = {}) {

    const actionHandlers = {
        ...defaultHttpStatusCodeHandler,
        ...commonActionHandlers,
    };

    return {

        actionHandlers,

        /**
         * Handles axios errors
         *
         * @param {AxiosError} err
         * @param {object} actionHandlers
         *
         * @returns {void}
         */
        handle(err, actionHandlers = {}) {
            const handlers = {
                ...this.actionHandlers,
                ...actionHandlers,
            };

            if (err.request) {
                handlers.onRequestError(err);
                return;
            }
            resolveHttpStatus(err.code, handlers)(err);
        },
    };
}

export { createAxiosErrorHandler }
