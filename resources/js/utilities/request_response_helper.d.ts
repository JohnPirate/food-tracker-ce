import { AxiosResponse } from "axios";

declare function is_validation_error(response: AxiosResponse): boolean;

export { is_validation_error }
