declare function trans(str: string, args?: Record<string, unknown>): string;

export { trans }
