import { createNutrient } from "@/nutrient/createNutrient";
import type { Nutrient } from "@/types/v2/Nutrient";

function createMacroNutriens({ nutrientsKeyMap = {} as Record<string, Nutrient> }: { nutrientsKeyMap?: Record<string, Nutrient> } = {}) {
    return {
        energy: nutrientsKeyMap.energy ?? createNutrient({ key: "energy", unit: "kcal" }),
        carb: nutrientsKeyMap.carbohydrate_by_difference ?? createNutrient({ key: "carbohydrate_by_difference" }),
        fat: nutrientsKeyMap.total_lipid_fat ?? createNutrient({ key: "total_lipid_fat" }),
        protein: nutrientsKeyMap.protein ?? createNutrient({ key: "protein" }),
    }
}

export { createMacroNutriens };
