import type { Nutrient } from "@/types/v2/Nutrient";
import { trans } from "@/utilities/localization"

interface CreateNutrientAttributes {
    id?: number
    value?: number
    unit?: string
    group?: string
    key?: string
    display?: {
        name: string
        group: string
    } | null
}

function createNutrient({
    id = 0,
    value = 0,
    unit = "g",
    group = "",
    key = "",
    display,
}: CreateNutrientAttributes = {}): Nutrient {

    let displayValue = display ?? {
        name: trans(`nutrients.groups.${key}`),
        group: trans(`nutrients.groups.${group}`),
    };

    return {
        id,
        value,
        unit,
        group,
        key,
        display: displayValue,
    };
}

export { createNutrient };
