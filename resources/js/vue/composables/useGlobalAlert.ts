import { Alert } from "@/vue/utilities/Alert";
import { AlertOptions } from "@/vue/utilities/AlertOptions";
import { AlertType } from "@/vue/utilities/AlertType";

function useGlobalAlert() {

    // @ts-expect-error
    const alert = window.$alert as Alert;

    return {
        success(message: string) { alert.success(message) },
        info(message: string) { alert.info(message) },
        warning(message: string) { alert.warning(message) },
        error(message: string) { alert.error(message) },
        danger(message: string) { alert.danger(message) },
        show(opt: AlertOptions) { alert.show(opt) },
        hide(type: AlertType | null) { alert.hide(type) },
    };
}

export { useGlobalAlert }
