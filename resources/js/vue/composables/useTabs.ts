import { reactive } from "vue";

function useTabs({ activeTab = "" }: { activeTab?: string } = {}) {
    return reactive({

        activeTab,

        isActive(tab: string) { return this.activeTab === tab },

        setActiveTab(activeTab: string) { return this.activeTab = activeTab },

    });
}

export { useTabs };
