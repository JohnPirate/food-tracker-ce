import {
    MOMENT_JS_CONSUMED_DATE_OVERVIEW_FORMAT,
    MOMENT_JS_DATE_TIME_FORMAT,
} from '../../config';
import { moment } from "../../utilities/moment";

function useDisplayHelpers() {
    return {
        displayDate(date: string) {
            return moment(date, MOMENT_JS_DATE_TIME_FORMAT)
                .format(MOMENT_JS_CONSUMED_DATE_OVERVIEW_FORMAT);
        },
    };
}

export { useDisplayHelpers }
