import { create_url } from '../../utilities/url_helpers';
import {
    URL_FOOD_EDIT,
    URL_FOOD_SHOW,
    URL_TEMPLATE_CONSUME,
    URL_TEMPLATE_EDIT,
    URL_TEMPLATE_NEW,
    URL_TEMPLATES_OVERVIEW,
    URL_TEMPLATE_SHOW,
    USDA_FOOD_REPORT_LINK,
    USDA_FOOD_DATA_CENTRAL_REPORT_LINK,
    URL_FOOD_NEW,
    URL_CONSUMED_FOODS_SHOW, URL_REDIRECT_FOOD_CATALOGUE,
} from '../../config';

function useLinks () {

    function urlToFoodCatalogue() {
        return URL_REDIRECT_FOOD_CATALOGUE;
    }
    function urlToJournal() {
        return URL_CONSUMED_FOODS_SHOW;
    }
    function urlToFoodNew() {
        return URL_FOOD_NEW;
    }
    function urlToFoodShow(id: number) {
        return create_url(URL_FOOD_SHOW, { id });
    }
    function urlToUsdaFoodReport(id: number) {
        return create_url(USDA_FOOD_REPORT_LINK, { id });
    }
    function urlToUsdaFoodDataCentralReport(id: number) {
        return create_url(USDA_FOOD_DATA_CENTRAL_REPORT_LINK, { id });
    }
    function urlToFoodEdit(id: number) {
        return create_url(URL_FOOD_EDIT, { id });
    }
    function urlToTemplateNew() {
        return URL_TEMPLATE_NEW;
    }
    function urlToTemplatesOverview() {
        return URL_TEMPLATES_OVERVIEW;
    }
    function urlToTemplateShow(id: number) {
        return create_url(URL_TEMPLATE_SHOW, { id });
    }
    function urlToTemplateEdit(id: number) {
        return create_url(URL_TEMPLATE_EDIT, { id });
    }
    function urlToTemplateConsume(id: number) {
        return create_url(URL_TEMPLATE_CONSUME, { id });
    }

    return {
        urlToFoodCatalogue,
        urlToJournal,
        urlToFoodNew,
        urlToFoodShow,
        urlToUsdaFoodReport,
        urlToUsdaFoodDataCentralReport,
        urlToFoodEdit,
        urlToTemplateNew,
        urlToTemplatesOverview,
        urlToTemplateShow,
        urlToTemplateEdit,
        urlToTemplateConsume,
    }
}

export { useLinks }
