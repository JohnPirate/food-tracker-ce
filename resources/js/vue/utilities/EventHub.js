import Vue from 'vue';

class EventHub {
    constructor() {
        this.vue = new Vue();
    }

    $emit(event, data = null) {
        this.vue.$emit(event, data);
    }

    $on(event, callback) {
        this.vue.$on(event, callback);
    }
}

const glHub = new EventHub();

export {
    EventHub,
    glHub,
}
