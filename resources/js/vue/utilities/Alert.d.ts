import { AlertOptions } from './AlertOptions';
import { AlertType } from './AlertType';
import { EventHub } from './EventHub';
declare class Alert {
    constructor(ref: string, hub: EventHub);
    success(message: string): void;
    info(message: string): void;
    warning(message: string): void;
    error(message: string): void;
    danger(message: string): void;
    show(data: AlertOptions): void;
    hide(type: AlertType | null): void;
}
declare const glAlert: Alert;
export { Alert, glAlert };
