import { glHub } from './EventHub';
import { NAMESPACE_GLOBAL_ALERT } from '../../config';

class Alert {

    /**
     * Alert constructor
     * @param {string} ref
     * @param {object} hub
     */
    constructor(ref, hub) {
        this.ref = ref;
        this.hub = hub;
    }

    /**
     * @param {string} message
     */
    success(message) {
        this.show({
            variant: 'success',
            msg: message,
            autoHide: true,
            dismissible: true
        });
    }

    /**
     * @param {string} message
     */
    info(message) {
        this.show({
            variant: 'info',
            msg: message,
            dismissible: true
        });
    }

    /**
     * @param {string} message
     */
    warning(message) {
        this.show({
            variant: 'warning',
            msg: message,
            dismissible: true
        });
    }

    /**
     * @param {string} message
     */
    error(message) {
        this.show({
            variant: 'danger',
            msg: message,
        });
    }

    /**
     * @param {string} message
     */
    danger(message) {
        this.error(message);
    }

    /**
     * @param {object} data
     */
    show(data) {
        this.hub.$emit(`alert::${this.ref}::show`, data);
    }

    hide(type = null) {
        this.hub.$emit(`alert::${this.ref}::hide`, type);
    }
}

const glAlert = new Alert(NAMESPACE_GLOBAL_ALERT, glHub);

export {
    Alert,
    glAlert,
}
