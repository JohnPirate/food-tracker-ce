declare class EventHub {
    constructor();
    $emit(event: string, ...args: any[]): void;
    $on(event: string | string[], callback: Function): void;
}
declare const glHub: EventHub;
export {
    EventHub,
    glHub,
};
