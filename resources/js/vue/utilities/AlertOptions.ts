import { AlertType } from './AlertType';

interface AlertOptions {
    autoHide: boolean
    dismissible: boolean
    fade: boolean
    msg: string
    variant: AlertType
}

export { AlertOptions }
