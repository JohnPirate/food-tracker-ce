import Vue from 'vue';
import DashboardView from './components/dashboard_view';

const elementId = 'dashboard-view';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            DashboardView,
        },

        data() {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            const userRef = JSON.parse(environmentsData.user);
            return {
                // data properties
                userRef,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    userRef: this.userRef,
                }
            });
        }
    });
});
