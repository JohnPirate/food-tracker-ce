import {get_prop_value} from '../../utilities/helpers';
import {Food} from './food';
import {BaseObject} from './base_object';
import {MOMENT_JS_CONSUMED_DATE_FORMAT} from '../../config';
import moment from 'moment';

export class ConsumedFood extends BaseObject {
    constructor({consume = {}, food = new Food()} = {}) {
        super();
        this.init(consume);
        this.food = food;
    }

    init(consume) {
        this.id = get_prop_value(consume, 'id', null);
        this.consumed_value = get_prop_value(consume, 'consumed_value', '').toString();
        this.consumed_at = moment(get_prop_value(consume, 'consumed_at', moment())).format(MOMENT_JS_CONSUMED_DATE_FORMAT);
        this.deleted = get_prop_value(consume, 'deleted', false);
        this.group = get_prop_value(consume, 'group', null);
    }

    calcEnergy() {
        return this.food.calcEnergy(this.consumed_value);
    }

    calcCarb() {
        return this.food.calcCarb(this.consumed_value);
    }

    calcFat() {
        return this.food.calcFat(this.consumed_value);
    }

    calcProtein() {
        return this.food.calcProtein(this.consumed_value);
    }

    exportStoreData() {
        const data = this.export([
            'consumed_value',
            'consumed_at',
            'group',
        ]);
        data.food_id = this.food.id;
        return data;
    }

    exportUpdateData() {
        return this.export([
            'consumed_value',
            'consumed_at',
            'group',
        ]);
    }
}
