import {
    get_prop_value,
    cast,
} from '../../utilities/helpers';
import {BaseObject} from './base_object';

export class Nutrient extends BaseObject {
    constructor(data = {}) {
        super();
        this.init(data);
    }

    init(data) {
        this.id = get_prop_value(data, 'id', null);
        this.value = cast({value: get_prop_value(data, 'value', null), primitive: Number, nullable: true});
        this.unit = get_prop_value(data, 'unit', 'g');
        this.group = get_prop_value(data, 'group', null);
        this.key = get_prop_value(data, 'key', null);
    }

    exportStoreData() {
        return this.export([
            'value',
            'unit',
            'key',
        ]);
    }
}

export function createNutrientsStore(data) {
    const nutrients = [];
    data.forEach((nutrient) => {
        nutrients.push(nutrient.exportStoreData());
    });
    return Object.freeze(nutrients);
}
