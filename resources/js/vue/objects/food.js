import {
    get_prop_value,
    cast,
    has,
} from '../../utilities/helpers';
import {BaseObject} from './base_object';
import {createNutrient, Nutrient} from './nutrient';

export class Food extends BaseObject {
    constructor({meta = {}, nutrients = null} = {}) {
        super();
        this.init(meta, nutrients);
    }

    init(meta, nutrients) {
        const _this = this;
        this.id = get_prop_value(meta, 'id', null);
        this.name = get_prop_value(meta, 'name', null);
        this.description = get_prop_value(meta, 'description', null);
        this.brand = get_prop_value(meta, 'brand', null);
        this.barcode = get_prop_value(meta, 'barcode', null);
        this.private = get_prop_value(meta, 'private', false);
        this.ref_value = cast({value: get_prop_value(meta, 'ref_value', 100), primitive: Number});
        this.ref_unit = get_prop_value(meta, 'ref_unit', 'g');
        this.carb_without_fiber = get_prop_value(meta, 'carb_without_fiber', false);
        this.created_at = get_prop_value(meta, 'created_at', null);
        this.updated_at = get_prop_value(meta, 'updated_at', null);
        this.editable = get_prop_value(meta, 'editable', false);
        this.imported = get_prop_value(meta, 'imported', false);
        this.import_source = get_prop_value(meta, 'import_source', null);
        this.import_reference = get_prop_value(meta, 'import_reference', null);
        this.import_datetime = get_prop_value(meta, 'import_datetime', null);

        if (Array.isArray(nutrients)) {
            this.nutrients = nutrients;
        } else if (has.call(meta, 'nutrients')) {
            this.nutrients = [];
            get_prop_value(meta, 'nutrients', []).forEach((nutrient) => {
                _this.nutrients.push(new Nutrient(nutrient));
            });
        } else {
            this.nutrients = [];
        }

        this.nutrientsKeyVal = {};
        if (this.nutrients.length) {
            const _this = this;
            this.nutrients.forEach((nutrient) => {
                _this.nutrientsKeyVal[nutrient.key] = nutrient;
            });
        }
    }

    get energy () {
        if (!has.call(this.nutrientsKeyVal, 'energy')) {
            this.nutrientsKeyVal.energy = new Nutrient();
        }
        return this.nutrientsKeyVal.energy;
    }

    get carb () {
        if (!has.call(this.nutrientsKeyVal, 'carbohydrate_by_difference')) {
            this.nutrientsKeyVal.carbohydrate_by_difference = new Nutrient();
            this.nutrientsKeyVal.fiber_total_dietary = new Nutrient();
        }
        if (!this.carb_without_fiber) {
            let nutrient = {
                key: this.nutrientsKeyVal.carbohydrate_by_difference.key,
                group: this.nutrientsKeyVal.carbohydrate_by_difference.group,
                value: this.nutrientsKeyVal.carbohydrate_by_difference.value,
                unit: this.nutrientsKeyVal.carbohydrate_by_difference.unit,
            };
            nutrient.value = !has.call(this.nutrientsKeyVal, 'fiber_total_dietary')
                ? nutrient.value
                : nutrient.value - this.nutrientsKeyVal.fiber_total_dietary.value;
            return nutrient;
        }
        return {
            key: 'carbohydrate',
            group: this.nutrientsKeyVal.carbohydrate_by_difference.group,
            value: this.nutrientsKeyVal.carbohydrate_by_difference.value,
            unit: this.nutrientsKeyVal.carbohydrate_by_difference.unit,
        };
    }

    get fat () {
        if (!has.call(this.nutrientsKeyVal, 'total_lipid_fat')) {
            this.nutrientsKeyVal.total_lipid_fat = new Nutrient();
        }
        return this.nutrientsKeyVal.total_lipid_fat;
    }

    get protein () {
        if (!has.call(this.nutrientsKeyVal, 'protein')) {
            this.nutrientsKeyVal.protein = new Nutrient();
        }
        return this.nutrientsKeyVal.protein;
    }

    calcEnergy(ref = null) {
        return parseFloat(ref &&  ref !== this.ref_value
            ? this.energy.value / this.ref_value * ref
            : this.energy.value);
    }

    calcCarb(ref = null) {
        return parseFloat(ref &&  ref !== this.ref_value
            ? this.carb.value / this.ref_value * ref
            : this.carb.value);
    }

    calcFat(ref = null) {
        return parseFloat(ref &&  ref !== this.ref_value
            ? this.fat.value / this.ref_value * ref
            : this.fat.value);
    }

    calcProtein(ref = null) {
        return parseFloat(ref &&  ref !== this.ref_value
            ? this.protein.value / this.ref_value * ref
            : this.protein.value);
    }

    exportStoreData() {
        return this.export([
            'name',
            'description',
            'brand',
            'barcode',
            'private',
            'ref_value',
            'ref_unit',
            'carb_without_fiber',
        ]);
    }
}

export function createFoodStore(food, nutrients) {
    const exp = food.exportStoreData();
    exp.nutrients = nutrients;
    return Object.freeze(exp);
}
