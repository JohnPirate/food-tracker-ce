export class BaseObject {
    export(props = []) {
        const _this = this;
        const ret = {};
        props.forEach((prop) => {
            // TODO(ssandriesser): handle error if prop not exists
            if (prop instanceof Object) {
                ret[prop.alias] = _this[prop.prop];
                return;
            }
            ret[prop] = _this[prop];
        });
        return ret;
    }
}
