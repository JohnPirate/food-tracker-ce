import Vue from 'vue';
import Vuex from 'vuex';
import nutrients from '../../store/modules/nutrients/index';
import {food} from '../../store/modules/food/index';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export const createStore = () => new Vuex.Store({
    modules: {
        food,
        nutrients
    },
    strict: debug
});
export default createStore();
