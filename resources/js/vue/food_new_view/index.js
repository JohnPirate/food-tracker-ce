import Vue from 'vue';
import FoodNewView from './components/food_new_view';
import store from './store/index';

const elementId = 'food-new-view';

document.addEventListener('DOMContentLoaded', function () {

    new Vue({

        el: `#${elementId}`,

        components: {
            FoodNewView,
        },

        store,

        
        data () {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                // data properties
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {

                }
            });
        }
    });
});