import { glHub } from '../../utilities/EventHub';
import GlobalAlert from '../alert';
import { NAMESPACE_GLOBAL_ALERT } from '../../../config';
import Vue from 'vue';

const elementId = 'global-alert';

document.addEventListener('DOMContentLoaded', function () {

    if (document.getElementById(elementId)) {

        new Vue({

            el: `#${elementId}`,

            components: {
                GlobalAlert
            },

            render(createElement) {
                return createElement(elementId, {
                    props: {
                        alertRef: NAMESPACE_GLOBAL_ALERT,
                        hub: glHub
                    }
                });
            }
        });
    }

});
