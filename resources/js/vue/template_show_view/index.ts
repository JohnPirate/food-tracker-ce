import Vue from 'vue';
import TemplateShowView from './components/template_show_view.vue';

declare module "vue/types/vue" {
    interface Vue {
        templateId: number
    }
}

const elementId = 'template-show-view';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            TemplateShowView,
        },

        data() {
            const environmentsData = document.getElementById(elementId)?.dataset;
            if (environmentsData === undefined || environmentsData.templateId === undefined) {
                throw Error("No environment data found");
            }
            return {
                templateId: parseInt(environmentsData.templateId),
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    templateId: this.templateId,
                }
            });
        }
    });
});
