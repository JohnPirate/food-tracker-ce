import axios from 'axios';
import {create_url, create_url_params} from '../../utilities/url_helpers';

/**
 * TemplatesService
 * @type {Object}
 */
export default {
    /**
     * @return {Promise<AxiosResponse<T>>}
     */
    fetchTemplates() {
        let url = `/api/v1/templates`;
        return axios.get(url);
    },

    /**
     * @param {Number}  id
     * @param {Boolean} full
     * @return {Promise<AxiosResponse<T>>}
     */
    fetchTemplate(id, full = false) {
        let url = `/api/v1/templates/:id`;
        if (full) {
            url += create_url_params({full});
        }
        return axios.get(create_url(url, {id}));
    },

    /**
     * @param {Number} id ID of the template
     * @param {Object} data
     * @return {Promise<AxiosResponse<T>>}
     */
    updateTemplate(id, data) {
        let url = `/api/v1/templates/:id`;
        return axios.put(create_url(url, {id}), data);
    },

    /**
     * @param {Object} data
     * @return {Promise<AxiosResponse<T>>}
     */
    createTemplate(data) {
        let url = `/api/v1/templates`;
        return axios.post(url, data);
    },

    /**
     * @param {Number} id ID of the template
     * @return {Promise<AxiosResponse<T>>}
     */
    deleteTemplate(id) {
        let url = `/api/v1/templates/:id`;
        return axios.delete(create_url(url, {id}));
    },

    /**
     * @param {Number} id ID of the template
     * @param {Array}  data
     * @return {Promise<AxiosResponse<T>>}
     */
    consumeTemplate(id, data) {
        let url = `/api/v1/templates/:id/consume`;
        return axios.post(create_url(url, {id}), data);
    },

    /**
     * @param {Number} id
     * @param {Object} data
     * @return {Promise<AxiosResponse<T>>}
     */
    addFoodToTemplate(id, data) {
        let url = `/api/v1/templates/:id/add-food`;
        return axios.post(create_url(url, {id}), data);
    },

    /**
     * @param {Number} id
     * @return {Promise<AxiosResponse<T>>}
     */
    copyTemplate(id) {
        let url = `/api/v1/templates/:id/copy`;
        return axios.post(create_url(url, {id}));
    },
}
