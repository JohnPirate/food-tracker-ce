import axios from 'axios';
import {create_url} from '../../utilities/url_helpers';

/**
 * FoodsService
 * @type {Object}
 */
export default {

    /**
     * @param {Number} id
     * @returns {AxiosPromise<any>}
     */
    getFood(id) {
        let url = `/api/v1/foods/${id}`;
        return axios.get(url);
    },

    /**
     * @param {Object} data
     * @return {AxiosPromise<any>}
     */
    storeFood(data) {
        let url = `/api/v1/foods`;
        return axios.post(url, data);
    },

    /**
     * @param {Number} id
     * @param {Object} data
     * @return {AxiosPromise<any>}
     */
    updateFood(id, data) {
        let url = `/api/v1/foods/:id`;
        return axios.put(create_url(url, {id}), data);
    },

    /**
     * @param {Number} id
     * @return {AxiosPromise<any>}
     */
    deleteFood(id) {
        let url = `/api/v1/foods/:id`;
        return axios.delete(create_url(url, {id}));
    },

    /**
     * @return {AxiosPromise<any>}
     */
    getFoods() {
        let url = `/api/v1/foods`;
        return axios.get(url);
    },

    /**
     * @param {String} q
     * @return {Promise<AxiosResponse<T>>}
     */
    search(q) {
        let url = `/api/v1/foods/search?q=${q}`;
        return axios.get(url);
    },
}
