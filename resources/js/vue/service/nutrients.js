import axios from 'axios';

/**
 * NutrientsService
 * @type {Object}
 */
export default {

    /**
     * @returns {AxiosPromise<any>}
     */
    getNutrients() {
        let url = `/api/v1/nutrients`;
        return axios.get(url);
    },

    /**
     * @returns {AxiosPromise<any>}
     */
    getNutrientUnits() {
        let url = `/api/v1/nutrients/units`;
        return axios.get(url);
    }
}
