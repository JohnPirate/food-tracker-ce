import axios from 'axios';
import {create_url, create_url_params} from '../../utilities/url_helpers';

/**
 * ConsumedFoodsService
 * @type {Object}
 */
export default {

    /**
     * @param {String} from
     * @param {String} to
     * @returns {AxiosPromise<any>}
     */
    getConsumedFoods(from, to) {
        let url = `/api/v1/consumed-foods`;
        return axios.get(url + create_url_params({from, to}));
    },

    /**
     * @returns {AxiosPromise<any>}
     */
    getConsumedFoodGroups() {
        let url = `/api/v1/consumed-foods/groups`;
        return axios.get(url);
    },

    /**
     * @param {String} from
     * @param {String} to
     * @returns {AxiosPromise<any>}
     */
    getConsumedFoodGroupsAt(from, to) {
        let url = `/api/v1/consumed-foods/groups`;
        return axios.get(url + create_url_params({from, to}));
    },

    /**
     * @param {Object} data
     * @returns {AxiosPromise<any>}
     */
    storeConsumedFood(data) {
        let url = `/api/v1/consumed-foods`;
        return axios.post(url, data);
    },

    /**
     * @param {Number} id
     * @param {Object} data
     * @returns {AxiosPromise<any>}
     */
    updateConsumedFood(id, data) {
        let url = `/api/v1/consumed-foods/:id`;
        return axios.put(create_url(url, {id}), data);
    },

    /**
     * @param {Number} id
     * @returns {AxiosPromise<any>}
     */
    deleteConsumedFood(id) {
        let url = `/api/v1/consumed-foods/:id`;
        return axios.delete(create_url(url, {id}));
    },

    /**
     * @param {Object} data
     * @returns {AxiosPromise<any>}
     */
    copyConsumedFoodGroup(data) {
        let url = `/api/v1/consumed-food-groups/copy`;
        return axios.post(url, data);
    },

    /**
     * @param {Number} groupId
     * @param {String} consumedAt
     * @return {Promise<AxiosResponse<T>>}
     */
    deleteFoodGrouping(groupId, consumedAt) {
        let url = `/api/v1/consumed-food-groups/delete-grouping`;
        return axios.post(url, {
            group_id: groupId,
            consumed_at: consumedAt,
        });
    },

    createTemplateFromFoodGrouping(data) {
        let url = `/api/v1/consumed-food-groups/create-template`;
        return axios.post(url, data);
    },
}
