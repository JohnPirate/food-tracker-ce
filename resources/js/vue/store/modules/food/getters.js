export const food = (state, getters) => {
    return state.foodRef;
};

export const errors = (state, getters) => {
    return state.__errors;
};

export const hasAnyErrors = (state, getters) => {
    return getters.errors.any();
};

export const isFoodLoading = (state, getters) => {
    return state.foodRequest;
};

export const isStoreFoodLoading = (state, getters) => {
    return state.storeFoodRequest;
};

export const isUpdateFoodLoading = (state, getters) => {
    return state.updateFoodRequest;
};

export const isDeleteFoodLoading = (state, getters) => {
    return state.deleteFoodRequest;
};

export const foods = (state, getters) => {
    return state.__foods;
};

export const areFoodsLoading = (state, getters) => {
    return state.foodsRequest;
};
