import * as actions from './actions';
import * as getters from './getters';
import mutations from './mutations';
import {state} from './state';

import { createHelpers } from 'vuex-map-fields';

import foodMeta from './forms/food_meta';
import foodNutrients from './forms/food_nutrients';

const modules = {
    foodMeta,
    foodNutrients,
};

export const {mapFields: mapFoodMetaFields} = createHelpers({
    getterType: `food/foodMeta/getField`,
    mutationType: `food/foodMeta/updateField`,
});

export const {mapMultiRowFields: mapNutrientMultiRowFields} = createHelpers({
    getterType: `food/foodNutrients/getField`,
    mutationType: `food/foodNutrients/updateField`,
});

export const {mapFields: mapFoodNutrientFields} = createHelpers({
    getterType: `food/foodNutrients/getField`,
    mutationType: `food/foodNutrients/updateField`,
});

export const food = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
    modules,
};
