import { getField, updateField } from 'vuex-map-fields';

import {
    MUST_HAVE_NUTRIENTS,
} from '../../../../../config';

import * as types from '../mutation_types';

import {Nutrient} from '../../../../objects/nutrient';
import {cloneDeep} from 'lodash';

const initNutrients = [
    new Nutrient({
        key: 'energy',
        unit: 'kcal',
        value: null,
    }),
    new Nutrient({
        key: 'total_lipid_fat',
        unit: 'g',
        value: null,
    }),
    new Nutrient({
        key: 'fatty_acids_total_saturated',
        unit: 'g',
        value: null,
    }),
    new Nutrient({
        key: 'carbohydrate_by_difference',
        unit: 'g',
        value: null,
    }),
    new Nutrient({
        key: 'sugars_total',
        unit: 'g',
        value: null,
    }),
    new Nutrient({
        key: 'fiber_total_dietary',
        unit: 'g',
        value: null,
    }),
    new Nutrient({
        key: 'protein',
        unit: 'g',
        value: null,
    }),
    new Nutrient({
        key: 'salt',
        unit: 'g',
        value: null,
    }),
];

const musthaveNutrients = MUST_HAVE_NUTRIENTS;

const actions = {
    addNutrient({commit}, data) {
        commit(types.ADD_NUTRIENT, data);
    },
    removeNutrient({commit}, data) {
        commit(types.REMOVE_NUTRIENT, data);
    },
    setFoodNutrientsForm({commit}, data) {
        commit(types.SET_FOOD_NUTRIENTS_FORM, data);
    },
    resetNutrientsForm({commit}) {
        commit(types.RESET_NUTRIENTS_FORM);
    },
};

const mutations = {
    updateField,

    [types.ADD_NUTRIENT](state, data) {
        state.rows.push(state.formNutrient);
        state.formNutrient = new Nutrient();
    },

    [types.REMOVE_NUTRIENT](state, data) {
        state.rows.splice(state.rows.indexOf(data), 1);
    },

    [types.SET_FOOD_NUTRIENTS_FORM](state, data) {
        state.rows = [];
        data.forEach((nutrient) => {
            state.rows.push(new Nutrient(nutrient));
        });
        state.resetRows = cloneDeep(state.rows);
    },

    [types.RESET_NUTRIENTS_FORM](state) {
        state.rows = cloneDeep(state.resetRows);
    }
};

const getters = {
    getField,
};

const state = () => ({
    resetRows: cloneDeep(initNutrients),
    musthaveNutrients,
    rows: cloneDeep(initNutrients),
    formNutrient: new Nutrient(),
});

export default {
    namespaced: true,
    actions,
    mutations,
    getters,
    state,
};
