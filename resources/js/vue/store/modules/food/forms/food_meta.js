import Vue from 'vue';
import {getField, updateField} from 'vuex-map-fields';

import {cloneDeep} from 'lodash';

import * as types from '../mutation_types';

import {Food} from '../../../../objects/food';

const actions = {
    resetFoodMetaForm({commit}) {
        commit(types.RESET_FOOD_META_FORM);
    },
};

const mutations = {
    updateField,

    [types.SET_FOOD_META_FORM](state, data) {
        Vue.set(state, 'rows', [new Food({meta:data})]);
        state.origin = cloneDeep(state.rows);
    },

    [types.RESET_FOOD_META_FORM](state) {
        Vue.set(state, 'rows', cloneDeep(state.origin));
    },
};

const getters = {
    getField,
};

const state = () => ({
    origin: [new Food()],
    rows: [new Food()],
});

export default {
    namespaced: true,
    actions,
    mutations,
    getters,
    state,
};
