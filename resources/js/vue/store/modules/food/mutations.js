import * as types from './mutation_types';
import Vue from 'vue';
import Error from '../../../../utilities/errors';
import {Food} from '../../../objects/food';

export default {
    [types.REQUEST_FOOD](state, data) {
        state.foodRequest = data;
    },
    [types.SET_FOOD](state, data) {
        Vue.set(state, 'foodRef', new Food({meta: data}));
    },
    [types.REQUEST_STORE_FOOD](state, data) {
        state.storeFoodRequest = data;
    },
    [types.SET_REQUEST_STORE_FOOD_VALIDATION_ERROR](state, data) {
        const error = new Error();
        error.record(data);
        Vue.set(state, '__errors', error);
    },
    [types.REQUEST_UPDATE_FOOD](state, data) {
        state.updateFoodRequest = data;
    },
    [types.SET_REQUEST_UPDATE_FOOD_VALIDATION_ERROR](state, data) {
        const error = new Error();
        error.record(data);
        Vue.set(state, '__errors', error);
    },
    [types.REQUEST_DELETE_FOOD](state, data) {
        state.deleteFoodRequest = data;
    },
    [types.REQUEST_FOODS](state, data) {
        state.foodsRequest = data;
    },
    [types.SET_FOODS](state, data) {
        Vue.set(state, '__foods', data);
    },
    [types.CLEAR_REQUEST_ERROR](state, data) {
        const error = new Error();
        error.record(state.__errors.errors);
        error.clear(data);
        Vue.set(state, '__errors', error);
    },
};
