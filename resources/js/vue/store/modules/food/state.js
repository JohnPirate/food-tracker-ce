import Errors from '../../../../utilities/errors';
import {Food} from '../../../objects/food';

export const state = () => ({
    foodRef: new Food(),
    foodRequest: false,
    updateFoodRequest: false,
    deleteFoodRequest: false,

    __foods: [],
    foodsRequest: false,

    __errors: new Errors(),
});
