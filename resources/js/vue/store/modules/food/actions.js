import * as types from './mutation_types';
import FoodApi from '../../../service/foods';
import {createFoodStore, Food} from '../../../objects/food';
import {createNutrientsStore} from '../../../objects/nutrient';
import {redirect_to} from '../../../../utilities/url_helpers';
import {handle_response_error} from '../../../../utilities/response_error';


/*
|---------------------------------------------------------------------------
| Errors
|---------------------------------------------------------------------------
*/
export const clearRequestError = ({ commit }, key) => {
    commit(types.CLEAR_REQUEST_ERROR, key);
};


/*
|---------------------------------------------------------------------------
| Fetch food
|---------------------------------------------------------------------------
*/
export const requestFood = ({ commit }, data) => {
    commit(types.REQUEST_FOOD, data);
};
export const receiveFoodSuccess = ({ dispatch, commit}, food) => {
    commit(types.SET_FOOD, food);
    dispatch(`requestFood`, false);
};
export const receiveFoodError = ({ dispatch }, error) => {
    handle_response_error(error);
    dispatch(`requestFood`, false);
};
export const fetchFood = ({ state, dispatch }, id) => {
    dispatch(`requestFood`, true);
    return FoodApi.getFood(id)
        .then(({ data }) => {
            dispatch(`receiveFoodSuccess`, data);
        })
        .catch((error) => {
            dispatch(`receiveFoodError`, error);
            return error;
        });
};


/*
|---------------------------------------------------------------------------
| Fetch food for edit
|---------------------------------------------------------------------------
*/
export const receiveFoodForEditSuccess = ({ dispatch, commit}, food) => {
    commit(`foodMeta/${types.SET_FOOD_META_FORM}`, food);
    commit(`foodNutrients/${types.SET_FOOD_NUTRIENTS_FORM}`, food.nutrients);
    dispatch(`requestFood`, false);
};
export const fetchFoodForEdit = ({ state, dispatch }, id) => {
    dispatch(`requestFood`, true);
    return FoodApi.getFood(id)
        .then((response) => {
            dispatch(`receiveFoodForEditSuccess`, response.data);
            return response;
        })
        .catch((error) => {
            dispatch(`receiveFoodError`, error);
            return error;
        });
};


/*
|---------------------------------------------------------------------------
| Store food
|---------------------------------------------------------------------------
*/
export const requestStoreFood = ({ commit }, data) => {
    commit(types.REQUEST_STORE_FOOD, data);
};
export const receiveStoreFoodSuccess = ({ dispatch, commit}, data) => {
    commit(types.SET_FOOD, new Food(data.data));
    dispatch(`requestFood`, false);
};
export const receiveStoreFoodError = ({ dispatch, commit }, error) => {
    if (error.response.status === 422) {
        commit(types.SET_REQUEST_STORE_FOOD_VALIDATION_ERROR, error.response.data.errors)
    } else {
        handle_response_error(error);
    }
    dispatch(`requestFood`, false);
};
export const storeFood = ({dispatch, state}) => {
    const nutrients = createNutrientsStore(state.foodNutrients.rows);
    const food = createFoodStore(
        state.foodMeta.rows[0],
        nutrients
    );
    dispatch(`requestStoreFood`, true);
    return FoodApi.storeFood(food)
        .then((response) => {
            dispatch(`receiveStoreFoodSuccess`, response.data);
            return response;
        })
        .catch((error) => {
            dispatch(`receiveStoreFoodError`, error);
            return error;
        });
};


/*
|---------------------------------------------------------------------------
| Update Food
|---------------------------------------------------------------------------
*/
export const requestUpdateFood = ({ commit }, data) => {
    commit(types.REQUEST_UPDATE_FOOD, data);
};
export const receiveUpdateFoodSuccess = ({ dispatch, commit}, data) => {
    commit(`foodMeta/${types.SET_FOOD_META_FORM}`, data.data);
    commit(`foodNutrients/${types.SET_FOOD_NUTRIENTS_FORM}`, data.data.nutrients);
    dispatch(`requestUpdateFood`, false);
};
export const receiveUpdateFoodError = ({ dispatch, commit }, error) => {
    if (error.response.status === 422) {
        commit(types.SET_REQUEST_UPDATE_FOOD_VALIDATION_ERROR, error.response.data.errors)
    } else {
        handle_response_error(error);
    }
    dispatch(`requestUpdateFood`, false);
};
export const updateFood = ({dispatch, state}, id) => {
    const nutrients = createNutrientsStore(state.foodNutrients.rows);
    const food = createFoodStore(
        state.foodMeta.rows[0],
        nutrients
    );
    dispatch(`requestUpdateFood`, true);
    return FoodApi.updateFood(id, food)
        .then((response) => {
            dispatch(`receiveUpdateFoodSuccess`, response.data);
            return response;
        })
        .catch((error) => {
            dispatch(`receiveUpdateFoodError`, error);
            return error;
        });
};


/*
 |---------------------------------------------------------------------------
 | Delete Food
 |---------------------------------------------------------------------------
 */
export const requestDeleteFood = ({ commit }, data) => {
    commit(types.REQUEST_DELETE_FOOD, data);
};
export const receiveDeleteFoodSuccess = ({ dispatch, commit}, data) => {
    dispatch(`requestDeleteFood`, false);
};
export const receiveDeleteFoodError = ({ dispatch, commit }, error) => {
    handle_response_error(error);
    dispatch(`requestDeleteFood`, false);
};
export const deleteFood = ({dispatch, state}, id) => {
    dispatch(`requestDeleteFood`, true);
    return FoodApi.deleteFood(id)
        .then((response) => {
            dispatch(`receiveDeleteFoodSuccess`, response.data);
            return response;
        })
        .catch((error) => {
            dispatch(`receiveDeleteFoodError`, error);
            return error;
        });
};


/*
|---------------------------------------------------------------------------
| Fetch foods
|---------------------------------------------------------------------------
*/
export const requestFoods = ({ commit }, data) => {
    commit(types.REQUEST_FOODS, data);
};
export const receiveFoodsSuccess = ({ dispatch, commit}, foodsRaw) => {
    const foods = foodsRaw.map((food) => {
        return new Food({meta:food});
    });
    commit(types.SET_FOODS, foods);
    dispatch(`requestFoods`, false);
};
export const receiveFoodsError = ({ dispatch }, error) => {
    handle_response_error(error);
    dispatch(`requestFoods`, false);
};
export const fetchFoods = ({ state, dispatch }) => {
    dispatch(`requestFoods`, true);
    return FoodApi.getFoods()
        .then((response) => {
            dispatch(`receiveFoodsSuccess`, response.data);
            return response;
        })
        .catch((error) => {
            dispatch(`receiveFoodsError`, error);
            return error;
        });
};
