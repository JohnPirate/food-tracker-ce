import * as types from './mutation_types';

const actions = {};

const mutations = {};

const getters = {};

const state = () => {
    return {};
};
export default {
    actions,
    mutations,
    getters,
    state,
};
