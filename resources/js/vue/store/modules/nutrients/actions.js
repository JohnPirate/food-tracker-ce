import * as types from './mutation_types';
import NutrientsApi from '../../../service/nutrients';
import {handle_response_error} from '../../../../utilities/response_error';


export const requestNutrients = ({ commit }, data) => {
    commit(types.REQUEST_NUTRIENTS, data);
};
export const receiveNutrientsSuccess = ({ dispatch, commit}, nutrients) => {
    commit(types.SET_NUTRIENTS, nutrients);
    const selection = nutrients.map((n) => n.key);
    commit(types.SET_NUTRIENTS_SELECTION, selection);
    dispatch(`requestNutrients`, false);
};
export const receiveNutrientsError = ({ dispatch }, error) => {
    handle_response_error(error);
    dispatch(`requestNutrients`, false);
};
export const fetchNutrients = ({ state, dispatch }) => {

    dispatch(`requestNutrients`, true);

    NutrientsApi.getNutrients()
        .then(({ data }) => {
            dispatch(`receiveNutrientsSuccess`, data);
        })
        .catch((error) => {
            dispatch(`receiveNutrientsError`, error);
        });
};


export const requestNutrientUnits = ({ commit }, data) => {
    commit(types.REQUEST_NUTRIENT_UNITS, data);
};
export const receiveNutrientUnitsSuccess = ({ dispatch, commit}, nutrients) => {
    commit(types.SET_NUTRIENT_UNITS, nutrients);
    dispatch(`requestNutrientUnits`, false);
};
export const receiveNutrientUnitsError = ({ dispatch }, error) => {
    handle_response_error(error);
    dispatch(`requestNutrientUnits`, false);
};
export const fetchNutrientUnits = ({ state, dispatch }) => {

    dispatch(`requestNutrientUnits`, true);

    NutrientsApi.getNutrientUnits()
        .then(({ data }) => {
            dispatch(`receiveNutrientUnitsSuccess`, data);
        })
        .catch((error) => {
            dispatch(`receiveNutrientUnitsError`, error);
        });
};
