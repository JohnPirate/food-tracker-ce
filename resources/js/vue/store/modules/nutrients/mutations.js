import * as types from './mutation_types';

export default {
    [types.REQUEST_NUTRIENTS](state, data) {
        state.nutrientsRequest = data;
    },
    [types.SET_NUTRIENTS](state, data) {
        state.nutrients = data;
    },
    [types.SET_NUTRIENTS_SELECTION](state, data) {
        state.__nutrientsSelection = data;
    },
    [types.REQUEST_NUTRIENT_UNITS](state, data) {
        state.nutrientUnitsRequest = data;
    },
    [types.SET_NUTRIENT_UNITS](state, data) {
        state.__nutrientUnits = data;
    },
};
