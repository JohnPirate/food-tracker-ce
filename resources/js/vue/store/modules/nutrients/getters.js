export const nutrientsSelection = (state, getters) => {
    return state.__nutrientsSelection;
};
export const nutrientUnits = (state, getters) => {
    return state.__nutrientUnits;
};
export const areNutrientsLoading = (state, getters) => {
    return state.nutrientsRequest;
};
export const areNutrientUnitsLoading = (state, getters) => {
    return state.nutrientUnitsRequest;
};
