export const REQUEST_CONSUMED_FOODS = 'REQUEST_CONSUMED_FOODS';
export const SET_CONSUMED_FOODS = 'SET_CONSUMED_FOODS';

export const SET_CONSUMED_FOOD_NEW = 'SET_CONSUMED_FOOD_NEW';
export const SET_CONSUMED_FOOD_EDIT = 'SET_CONSUMED_FOOD_EDIT';

export const REQUEST_CONSUMED_FOOD_GROUPS = 'REQUEST_CONSUMED_FOOD_GROUPS';
export const SET_CONSUMED_FOOD_GROUPS = 'SET_CONSUMED_FOOD_GROUPS';
export const REQUEST_CONSUMED_FOOD_GROUPS_USED_AT = 'REQUEST_CONSUMED_FOOD_GROUPS_USED_AT';
export const SET_CONSUMED_FOOD_GROUPS_USED_AT = 'SET_CONSUMED_FOOD_GROUPS_USED_AT';
export const ADD_CONSUMED_FOOD_GROUP = 'ADD_CONSUMED_FOOD_GROUP';
export const SET_CONSUMED_FOOD_GROUP = 'SET_CONSUMED_FOOD_GROUP';

export const REQUEST_STORE_CONSUMED_FOOD = 'REQUEST_STORE_CONSUMED_FOOD';
export const SET_CONSUMED_FOOD_VALIDATION_ERROR = 'SET_CONSUMED_FOOD_VALIDATION_ERROR';
export const CLEAR_CONSUMED_FOOD_VALIDATION_ERROR = 'CLEAR_CONSUMED_FOOD_VALIDATION_ERROR';

export const REQUEST_DELETE_CONSUMED_FOOD = 'REQUEST_DELETE_CONSUMED_FOOD';

export const RESET_CONSUMED_FOOD_MODAL = 'RESET_CONSUMED_FOOD_MODAL';
