import * as types from './mutation_types';
import {handle_response_error} from '../../../../utilities/response_error';
import ConsumedFoodsApi from '../../../service/consumed_foods';
import {ConsumedFood} from '../../../objects/consumed_food';
import {Food} from '../../../objects/food';

export const requestConsumedFoods = ({ commit }, data) => {
    commit(types.REQUEST_CONSUMED_FOODS, data);
};
export const receiveConsumedFoodsSuccess = ({ dispatch, commit}, consumedFoodsRaw) => {
    const consumedFoods = consumedFoodsRaw.map((consumedFood) => {
        const food = new Food({meta: consumedFood});
        food.id = consumedFood.food_id;
        return new ConsumedFood({
            consume: consumedFood,
            food,
        });
    });
    commit('consumedFoodsForm/' + types.SET_CONSUMED_FOODS, consumedFoods);
};
export const receiveConsumedFoodsError = ({ dispatch }, error) => {
    handle_response_error(error);
};
export const fetchConsumedFoods = ({ dispatch }, {from, to, silent = false}) => {

    if (!silent) {
        dispatch(`requestConsumedFoods`, true);
    }

    ConsumedFoodsApi.getConsumedFoods(from, to)
        .then(({ data }) => {
            dispatch(`receiveConsumedFoodsSuccess`, data);
        })
        .catch((error) => {
            dispatch(`receiveConsumedFoodsError`, error);
        })
        .then(() => {
            if (!silent) {
                dispatch(`requestConsumedFoods`, false);
            }
        });
};
