import Vue from 'vue';
import {getField, updateField} from 'vuex-map-fields';

import * as types from '../mutation_types';
import ConsumedFoodsApi from '../../../../service/consumed_foods';
import {handle_response_error} from '../../../../../utilities/response_error';
import * as _ from 'lodash';

const actions = {
    setConsumedFoodGroups({ commit }, groups) {
        commit(types.SET_CONSUMED_FOOD_GROUPS, groups);
    },
    addConsumedFoodGroup({ commit }, groups) {
        commit(types.ADD_CONSUMED_FOOD_GROUP, groups);
    },
    requestConsumedFoodGroups({ commit }, data) {
        commit(types.REQUEST_CONSUMED_FOOD_GROUPS, data);
    },
    receiveConsumedFoodGroupsSuccess({ dispatch }, groups) {
        dispatch('setConsumedFoodGroups', groups);
        dispatch('requestConsumedFoodGroups', false);
    },
    receiveConsumedFoodGroupsError({ dispatch }, error) {
        handle_response_error(error);
        dispatch('requestConsumedFoodGroups', false);
    },
    fetchConsumedFoodGroups({ dispatch }) {
        dispatch('requestConsumedFoodGroups', true);
        return ConsumedFoodsApi.getConsumedFoodGroups()
            .then((response) => {
                dispatch('receiveConsumedFoodGroupsSuccess', response.data);
                return response;
            })
            .catch((error) => {
                dispatch('receiveConsumedFoodGroupsError', error);
                return error;
            });
    },
    setConsumedFoodGroupsAt({ commit }, groups) {
        commit(types.SET_CONSUMED_FOOD_GROUPS_USED_AT, groups);
    },
    requestConsumedFoodGroupsAt({ commit }, data) {
        commit(types.REQUEST_CONSUMED_FOOD_GROUPS_USED_AT, data);
    },
    receiveConsumedFoodGroupsAtSuccess({ dispatch }, groups) {
        dispatch('setConsumedFoodGroupsAt', groups);
        dispatch('requestConsumedFoodGroupsAt', false);
    },
    receiveConsumedFoodGroupsAtError({ dispatch }, error) {
        handle_response_error(error);
        dispatch('requestConsumedFoodGroupsAt', false);
    },
    fetchConsumedFoodGroupsAt({ dispatch }, {from = null, to = null} = {}) {
        dispatch('requestConsumedFoodGroupsAt', true);
        return ConsumedFoodsApi.getConsumedFoodGroupsAt(from, to)
            .then((response) => {
                dispatch('receiveConsumedFoodGroupsAtSuccess', response.data);
                return response;
            })
            .catch((error) => {
                dispatch('receiveConsumedFoodGroupsAtError', error);
                return error;
            });
    },
};

const mutations = {
    updateField,

    [types.REQUEST_CONSUMED_FOOD_GROUPS](state, data) {
        state.groupsRequest = data;
    },
    [types.SET_CONSUMED_FOOD_GROUPS](state, groups) {
        Vue.set(state, 'groups', groups);
    },
    [types.REQUEST_CONSUMED_FOOD_GROUPS_USED_AT](state, data) {
        state.usedGroupsRequest = data;
    },
    [types.SET_CONSUMED_FOOD_GROUPS_USED_AT](state, groups) {
        Vue.set(state, 'usedGroups', groups);
    },
    [types.ADD_CONSUMED_FOOD_GROUP](state, data) {
        state.usedGroups.push(data);
    },
};

const getters = {
    getField,

    areConsumedFoodGroupsLoading(state) {
        return state.groupsRequest;
    },
    areConsumedFoodGroupsAtLoading(state) {
        return state.usedGroupsRequest;
    },
    getGroups(state) {
        return state.groups;
    },
    getUsedGroups(state) {
        return state.usedGroups;
    },
    optionGroups(state, getters) {
        const ret = [];

        if (getters.getUsedGroups.length > 0) {
            const unusedGroups = _.differenceBy(
                getters.getGroups,
                getters.getUsedGroups,
                'id'
            );
            ret.push({
                title: window.$trans('forms.food_groups.opt_groups.used_groups'),
                groups: getters.getUsedGroups,
            });
            ret.push({
                title: window.$trans('forms.food_groups.opt_groups.other_groups'),
                groups: unusedGroups,
            });
        } else {
            ret.push({
                title: window.$trans('forms.food_groups.opt_groups.groups'),
                groups: getters.getGroups,
            });
        }
        return ret;
    },
};

const state = () => ({
    groupsRequest: false,
    groups: [],
    usedGroupsRequest: false,
    usedGroups: [],
});

export default {
    namespaced: true,
    mutations,
    getters,
    actions,
    state,
};
