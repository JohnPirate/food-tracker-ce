import Vue from 'vue';
import {getField, updateField} from 'vuex-map-fields';

import * as types from '../mutation_types';
import {ConsumedFood} from '../../../../objects/consumed_food';
import ConsumedFoodsApi from '../../../../service/consumed_foods';
import {handle_response_error} from '../../../../../utilities/response_error';

import Errors from '../../../../../utilities/errors';

const actions = {
    resetConsumedFoodModal({ commit }) {
        commit(types.RESET_CONSUMED_FOOD_MODAL);
    },
    setConsumedFoodNew({ commit }, food) {
        const consumedFood = new ConsumedFood({food});
        commit(types.SET_CONSUMED_FOOD_NEW, consumedFood);
    },
    setConsumedFoodEdit({ commit }, consumedFood) {
        commit(types.SET_CONSUMED_FOOD_EDIT, consumedFood);
    },
    setConsumedFoodGroup({ commit }, group) {
        commit(types.SET_CONSUMED_FOOD_GROUP, group);
    },


    requestStoreConsumedFood({ commit }, data) {
        commit(types.REQUEST_STORE_CONSUMED_FOOD, data);
    },
    receiveStoreConsumedFoodSuccess({ dispatch }, data) {
        $alert.success(data.message);
        dispatch('requestStoreConsumedFood', false);
    },
    receiveStoreConsumedFoodError({ dispatch }, error) {
        const response = error.response;
        if (response.status === 422) {
            dispatch('setConsumedFoodValidationError', response.data.errors);
        } else {
            handle_response_error(error);
        }
        dispatch('requestStoreConsumedFood', false);
    },

    receiveUpdateConsumedFoodSuccess({ dispatch }, data) {
        $alert.success(data.message);
        dispatch('requestStoreConsumedFood', false);
    },


    requestDeleteConsumedFood({ commit }, data) {
        commit(types.REQUEST_DELETE_CONSUMED_FOOD, data);
    },
    receiveDeleteConsumedFoodSuccess({ dispatch }, data) {
        $alert.success(data.message);
        dispatch('requestDeleteConsumedFood', false);
    },
    receiveDeleteConsumedFoodError({ dispatch }, error) {
        handle_response_error(error);
        dispatch('requestDeleteConsumedFood', false);
    },
    deleteConsumedFoodRequest({ dispatch }, consumedFoodId) {

        dispatch('requestDeleteConsumedFood', true);

        return ConsumedFoodsApi.deleteConsumedFood(consumedFoodId)
            .then(({ data }) => {
                dispatch('receiveDeleteConsumedFoodSuccess', data);

                return true;
            })
            .catch((error) => {
                dispatch('receiveDeleteConsumedFoodError', error);
            })
        ;
    },


    setConsumedFoodValidationError({ commit }, data) {
        commit(types.SET_CONSUMED_FOOD_VALIDATION_ERROR, data);
    },
    clearConsumedFoodValidationError({ commit }, name) {
        commit(types.CLEAR_CONSUMED_FOOD_VALIDATION_ERROR, name);
    },
};

const mutations = {
    updateField,

    [types.SET_CONSUMED_FOODS](state, data) {
        state.rows = [];
        data.forEach((consumedFood) => {
            state.rows.push(consumedFood);
        });
    },

    [types.SET_CONSUMED_FOOD_NEW](state, consumedFood) {
        Vue.set(state, 'consumedFood', consumedFood);
        state.storeUpdateModalState = 'NEW';
    },
    [types.SET_CONSUMED_FOOD_EDIT](state, consumedFood) {
        Vue.set(state, 'consumedFood', consumedFood);
        state.storeUpdateModalState = 'EDIT';
    },
    [types.SET_CONSUMED_FOOD_GROUP](state, group) {
        Vue.set(state.consumedFood, 'group', group);
    },
    [types.RESET_CONSUMED_FOOD_MODAL](state) {
        Vue.set(state, 'consumedFood', new ConsumedFood());
        state.storeUpdateModalState = 'NEW';
    },

    [types.REQUEST_STORE_CONSUMED_FOOD](state, data) {
        state.storeConsumedFoodRequest = data;
    },

    [types.REQUEST_DELETE_CONSUMED_FOOD](state, data) {
        state.deleteConsumedFoodRequest = data;
    },

    [types.SET_CONSUMED_FOOD_VALIDATION_ERROR](state, data) {
        Vue.set(state, 'errors', new Errors(data));
    },
    [types.CLEAR_CONSUMED_FOOD_VALIDATION_ERROR](state, name) {
        const err = state.errors;
        err.clear(name);
        Vue.set(state, 'errors', err);
    },
};

const getters = {
    getField,

    getSumValues(state, getters) {
        const sum = {
            energy: 0,
            carb: 0,
            fat: 0,
            protein: 0,
        };
        state.rows.forEach((row) => {
            /** @var {ConsumedFood} row */
            sum.energy += row.calcEnergy();
            sum.carb += row.calcCarb();
            sum.fat += row.calcFat();
            sum.protein += row.calcProtein();
        });
        return sum;
    },

    getSumFactors(state, getters) {
        if (getters.getSumValues.energy > 0) {
            const fat = getters.getSumValues.fat * 9;
            const protein = getters.getSumValues.protein * 4;
            const carb = getters.getSumValues.carb * 4;
            return {
                fat,
                protein,
                carb,
            }
        }
        return {
            carb: 0,
            fat: 0,
            protein: 0,
        }
    },

    isEditState(state) {
        return state.storeUpdateModalState === 'EDIT';
    },

    isStoreConsumedFoodLoading(state) {
        return state.storeConsumedFoodRequest;
    },

    isDeleteConsumedFoodLoading(state) {
        return state.deleteConsumedFoodRequest;
    }
};

const state = () => ({
    rows: [],
    consumedFood: new ConsumedFood(),
    storeUpdateModalState: 'NEW',
    errors: new Errors(),

    storeConsumedFoodRequest: false,
    deleteConsumedFoodRequest: false,
});

export default {
    namespaced: true,
    mutations,
    getters,
    actions,
    state,
};
