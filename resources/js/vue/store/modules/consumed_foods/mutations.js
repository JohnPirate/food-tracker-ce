import * as types from './mutation_types';

export default {
    [types.REQUEST_CONSUMED_FOODS](state, data) {
        state.consumedFoodsRequest = data;
    },
};
