import * as actions from './actions';
import * as getters from './getters';
import mutations from './mutations';
import state from './state';

import { createHelpers } from 'vuex-map-fields';

import consumedFoodsForm from './forms/consumed_foods';
import consumedFoodGroupsForm from './forms/consumed_food_groups';

const modules = {
    consumedFoodsForm,
    consumedFoodGroupsForm,
};

export const {mapMultiRowFields: mapConsumedFoodsMultiRowFields} = createHelpers({
    getterType: `consumedFoods/consumedFoodsForm/getField`,
    mutationType: `consumedFoods/consumedFoodsForm/updateField`,
});

export const {mapFields: mapConsumedFoodInsertUpdateFields} = createHelpers({
    getterType: `consumedFoods/consumedFoodsForm/getField`,
    mutationType: `consumedFoods/consumedFoodsForm/updateField`,
});

export const {mapMultiRowFields: mapConsumedFoodGroupsMultiRowFields} = createHelpers({
    getterType: `consumedFoods/consumedFoodGroupsForm/getField`,
    mutationType: `consumedFoods/consumedFoodGroupsForm/updateField`,
});

export const consumedFoods = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
    modules
};
