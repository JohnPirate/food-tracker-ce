import * as types from './mutation_types';

const actions = {
    requestFetchTemplates({ commit }, data) {
        commit(types.REQUEST_FETCH_TEMPLATES, data);
    },
};

const mutations = {
    [types.REQUEST_FETCH_TEMPLATES](state, data) {
        state.fetchTemplatesRequest = data;
    },
};

const getters = {
    isFetchTemplatesRequestActive(state) {
        return state.fetchTemplatesRequest;
    },
};

const state = () => {
    return {
        fetchTemplatesRequest: false,
    };
};


export default {
    actions,
    mutations,
    getters,
    state,
};
