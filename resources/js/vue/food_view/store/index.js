import Vue from 'vue';
import Vuex from 'vuex';
import nutrients from '../../store/modules/nutrients/index';
import {food} from '../../store/modules/food/index';
import {consumedFoods} from '../../store/modules/consumed_foods';
import VuexORM from '@vuex-orm/core'
import Template from '../../models/Template';
import templates from '../../store/modules/templates/index';

Vue.use(Vuex);

const database = new VuexORM.Database();
database.register(Template, templates);

const debug = process.env.NODE_ENV !== 'production';

export const createStore = () => new Vuex.Store({
    plugins: [VuexORM.install(database)],
    modules: {
        food,
        nutrients,
        consumedFoods,
    },
    strict: debug
});
export default createStore();
