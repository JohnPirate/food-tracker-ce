import Vue from 'vue';
import FoodView from './components/food_view';
import store from './store/index';

const elementId = 'food-view';

document.addEventListener('DOMContentLoaded', function () {

    new Vue({

        el: `#${elementId}`,

        components: {
            FoodView,
        },

        store,

        data () {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                foodId: parseInt(environmentsData.foodId)
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    foodId: this.foodId
                }
            });
        }
    });
});
