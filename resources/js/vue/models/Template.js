import {Model} from '@vuex-orm/core';
import Ingredient from './Ingredient';

import {has} from '../../utilities/helpers';

export default class Template extends Model {

    static get entity() {
        return 'templates';
    }

    static get primaryKey() {
        return 'id';
    }

    static fields() {
        return {
            id: this.uid(),
            name: this.attr(null),
            description: this.attr(null),
            portion: this.attr(1),
            created_at: this.attr(null),
            updated_at: this.attr(null),
            ingredients: this.hasMany(Ingredient, 'template_id'),
        };
    }

    get nutrients() {
        let nutrients = {};
        this.ingredients.forEach((ingredient) => {
            let food = ingredient.food;
            food.nutrients.forEach((nutrient) => {
                if (!has.call(nutrients, nutrient.key)) {
                    nutrients[nutrient.key] = {
                        key: nutrient.key,
                        value: 0,
                        unit: nutrient.unit,
                        group: nutrient.group,
                    };
                }
                // FIXME(ssandriesser): find a solution for the carb/fiber issue
                nutrients[nutrient.key].value += (nutrient.value / food.ref_value * ingredient.value);
            });
        });

        return nutrients;
    }

    getUpdateData() {

        let ingredients = [];

        this.ingredients.forEach((ingredient) => {
            ingredients.push({
                id: ingredient.id,
                value: ingredient.value,
                foodId: ingredient.food_id,
            });
        });

        return {
            name: this.name,
            description: this.description,
            portion: this.portion,
            ingredients,
        };
    }

    getConsumeData() {
        let ingredients = [];

        this.ingredients.forEach((ingredient) => {
            ingredients.push({
                value: ingredient.value,
                foodId: ingredient.food_id,
            });
        });

        return ingredients;
    }

}
