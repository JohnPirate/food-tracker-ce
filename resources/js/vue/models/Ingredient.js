import {Model} from '@vuex-orm/core';
import Food from './Food';
import Template from './Template';

export default class Ingredient extends Model {

    static get entity() {
        return 'ingredients';
    }

    static get primaryKey() {
        return 'id';
    }

    static fields() {
        return {
            id: this.uid(),
            food_id: this.attr(null),
            template_id: this.attr(null),
            value: this.number(0).nullable(),
            created_at: this.attr(null),
            updated_at: this.attr(null),
            food: this.hasOne(Food, 'ingredient_id'),
            template: this.belongsTo(Template, 'template_id'),
        };
    }
}
