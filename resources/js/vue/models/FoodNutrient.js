import { Model } from '@vuex-orm/core'

export default class FoodNutrient extends Model {

    static get entity() {
        return 'food_nutrients';
    }

    static get primaryKey() {
        return 'id';
    }

    static fields () {
        return {
            id: this.uid(),
            food_id: this.attr(null),
            value: this.attr(null),
            unit: this.attr('g'),
            group: this.attr(null),
            key: this.attr(null),
        }
    }
}
