import { Model } from '@vuex-orm/core'
import FoodNutrient from './FoodNutrient';

export default class Food extends Model {

    static get entity() {
        return 'foods';
    }

    static get primaryKey() {
        return 'id';
    }

    static fields () {
        return {
            id: this.uid(),
            ingredient_id: this.attr(null),
            name: this.attr(null),
            description: this.attr(null),
            brand: this.attr(null),
            barcode: this.attr(null),
            ref_value: this.number(100),
            ref_unit: this.string('g'),
            carb_without_fiber: this.boolean(false),
            deleted_at: this.attr(null),
            created_at: this.attr(null),
            updated_at: this.attr(null),
            editable: this.boolean(false),
            imported: this.boolean(false),
            import_source: this.attr(null),
            import_datetime: this.attr(null),
            nutrients: this.hasMany(FoodNutrient, 'food_id', 'id'),
        }
    }

    isDeleted() {
        return this.deleted_at !== null;
    }
}
