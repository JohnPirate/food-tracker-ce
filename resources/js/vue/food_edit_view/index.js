import Vue from 'vue';
import FoodEditView from './components/food_edit_view';
import store from './store/index';

const elementId = 'food-edit-view';

document.addEventListener('DOMContentLoaded', function () {

    new Vue({

        el: `#${elementId}`,

        components: {
            FoodEditView,
        },

        store,

        
        data () {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                foodId: parseInt(environmentsData.foodId)
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    foodId: this.foodId
                }
            });
        }
    });
});
