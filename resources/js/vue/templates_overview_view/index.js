import Vue from 'vue';
import TemplatesOverviewView from './components/templates_overview_view';
import store from './store/index';

const elementId = 'templates-overview-view';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            TemplatesOverviewView,
        },

        store,

        data() {
            // const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                // data properties
                // foo: environmentsData.foo,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    // foo: this.foo,
                }
            });
        }
    });
});
