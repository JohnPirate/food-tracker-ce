import {
    URL_FOOD_EDIT,
    URL_FOOD_SHOW,
    URL_TEMPLATE_CONSUME,
    URL_TEMPLATE_EDIT,
    URL_TEMPLATE_NEW,
    URL_TEMPLATES_OVERVIEW,
    URL_TEMPLATE_SHOW,
    USDA_FOOD_REPORT_LINK,
    USDA_FOOD_DATA_CENTRAL_REPORT_LINK,
    URL_FOOD_NEW,
    URL_CONSUMED_FOODS_SHOW, URL_REDIRECT_FOOD_CATALOGUE,
} from '../../config';

import {create_url} from '../../utilities/url_helpers';

export default {
    methods: {
        urlToFoodCatalogue() {
            return URL_REDIRECT_FOOD_CATALOGUE;
        },
        urlToJournal() {
            return URL_CONSUMED_FOODS_SHOW;
        },
        urlToFoodNew() {
            return URL_FOOD_NEW;
        },
        urlToFoodShow(id) {
            return create_url(URL_FOOD_SHOW, {id});
        },
        urlToUsdaFoodReport(id) {
            return create_url(USDA_FOOD_REPORT_LINK, {id});
        },
        urlToUsdaFoodDataCentralReport(id) {
            return create_url(USDA_FOOD_DATA_CENTRAL_REPORT_LINK, {id});
        },
        urlToFoodEdit(id) {
            return create_url(URL_FOOD_EDIT, {id});
        },
        urlToTemplateNew() {
            return URL_TEMPLATE_NEW;
        },
        urlToTemplatesOverview() {
            return URL_TEMPLATES_OVERVIEW;
        },
        urlToTemplateShow(id) {
            return create_url(URL_TEMPLATE_SHOW, {id});
        },
        urlToTemplateEdit(id) {
            return create_url(URL_TEMPLATE_EDIT, {id});
        },
        urlToTemplateConsume(id) {
            return create_url(URL_TEMPLATE_CONSUME, {id});
        },
    },
};
