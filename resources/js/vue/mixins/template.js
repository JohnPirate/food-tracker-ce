import templateIdMixin from './template_id';

import Template from '../models/Template';

export default {

    mixins: [
        templateIdMixin,
    ],

    computed: {
        template() {
            let template = Template.query().with('ingredients.food.*').find(this.templateId);
            if (template) {
                return template;
            }
            return new Template();
        },
    },
}
