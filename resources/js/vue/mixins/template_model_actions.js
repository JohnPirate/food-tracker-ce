import Template from '../models/Template';

export default {
    methods: {
        updateTemplateModel(elementId, key, value) {
            let data = {};
            data[key] = value;
            Template.update({
                where: elementId,
                data,
            });
        },
    },
};
