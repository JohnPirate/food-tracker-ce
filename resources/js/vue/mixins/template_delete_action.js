import {
    DEFAULT_REDIRECT_TIME,
    URL_TEMPLATES_OVERVIEW,
} from '../../config';
import TemplateApi from '../service/templates';
import {redirect_to} from '../../utilities/url_helpers';
import {is_error} from '../../utilities/helpers';
import {handle_response_error} from '../../utilities/response_error';

export default {
    methods: {
        deleteTemplateAction(id) {
            return TemplateApi.deleteTemplate(id)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error;
                })
                ;
        },
        deleteTemplate(id) {
            if (!window.confirm(this.$trans('components.modals.delete_template.message'))) {
                return;
            }
            this.deleteTemplateAction(id)
                .then((data) => {
                    if (is_error(data)) {
                        handle_response_error(data);
                        return;
                    }
                    window.$alert.success(data.data.message);
                    redirect_to({
                        url: URL_TEMPLATES_OVERVIEW,
                        delay: DEFAULT_REDIRECT_TIME,
                    });
                })
            ;
        },
    },
};
