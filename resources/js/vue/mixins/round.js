import {round} from '../../utilities/helpers';

export default {
    methods: {
        round(val, decimals = 2) {
            return round(val, decimals);
        }
    }
}
