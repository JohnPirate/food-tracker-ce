import foodMixin from './food';

export default {
    mixins: [foodMixin],

    props: {
        refValue: {
            type: String,
        },
        refUnit: {
            type: String,
        },
    },
    computed: {
        getRefValue() {
            return this.refValue ? this.refValue : this.getFood.ref_value;
        },
        getRefUnit() {
            return this.refUnit ? this.refUnit : this.getFood.ref_unit;
        },
        getEnergy() {
            return this.getFood.calcEnergy(this.getRefValue);
        },
        getCarb() {
            return this.getFood.calcCarb(this.getRefValue);
        },
        getFat() {
            return this.getFood.calcFat(this.getRefValue);
        },
        getProtein() {
            return this.getFood.calcProtein(this.getRefValue);
        },
    }
};
