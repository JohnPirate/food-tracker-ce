export default {
    props: {
        modalRef: {
            type: String,
            required: true
        }
    },

    computed: {
        getModalRef() {
            return this.modalRef;
        },
    },

    methods: {
        showSelf () {
            const hub = this.$root;
            hub.$emit('bv::show::modal', this.getModalRef);
        },

        hideSelf () {
            const hub = this.$root;
            hub.$emit('bv::hide::modal', this.getModalRef);
        }
    }
}
