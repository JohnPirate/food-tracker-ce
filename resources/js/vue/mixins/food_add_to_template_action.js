export default {
    methods: {
        addFoodToTemplate(food) {
            const hub = this.$root;
            hub.$emit(`action::addFoodToTemplate`, food);
        },
    },
};
