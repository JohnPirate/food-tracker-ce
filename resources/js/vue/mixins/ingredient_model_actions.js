import Ingredient from '../models/Ingredient';

export default {
    methods: {
        updateIngredientModel(elementId, key, value) {
            let data = {};
            data[key] = value;
            Ingredient.update({
                where: elementId,
                data,
            });
        },
        updateIngredientModelValue(elementId, value) {
            if (value === '' || isNaN(value)) {
                value = null;
            }
            this.updateIngredientModel(elementId, 'value', value);
        },
    },
};
