import {cloneDeep} from 'lodash';
import Template from '../models/Template';
import TemplateAPI from '../service/templates';
import Ingredient from '../models/Ingredient';

export default {

    data() {
        return {
            originalData: null,
            request: {
                fetchTemplate: false
            },
        };
    },

    computed: {
        isFetchTemplateRequesting() {
            return this.request.fetchTemplate;
        },
        getOriginalData() {
            if (!this.originalData) {
                return {
                    portion: 1,
                };
            }
            return this.originalData;
        },
    },

    methods: {
        fetchTemplate(id, full = false) {
            const vm = this;
            vm.request.fetchTemplate = true;
            return TemplateAPI.fetchTemplate(id, full)
                .then((response) => {
                    vm.originalData = cloneDeep(response.data);
                    Template.create({data: response.data});
                    return response;
                })
                .catch((error) => {
                    return error;
                })
                .then(() => {
                    vm.request.fetchTemplate = false;
                })
                ;
        },
        actionCreateTemplate() {
            this.template.$save();
            let data = this.template.getUpdateData();
            return TemplateAPI.createTemplate(data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error;
                })
                ;
        },
        actionUpdateTemplate(id) {
            this.template.$save();
            let data = this.template.getUpdateData();
            return TemplateAPI.updateTemplate(id, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error;
                })
                ;
        },
        actionConsumeTemplate(id, data) {
            return TemplateAPI.consumeTemplate(id, data);
        },
        addTemplateIngredient(templateId, food) {
            const hub = this.$root;

            // check if food already exists as ingredient
            if (Ingredient.query().where('food_id', food.id).exists()) {
                window.$alert.warning(this.$trans('messages.food_already_exists_as_ingredient', {name: food.name}));
                hub.$emit(`action::searchFood::close`);
                return;
            }

            Ingredient.insert({
                data: {
                    food_id: food.id,
                    template_id: templateId,
                    value: food.value,
                    food: food,
                },
            })
                .then(() => {
                    hub.$emit(`action::searchFood::close`);
                    window.$alert.hide('warning');
                });
        },
        addFoodToTemplate() {
            const hub = this.$root;
            hub.$emit(`action::searchFood`);
        },
    },
};
