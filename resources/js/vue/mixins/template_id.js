export default {
    props: {
        templateId: {
            type: Number,
            required: true
        }
    }
}
