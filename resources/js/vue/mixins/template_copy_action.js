import {
    DEFAULT_REDIRECT_TIME,
    URL_TEMPLATE_EDIT,
} from '../../config';
import TemplateApi from '../service/templates';
import {create_url, redirect_to} from '../../utilities/url_helpers';
import {handle_response_error} from '../../utilities/response_error';

export default {
    methods: {
        copyTemplateAction(id) {
            return TemplateApi.copyTemplate(id);
        },
        copyTemplate(id) {
            if (!window.confirm(this.$trans('components.modals.copy_template.message'))) {
                return;
            }
            this.copyTemplateAction(id)
                .then(({data}) => {
                    window.$alert.success(data.message);
                    redirect_to({
                        url: create_url(URL_TEMPLATE_EDIT, {id:data.data.template_id}),
                        delay: DEFAULT_REDIRECT_TIME,
                    });
                })
                .catch((error) => {
                    handle_response_error(error);
                })
            ;
        },
    },
};
