
import {
    MOMENT_JS_CONSUMED_DATE_FORMAT,
    MOMENT_JS_CONSUMED_DATE_OVERVIEW_FORMAT
} from '../../config';

import { createNamespacedHelpers } from 'vuex';
import {
    mapConsumedFoodInsertUpdateFields,
} from '../store/modules/consumed_foods/index';

const {
    mapActions: mapConsumedFoodsFormActions,
} = createNamespacedHelpers('consumedFoods/consumedFoodsForm');

const {
    mapActions: mapConsumedFoodGroupsFormActions,
    mapGetters: mapConsumedFoodGroupsFormGetters,
} = createNamespacedHelpers('consumedFoods/consumedFoodGroupsForm');

export default {

    computed: {
        ...mapConsumedFoodInsertUpdateFields([
            'consumedFood.group',
        ]),
        ...mapConsumedFoodGroupsFormGetters([
            'optionGroups',
            'areConsumedFoodGroupsLoading',
            'areConsumedFoodGroupsAtLoading',
        ]),
    },

    methods: {
        ...mapConsumedFoodsFormActions([
            'setConsumedFoodGroup',
        ]),
        ...mapConsumedFoodGroupsFormActions([
            'addConsumedFoodGroup',
            'fetchConsumedFoodGroups',
            'fetchConsumedFoodGroupsAt',
        ]),
        updateGroup(group) {
            // NOTE(ssandriesser): workaround; existing groups need parameter access,
            //                     because they are empty objects in case of no access.
            let clone = null;
            if (group) {
                clone = {
                    id: group.id,
                    name: group.name,
                };
            }
            this.setConsumedFoodGroup(clone);
        },
        addTag(newTag) {
            const nt = {
                id: null,
                name: newTag
            };
            this.setConsumedFoodGroup(nt);
            this.addConsumedFoodGroup(nt);
        },
        fetchGroups({consumedAt = null} = {}) {
            const vm = this;
            this.fetchConsumedFoodGroups()
                .then((data) => {
                    if (! (data instanceof Error)) {
                        vm.fetchGroupsAt({consumedAt});
                    }
                })
            ;
        },
        fetchGroupsAt({consumedAt = null} = {}) {
            let date = consumedAt;
            if (!date) {
                date = this.$moment(this.consumed_at, MOMENT_JS_CONSUMED_DATE_FORMAT)
                    .format(MOMENT_JS_CONSUMED_DATE_OVERVIEW_FORMAT);
            }
            this.fetchConsumedFoodGroupsAt({
                from: date,
                to: date,
            });
        },
    }
}
