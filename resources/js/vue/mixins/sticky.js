export default {

    data() {
        return {
            sticky: {
                menu: false,
                offset: {
                    navbar: '{top:57}',
                },
            },
        };
    },

    computed: {
        stickyClass() {
            return [
                {'border-bottom': this.sticky.menu},
                {'shadow': this.sticky.menu},
                {'bg-white': this.sticky.menu},
            ];
        },
        stickyOffsetNavbar() {
            return this.sticky.offset.navbar;
        },
        isMenuSticked() {
            return this.sticky.menu;
        }
    },

    methods: {
        onStick(obj) {
            this.sticky.menu = obj.sticked;
            if (this.sticky.menu) {
                $('.navbar.fixed-top').addClass('shadow-none');
            } else {
                $('.navbar.fixed-top').removeClass('shadow-none');
            }
        },
    },
}
