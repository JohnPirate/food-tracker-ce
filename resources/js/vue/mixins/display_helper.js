import {
    MOMENT_JS_CONSUMED_DATE_OVERVIEW_FORMAT,
    MOMENT_JS_DATE_TIME_FORMAT,
} from '../../config';

export default {
    methods: {
        displayDate(date) {
            return this.$moment(date, MOMENT_JS_DATE_TIME_FORMAT)
                .format(MOMENT_JS_CONSUMED_DATE_OVERVIEW_FORMAT);
        },
    }
}
