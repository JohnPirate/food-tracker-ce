import {Food} from '../objects/food';

export default {
    props: {
        food: {
            type: Food,
            required: true
        }
    },
    computed: {
        getFood() {
            return this.food;
        }
    }
}
