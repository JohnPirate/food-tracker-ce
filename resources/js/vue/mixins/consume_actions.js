import ConsumedFoodsApi from '../service/consumed_foods';

import { createNamespacedHelpers } from 'vuex';

const {
    mapActions: mapConsumedFoodsFormActions,
    mapState: mapConsumedFoodsFormStates,
    mapGetters: mapConsumedFoodsFormGetters,
} = createNamespacedHelpers('consumedFoods/consumedFoodsForm');


export default {
    methods: {
        ...mapConsumedFoodsFormActions([
            'requestStoreConsumedFood',
            'receiveStoreConsumedFoodSuccess',
            'receiveStoreConsumedFoodError',
        ]),
        onConsume(food) {
            const hub = this.$root;
            hub.$emit(`action::newConsumedFood`, food);
        },
        storeConsumedFoodAction(data) {
            const vm = this;
            this.requestStoreConsumedFood(true);
            return ConsumedFoodsApi.storeConsumedFood(data)
                .then((response) => {
                    vm.receiveStoreConsumedFoodSuccess(response.data);
                    return response;
                })
                .catch((error) => {
                    vm.receiveStoreConsumedFoodError(error);
                    return error;
                })
            ;
        },
    }
}
