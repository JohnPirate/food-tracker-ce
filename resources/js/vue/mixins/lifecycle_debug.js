export default {
    beforeCreate() {
        console.log(`lifecycle - beforeCreate - ${this.$el}`);
    },

    created() {
        console.log(`lifecycle - created - ${this.$el}`);
    },

    beforeMount() {
        console.log(`lifecycle - beforeMount - ${this.$el}`);
    },

    mounted() {
        console.log(`lifecycle - mounted - ${this.$el}`);
    },

    beforeUpdate() {
        console.log(`lifecycle - beforeUpdate - ${this.$el}`);
    },

    updated() {
        console.log(`lifecycle - updated - ${this.$el}`);
    },

    activated() {
        console.log(`lifecycle - activated - ${this.$el}`);
    },

    deactivated() {
        console.log(`lifecycle - deactivated - ${this.$el}`);
    },

    beforeDestroy() {
        console.log(`lifecycle - beforeDestroy - ${this.$el}`);
    },

    destroyed() {
        console.log(`lifecycle - destroyed - ${this.$el}`);
    },

    errorCaptured() {
        console.log(`lifecycle - errorCaptured - ${this.$el}`);
    },
};
