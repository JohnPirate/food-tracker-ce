export default {
    props: {
        foodId: {
            type: Number,
            required: true
        }
    }
}
