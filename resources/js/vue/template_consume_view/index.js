import Vue from 'vue';
import TemplateConsumeView from './components/template_consume_view';
import store from '../template_edit_view/store/index';

const elementId = 'template-consume-view';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            TemplateConsumeView,
        },

        store,

        data() {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                templateId: parseInt(environmentsData.templateId),
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    templateId: this.templateId,
                }
            });
        }
    });
});
