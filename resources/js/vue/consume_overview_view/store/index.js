import Vue from 'vue';
import Vuex from 'vuex';
import {consumedFoods} from '../../store/modules/consumed_foods';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export const createStore = () => new Vuex.Store({
    modules: {
        consumedFoods,
    },
    strict: debug
});
export default createStore();
