import Vue from 'vue';
import ConsumeOverviewView from './components/consume_overview_view';
import store from './store/index';

const elementId = 'consume-overview-view';

document.addEventListener('DOMContentLoaded', function () {

    new Vue({

        el: `#${elementId}`,

        components: {
            ConsumeOverviewView,
        },

        store,

        data () {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                from: environmentsData.from,
                to: environmentsData.to,
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    from: this.from,
                    to: this.to,
                }
            });
        }
    });
});
