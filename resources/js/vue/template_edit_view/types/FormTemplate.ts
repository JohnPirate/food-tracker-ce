import type { TemplateFood } from "@/types/v2/TemplateFood";

interface FormTemplate {
    name: string
    description: string
    portion: number
    foods: Partial<TemplateFood[]>
}

export type { FormTemplate };
