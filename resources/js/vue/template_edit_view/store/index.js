import Vue from 'vue';
import Vuex from 'vuex';
import {consumedFoods} from '../../store/modules/consumed_foods';
import VuexORM from '@vuex-orm/core'
import Template from '../../models/Template';
import templates from '../../store/modules/templates/index';
import Ingredient from '../../models/Ingredient';
import ingredients from '../../store/modules/ingredients/index';
import Food from '../../models/Food';
import foods from '../../store/modules/foods/index';
import FoodNurient from '../../models/FoodNutrient';

Vue.use(Vuex);

const database = new VuexORM.Database();
database.register(Template, templates);
database.register(Ingredient, ingredients);
database.register(Food, foods);
database.register(FoodNurient);

const debug = process.env.NODE_ENV !== 'production';

export const createStore = () => new Vuex.Store({
    plugins: [VuexORM.install(database)],
    modules: {
        consumedFoods,
    },
    strict: debug
});
export default createStore();
