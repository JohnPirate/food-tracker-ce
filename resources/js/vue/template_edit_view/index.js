import Vue from 'vue';
import TemplateEditView from './components/template_edit_view';

const elementId = 'template-edit-view';

document.addEventListener('DOMContentLoaded', function () {
    new Vue({

        el: `#${elementId}`,

        components: {
            TemplateEditView,
        },

        data() {
            const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                // data properties
                templateId: parseInt(environmentsData.templateId),
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {
                    templateId: this.templateId,
                }
            });
        }
    });
});
