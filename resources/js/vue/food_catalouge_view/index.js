import Vue from 'vue';
import FoodCatalougeView from './components/food_catalouge_view';
import store from './store/index';

const elementId = 'food-catalouge-view';

document.addEventListener('DOMContentLoaded', function () {

    new Vue({

        el: `#${elementId}`,

        components: {
            FoodCatalougeView,
        },

        store,

        
        data () {
            // const environmentsData = document.querySelector(this.$options.el).dataset;
            return {
                // data properties
            };
        },

        render(createElement) {
            return createElement(elementId, {
                props: {

                }
            });
        }
    });
});
