<?php

return [
    'email' => [
        'title' => 'E-Mail',
        'placeholder' => 'E-Mail Adresse eingeben',
        'aria_label' => 'E-Mail Adresse eingeben.',
    ],
    'username' => [
        'title' => 'Benutzername',
        'placeholder' => 'Benutzernamen eingeben',
        'aria_label' => 'Benutzernamen eingeben.',
    ],
    'password' => [
        'title' => 'Passwort',
        'placeholder' => 'Passwort eingeben',
        'aria_label' => 'Passwort eingeben.',
    ],
    'password_confirmation' => [
        'title' => 'Passwort bestätigen',
        'placeholder' => 'Passwort bestätigen',
        'aria_label' => 'Passwort bestätigen.',
    ],
    'remember' => [
        'title' => 'Angemeldet bleiben',
        'aria_label' => 'Angemeldet bleiben.',
    ],
    'agree_terms' => [
        'title' => 'Ich akzeptiere die Allgemeinen Geschäftsbedingungen',
        'aria_label' => 'Hiermit akzeptierst du die Allgemeinen Geschäftsbedingungen.',
    ],
    'search_foods' => [
        'placeholder' => 'Lebensmittel suchen',
        'placeholder_instant_search' => 'Suche nach Lebensmittel...',
        'aria_label' => 'Lebensmittel suchen.',
    ],
    'consumed_at' => [
        'title' => 'Konsumiert am',
        'aria_label' => 'Konsumiert am.',
    ],
    'food_groups' => [
        'title' => 'Lebensmittelgruppe',
        'aria_label' => 'Wähle eine Lebensmittelgruppe.',
        'description' => 'Wähle eine Lebensmittelgruppe, gib eine Neue ein oder lass einfach das Feld frei.',
        'placeholder' => 'Gruppe auswählen',
        'tag_placeholder' => 'Als neue Gruppe hinzufügen',
        'select_label' => 'Drücke Enter um auszuwählen',
        'select_group_label' => 'Drücke Enter um Gruppe auszuwählen',
        'selected_label' => 'Ausgewählt',
        'deselect_label' => 'Drücke Enter um zu entfernen',
        'deselect_group_label' => 'Drücke Enter um Gruppe abzuwählen',
        'opt_groups' => [
            'used_groups' => 'Verwendete Gruppen',
            'other_groups' => 'Weitere Gruppen',
            'groups' => 'Gruppen',
        ],
    ],
    'consumed_value' => [
        'title' => 'Menge',
        'aria_label' => 'Menge - Wie viel möchtest du konsumieren?',
    ],
    'date_from' => [
        'title' => 'Wähle einen Tag aus',
        'aria_label' => 'Wähle einen Tag aus.',
    ],
    'select_time' => [
        'title' => 'Uhrzeit',
        'aria_label' => 'Wähle eine Uhrzeit.',
    ],
    'copy_consumed_group_at' => [
        'title' => 'Kopiere Gruppe',
        'aria_label' => ' to date.',
    ],
    'food_name' => [
        'title' => 'Name',
        'aria_label' => 'Name des Lebensmittel.',
    ],
    'food_brand' => [
        'title' => 'Marke',
        'aria_label' => 'Markenname.',
    ],
    'food_barcode' => [
        'title' => 'Strichcode',
        'aria_label' => 'Strichcode.',
    ],
    'food_description' => [
        'title' => 'Zusätzliche Informationen',
        'aria_label' => 'Beschreibung des Lebensmittels.',
    ],
    'food_private' => [
        'title' => 'Lebensmittel ist nur für mich sichtbar',
        'aria_label' => 'Lebensmittel als privat markieren.',
    ],
    'food_ref_value' => [
        'title' => 'Referenzmenge',
        'aria_label' => 'Referenzmenge des Lebensmittels.',
    ],
    'food_ref_unit' => [
        'title' => 'Einheit',
        'aria_label' => 'Einheit der Referenzmenge.',
    ],
    'food_carb_without_fiber' => [
        'title' => 'Die Kohlenhydratmenge ist exklusive Ballaststoffe',
        'aria_label' => 'Die Kohlenhydratmenge ist exklusive Ballaststoffe.',
    ],
    'nutrient_name' => [
        'title' => 'Nährstoff',
        'aria_label' => 'Name des Nährstoffs',
        'description' => 'Wähle einen Nährstoff',
        'placeholder' => 'Wähle einen Nährstoff',
        'tag_placeholder' => 'Als neue Gruppe hinzufügen',
        'select_label' => 'Drücke Enter um auszuwählen',
        'select_group_label' => 'Drücke Enter um Gruppe auszuwählen',
        'selected_label' => 'ausgewählt',
        'deselect_label' => 'Drücke Enter um zu entfernen',
        'deselect_group_label' => 'Drücke Enter um Gruppe abzuwählen',
    ],
    'nutrient_value' => [
        'title' => 'Menge',
        'aria_label' => 'Menge des Mährstoffs.',
    ],
    'nutrient_unit' => [
        'title' => 'Einheit',
        'aria_label' => 'Einheit der Nährstoffmenge.',
    ],
    'dont_remove_public_foods' => [
        'title' => 'Lösche nicht meine öffentlichen Lebensmittel',
        'aria_label' => 'Lösche nicht meine öffentlichen Lebensmittel.',
        'help' => 'Diese Option bietet anderen Mitgliedern die Möglichkeit deine Lebensmittel, die du erstellt hast und nicht als privat markiert hast, weiterhin nutzen zu können.',
    ],
    'template_name' => [
        'title' => 'Name',
        'title2' => 'Name der Vorlage',
        'aria_label' => 'Name der Vorlage.',
    ],
    'template_description' => [
        'title' => 'Beschreibung',
        'aria_label' => 'Beschreibung der Vorlage.',
    ],
    'template_group' => [
        'title' => 'Gruppenname',
        'aria_label' => 'Gruppenname unter dessen die Lebensmittel gruppiert werden.',
        'help' => 'Verwende einen eigenen Gruppennamen wenn der Templatename nicht zur Auswahl passt.',
    ],
    'template_portion' => [
        'title' => 'Portionen',
        'aria_label' => 'Anzahl der Portionen.',
        'help' => 'Beschreibt die Anzahl der Portionen, die diese Vorlage beinhaltet.',
    ],
    'template_portion_consume' => [
        'title' => 'Konsumiere portionen',
        'aria_label' => 'Wie viele Portionen möchtest du von der Vorlage konsumieren?',
        'help' => 'Wie viele Portionen möchtest du von der Vorlage konsumieren?',
    ],
    'ingredient_value' => [
        'title' => 'Menge',
        'aria_label' => 'Menge der Zutat in :unit.',
    ],
    'search_templates' => [
        'placeholder' => 'Vorlage suchen',
        'aria_label' => 'Vorlage suchen.',
    ],
    'templates' => [
        'title' => 'Vorlage',
        'aria_label' => 'Wähle eine Vorlage.',
        'placeholder' => 'Wähle eine Vorlage',
        'select_label' => 'Drücke Enter um auszuwählen',
        'select_template_label' => 'Drücke Enter um Vorlage auszuwählen',
        'selected_label' => 'ausgewählt',
        'deselect_label' => 'Drücke Enter um zu entfernen',
        'deselect_template_label' => 'Drücke Enter um Vorlage abzuwählen',
    ],
    'template_food_value' => [
        'title' => 'Menge',
        'aria_label' => 'Menge des Lebensmittel für Vorlage.',
    ],
];
