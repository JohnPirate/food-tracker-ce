<?php

return [
    /*
    |---------------------------------------------------------------------------
    | Buttons
    |---------------------------------------------------------------------------
    */
    'button' => [
        'new' => [
            'text' => 'Neu',
            'title' => 'Neu',
        ],
        'save' => [
            'text' => 'Speichern',
            'title' => ':action speichern.',
        ],
        'add' => [
            'text' => 'Hinzufügen',
            'title' => ':action hinzufügen.',
        ],
        'create' => [
            'text' => 'Anlegen',
            'title' => ':action anlegen.',
        ],
        'edit' => [
            'text' => 'Bearbeiten',
            'title' => ':action bearbeiten.',
        ],
        'back' => [
            'text' => 'Zurück',
            'title' => 'zurück.',
        ],
        'abort' => [
            'text' => 'Abbrechen',
            'title' => ':action abbrechen.',
        ],
        'cancel' => [
            'text' => 'Beenden',
            'title' => ':action beenden.',
        ],
        'remove' => [
            'text' => 'Entfernen',
            'title' => ':action entfernen.',
            'title_confirm' => ':action entfernen bestätigen.',
        ],
        'delete' => [
            'text' => 'Löschen',
            'title' => ':action löschen.',
            'title_confirm' => ':action löschen bestätigen.',
        ],
        'reset' => [
            'text' => 'Zurücksetzen',
            'title' => ':action zurücksetzen.',
        ],
        'show' => [
            'text' => 'Anzeigen',
            'title' => ':action anzeigen.',
        ],
        'sign_in' => [
            'text' => 'Anmelden',
            'title' => 'Mit deinem :appname-Konto anmelden.',
        ],
        'register' => [
            'text' => 'Registrieren',
            'title' => 'Ein :appname-Konto erstellen.',
        ],
        'send_password_reset_link' => [
            'text' => 'Passwort zurücksetzen Link senden',
            'title' => 'Passwort zurücksetzen Link senden.',
        ],
        'password_reset' => [
            'text' => 'Passwort zurücksetzen',
            'title' => 'Passwort zurücksetzen.',
        ],
        'new_food' => [
            'text' => 'Neues Lebensmittel',
            'title' => 'Erstelle ein neues Lebensmittel.',
        ],
        'consume' => [
            'text' => 'Konsumieren',
            'text2' => 'Lebensmittel konsumieren',
            'title' => 'Lebensmittel konsumieren.',
        ],
        'consume_from_template' => [
            'text' => 'Von Vorlage konsumieren',
            'title' => 'Von Vorlage konsumieren.',
        ],
        'add_to_favorites' => [
            'text' => 'Zu Favoriten hinzufügen',
            'title' => ':something zu Favoriten hinzufügen.',
        ],
        'go_to_date' => [
            'text' => 'Tag aufrufen',
            'title' => 'Ausgewähltes Datum aufrufen.',
        ],
        'today' => [
            'text' => 'Heute',
            'title' => 'Heutigen Tag aufrufen.',
        ],
        'copy_group' => [
            'text' => 'Kopieren',
            'title' => 'Gruppe kopieren.'
        ],
        'create_template' => [
            'text' => 'Vorlage erstellen',
            'title' => 'Erstelle Vorlage aus Gruppe.'
        ],
        'copy_group_action' => [
            'text' => 'Gruppe kopieren',
            'title' => 'Gruppe kopieren.'
        ],
        'create_template_from_food_grouping' => [
            'text' => 'Vorlage erstellen',
            'title' => 'Vorlage erstellen.'
        ],
        'dont_remove_public_foods' => [
            'text' => 'Konto löschen',
            'title' => 'Konto löschen.'
        ],
        'new_template' => [
            'text' => 'Neue Vorlage',
            'text_action' => 'Erstelle eine neue Vorlage',
            'title' => 'Erstelle eine neue Vorlage.',
        ],
        'copy_template' => [
            'text' => 'Kopieren',
            'title' => 'Template kopieren.'
        ],
        'add_food_to_template' => [
            'text' => 'Lebensmittel hinzufügen',
            'text_mnu' => 'Zu Vorlage hinzufügen',
            'text_action' => 'Zu Vorlage hinzufügen',
            'title' => 'Lebensmittel zu Vorlage hinzufügen.'
        ],
        'edit_template_to_add_food' => [
            'text' => 'Lebensmittel hinzufügen',
            'title' => 'Lebensmittel hinzufügen.'
        ],
        'prev_day' => [
            'text' => 'Vorheriger Tag',
            'title' => 'Gehe zu vorherigen Tag.'
        ],
        'next_day' => [
            'text' => 'Nächster Tag',
            'title' => 'Gehe zu nächsten Tag.'
        ],
        'update_personal_data' => [
            'text' => 'Benutzerdaten speichern',
            'title' => 'Speichere deine Benutzerdaten',
        ],
    ],

    /*
    |---------------------------------------------------------------------------
    | States
    |---------------------------------------------------------------------------
    */
    'states' => [
        'no_foods_found' => [
            'title' => 'Keine Lebensmittel gefunden',
            'message' => [
                'db_empty' => 'Es existiert noch kein Lebensmittel in der Datenbank.',
                'nothing_found' => 'Es konnte kein Lebensmittel unter dem Begriff \':query\' gefunden werden',
            ],
        ],
        'nothing_consumed' => [
            'title' => 'Keine Aufzeichnung',
            'message' => 'Für heute gibt es noch keine Aufzeichnung.',
        ],
        'no_templates_found' => [
            'title' => [
                'db_empty' => 'Noch keine Vorlagen',
                'nothing_found' => 'Keine Vorlage gefunden',
            ],
            'message' => [
                'db_empty' => 'Du hast noch keine Vorlage erstellt.',
                'nothing_found' => 'Es konnte keine Vorlage unter dem Begriff \':query\' gefunden werden',
            ],
        ],
        'no_ingredients' => [
            'title' => 'Keine Zutaten',
            'message' => 'Diese Vorlage beinhaltet noch keine Zutaten.',
        ],
        'no_foods' => [
            'title' => [
                'no_query' => 'Nach Lebensmittel suchen',
                'nothing_found' => 'Kein Lebensmittel gefunden',
            ],
            'message' => [
                'no_query' => 'Benutze das Suchfeld nach Lebensmittel zu suchen',
                'nothing_found' => 'Es konnte kein Lebensmittel unter dem Begriff \':query\' gefunden werden',
            ],
        ],
    ],

    /*
    |---------------------------------------------------------------------------
    | Modals
    |---------------------------------------------------------------------------
    */
    'modals' => [
        'consume_food_insert_update_modal' => [
            'header' => [
                'new' => 'Neuen Eintrag speichern',
                'edit' => 'Eintrag aktualisieren',
            ],
            'cancel' => [
                'new' => 'Neuer Eintrag',
                'edit' => 'Aktualisieren',
            ],
        ],
        'copy_consumed_foods_group_modal' => [
            'header' => 'Gruppe kopieren',
        ],
        'delete_consumed_food' => [
            'message' => 'Möchtest du wirklich diesen Eintrag löschen?',
        ],
        'delete_consumed_food_group' => [
            'message' => 'Möchtest du wirklich die gesamte Gruppierung löschen?',
        ],
        'delete_food' => [
            'message' => 'Möchtest du wirklich das Lebensmittel löschen?',
        ],
        'dont_remove_public_foods' => [
            'message' => 'Möchtest du wirklich dein Konto löschen?',
        ],
        'search_foods' => [
            'header' => 'Lebensmittel suchen',
        ],
        'delete_template' => [
            'message' => 'Möchtest du wirklich die Vorlage löschen?',
        ],
        'copy_template' => [
            'message' => 'Möchtest du wirklich eine Kopie der Vorlage erstellen?',
        ],
        'add_food_to_template' => [
            'header' => 'Lebensmittel zu Vorlage hinzufügen',
        ],
        'create_template_from_food_grouping' => [
            'header' => 'Vorlage erstellen',
        ],
    ],

    /*
    |---------------------------------------------------------------------------
    | Vue
    |---------------------------------------------------------------------------
    */
    'vue' => [
        'food_meta_form' => [
            'title' => 'Metadaten Lebensmittel',
        ],
        'template_form' => [
            'title1' => 'Metadaten Vorlage',
            'title2' => 'Lebensmittel',
        ],
        'frequently_consumed_foods' => [
            'title' => 'Häufig konsumierte Lebensmittel',
            'info' => 'Folgende Lebensmittel wurden in den vergangenen 30 Tagen am öftesten konsumiert',
            'btn' => 'Weitere Lebensmittel suchen',
        ],
    ],
];
