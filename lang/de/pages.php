<?php

return [
    'reset_password' => [
        'title' => 'Passwort wiederherstellen',
    ],
    'password_update' => [
        'title' => 'Passwort wiederherstellen',
    ],
    'verify_email' => [
        'title' => 'Bestätige deine E-Mail Adresse',
        'text' => [
            'verify_1' => 'Du hast einen Aktivierungslink zugeschickt bekommen. Um fortfahren zu können klicke auf den Link und bestätige somit deine E-Mail Adresse.',
            'verify_2' => 'Hast du keine E-Mail bekommen',
            'verify_3' => 'klicke hier um einen neuen Aktivierungslink anzufordern.',
        ],
    ],
    'food_catalouge' => [
        'title' => 'Lebensmittel Katalog',
    ],
    'consume_overview' => [
        'title' => 'Tagebuch',
    ],
    'food_new' => [
        'title' => 'Neues Lebensmittel erstellen',
    ],
    'food_edit' => [
        'title' => 'Lebensmittel Bearbeiten',
    ],
    'settings' => [
        'title' => 'Einstellungen',
        'block_delete_account' => [
            'title' => 'Account löschen',
            'text' => [
                'general' => 'Alle deine Daten werden gelöscht!',
            ],
            'subtitle' => [
                'opt' => 'Optional',
            ],
        ],
        'block_save_user_data' => [
            'title' => 'Benutzerdaten',
            'text' => [
                'general' => 'Hier kannst du deine persönlichen Daten abändern.',
            ],
        ],
    ],
    'templates_overview' => [
        'title' => 'Vorlagen',
    ],
    'template_new' => [
        'title' => 'Neue Vorlage erstellen',
    ],
    'template_edit' => [
        'title' => 'Vorlage bearbeiten',
    ],
    'template_consume' => [
        'title' => ':name konsumieren',
        'breadcrumb' => 'Vorlage konsumieren',
        'text' => [
            'default_portion' => 'Portionsgröße der Vorlage',
        ],
    ],
    'template_show' => [
        'subtitles' => [
            'meta' => 'Metadaten',
            'ingredients' => 'Zutaten',
            'nutrients' => 'Nährstoffe',
        ],
        'text' => [
            'portions' => 'Portionen',
            'description' => 'Beschreibung',
            'edited' => 'Zuletzt bearbeitet',
        ],
    ],
    'dashboard' => [
        'title' => 'Dashboard',
    ],
];
