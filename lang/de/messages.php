<?php

return [
    'internal_error' => 'Es tut uns leid. Leider ist ein interner Fehler aufgetreten. Bitte kontaktiere das Support Team.',
    'not_found' => 'Eintrag nicht gefunden.',
    'authentication_expired' => 'Deine Sitzung ist abgelaufen. Bitte melde dich neu an.',
    'not_authenticated' => 'Du hast keine aktive Sitzung. Bitte melde dich an.',
    'not_authorized' => 'Fehlende Berechtigungen.',
    'not_allowed_to_delete_food' => 'Dir ist es nicht erlaubt dieses Lebensmittes zu bearbeiten.',
    'fresh_verification_link_has_been_sent' => 'Ein neuer Bestätigungslink wurde an deine E-Mail Adresse gesendet.',
    'no_food_for_id' => 'Für ID :id konnte kein Lebensmittel gefunden werden.',
    'no_consumed_data_for_id' => 'Für ID :id konnten keine konsumierten Lebensmittel gefunden werden.',
    'no_group_for_id' => 'Für ID :id konnte keine Gruppe gefunden werden.',
    'no_food_ownership' => 'Dieses Lebensmittel gehört nicht dir.',
    'food_already_exists_as_ingredient' => ':name existiert bereits als Zutat.',
    'success_store' => 'Erfolgreich hinzugefügt.',
    'success_update' => 'Erfolgreich bearbeitet.',
    'success_deleted' => 'Erfolgreich gelöscht.',
    'success_copied' => 'Erfolgreich kopiert.',
    'success_consumed' => 'Erfolgreich konsumiert.',
    'success_food_added_to_template' => 'Lebensmittel erfolgreich zur Vorlage hinzugefügt.',
    'success_store_data' => ':data erfolgreich hinzugefügt.',
    'success_update_data' => ':data erfolgreich bearbeitet.',
    'success_deleted_data' => ':data erfolgreich gelöscht.',
    'success_delete_group_entries' => 'Gruppierung erflogreich gelöscht.',
    'success_create_template_from_food_grouping' => 'Vorlage erfolgreich erstellt.',

    // Template
    'ask_to_delete_template' => 'Bist du dir sicher, dass du diese Vorlage löschen möchtest?',
];
