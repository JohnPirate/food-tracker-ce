<?php

return [
    'register' => [
        'text' => 'Konto erstellen',
        'title' => 'Erstelle ein neues Konto.',
    ],
    'sign_in' => [
        'text' => 'Anmelden',
        'title' => 'Zur Anmeldung.',
    ],
    'sign_out' => [
        'text' => 'Abmelden',
        'title' => 'Melde dich von der Applikation ab.',
    ],
    'forgot_password' => [
        'text' => 'Passwort vergessen?',
        'title' => 'Hier kannst du dein Passwort zurücksetzen.',
    ],
    'reset_password' => [
        'text' => 'Passwort zurücksetzen',
        'title' => 'Hier kannst du dein Passwort zurücksetzen lassen.',
    ],
    'terms_and_policy' => [
        'text' => 'Allgemeine Geschäftsbedingungen',
        'title' => 'Zu den Allgemeinen Geschäftsbedingungen.',
    ],
    'settings' => [
        'text' => 'Einstellungen',
        'title' => 'Account Einstellungen.',
    ],
    'catalogue' => [
        'text' => 'Lebensmittel',
        'title' => 'Hier kommst du zum Lebensmittel Katalog.',
    ],
    'consumed_foods' => [
        'text' => 'Tagebuch',
        'title' => 'Hier kommst du zum Tagebuch.',
    ],
    'food_detail_view' => [
        'text' => 'Detailansicht',
        'title' => 'Hier kommst du zur Detailansicht des Lebensmittel.',
    ],
    'repo' => [
        'text' => 'Gitlab',
        'title' => 'Hier kommst du zum Gitlab Repository.',
    ],
    'licence' => [
        'text' => 'MIT',
        'title' => 'Hier kommst du zur Applikations Lizenz.',
    ],
    'go_to_source' => [
        'title' => 'Hier kommst du zur Daten-Quelle.',
    ],
    'verification_resend' => [
        'title' => 'Ein neuer Aktivierungslink wird dir auf deine E-Mail Adresse geschickt.',
    ],
    'templates_overview' => [
        'text' => 'Vorlagen',
        'title' => 'Hier kommst du zu den Vorlagen.',
    ],
    'template_detail_view' => [
        'text' => 'Detailansicht',
        'title' => 'Hier kommst du zur Detailansicht der Vorlage.',
    ],
];
