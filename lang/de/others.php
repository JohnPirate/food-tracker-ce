<?php

return [
    'toggle_navigation' => 'Toggle navigation',
    'branded_food' => 'Dieses Lebensmittel ist ein Markenprodukt.',
    'imported_food' => 'Dieses Lebensmittel wurde von einer externen Resource importiert.',
    'no_group' => 'Konsumierte Lebensmittel',
    'no_group_2' => 'Weitere Lebensmittel',
    'form' => 'Formular',
    'copy' => 'Kopie',
    'created_at' => 'Angelegt am',
    'updated_at' => 'Zuletzt bearbeitet',
    'change_translation' => 'Ändere Sprache zu :name',
    'consumed_calories' => 'Konsumierte Kalorien',
    'last_meal_ago' => 'Zuletzt konsumiert',
    'macro_nutrients' => 'Makro Nährstoffe',
    'info' => 'Info',
    'weekdays' => [
        'first_char' => [
            'M', 'D', 'M', 'D', 'F', 'S', 'S',
        ],
        'short' => [
            'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So',
        ],
    ],

    'weekly_overview' => 'Wochenübersicht',
    'nutrition_facts' => [
        'simple' => 'Einfach',
        'full' => 'Vollständig',
    ],
];
