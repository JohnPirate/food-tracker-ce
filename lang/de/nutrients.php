<?php

// Autogenerated: 2019-01-04 11:51:51

return [

    'groups' => [
        'proximates' => 'Proximate',
        'others' => 'Andere',
        'minerals' => 'Mineralstoffe',
        'vitamins' => 'Vitamine',
        'aminos' => 'Aminosäuren',
        'lipids' => 'Lipide',
        'isoflavones' => 'Isoflavone',
        'protein' => 'Eiweiß',
        'carb' => 'Kohlenhydrate',
        'carb_short' => 'KH',
        'carb_no_fiber' => 'KH (ohne Ballaststoffe)',
        'fat' => 'Fett',
    ],

    // Nutrients
    'protein' => "Eiweiß",
    'salt' => 'Salz',
    'total_lipid_fat' => "Fett",
    'carbohydrate_by_difference' => "Kohlenhydrate, gesamt",
    'carbohydrate' => "Kohlenhydrate, ohne Ballaststoffe",
    'ash' => "Asche",
    'energy' => "Energie",
    'starch' => "Stärke",
    'sucrose' => "Saccharose (Haushaltszucker)",
    'glucose_dextrose' => "Glucose (Dextrose, Traubenzucker)",
    'fructose' => "Fruktose (Fruchtzucker)",
    'lactose' => "Laktose (Milchzucker)",
    'maltose' => "Maltose (Malzzucker)",
    'alcohol_ethyl' => "Alkohol (Ethylalkohol)",
    'polyhydric_alcohols' => 'mehrwertige Alkohole',
    'water' => "Wasser",
    'adjusted_protein' => "Bereinigtes Protein",
    'caffeine' => "Koffein",
    'theobromine' => "Theobromin",
    'sugars_total' => "Zucker, gesamt",
    'galactose' => "Galaktose (Schleimzucker)",
    'fiber_total_dietary' => "Ballaststoffe",
    'fiber_soluble' => "Ballaststoffe, löslich",
    'fiber_insoluble' => "Ballaststoffe, unlöslich",
    'sugar_alcoholstotal' => "Zuckeralkohole, gesamt",
    'calcium_ca' => "Kalzium, Ca",
    'iron_fe' => "Eisen, Fe",
    'magnesium_mg' => "Magnesium, Mg",
    'phosphorus_p' => "Phosphor, P",
    'potassium_k' => "Kalium, K",
    'sodium_na' => "Natrium, Na",
    'zinc_zn' => "Zink, Zn",
    'copper_cu' => "Kupfer, Cu",
    'fluoride_f' => "Fluorid, F",
    'iodinei' => "Jod, I",
    'manganese_mn' => "Mangan, Mn",
    'selenium_se' => "Selen, Se",
    'chromium_cr' => 'Chrom, Cr',
    'molybdenum_mo' => 'Molybdän, Mo',
    'retinol' => "Vitamin A1 (Retinol)",
    'vitamin_a_rae' => "Vitamin A, RAE",
    'carotene_beta' => "Beta Carotin",
    'carotene_alpha' => "Alpha Carotin",
    'vitamin_e_alpha_tocopherol' => "Vitamin E (Alpha Tocopherol)",
    'vitamin_d2_ergocalciferol' => "Vitamin D2 (Ergocalciferol)",
    'vitamin_d3_cholecalciferol' => "Vitamin D3 (Cholecalciferol)",
    'vitamin_d_d2_d3' => "Vitamin D (D2 + D3)",
    'cryptoxanthin_beta' => "Cryptoxanthin, beta",
    'lycopene' => "Lycopin",
    'lutein_zeaxanthin' => "Lutein + Zeaxanthin",
    'tocopherol_beta' => "Tocopherol, Beta",
    'tocopherol_gamma' => "Tocopherol, Gamma",
    'tocopherol_delta' => "Tocopherol, Delta",
    'tocotrienol_alpha' => "Tocotrienol, Alpha",
    'tocotrienol_beta' => "Tocotrienol, Beta",
    'tocotrienol_gamma' => "Tocotrienol, Gamma",
    'tocotrienol_delta' => "Tocotrienol, Delta",
    'vitamin_c_total_ascorbic_acid' => "Vitamin C (Askorbinsäure)",
    'thiamin' => "Vitamin B1 (Thiamin)",
    'riboflavin' => "Vitamin B2 (Riboflavin)",
    'niacin' => "Vitamin B3 (Niacin)",
    'pantothenic_acid' => "Vitamin B5 (Pantothensäure)",
    'vitamin_b_6' => "Vitamin B6",
    'biotin' => "Biotin",
    'folate_total' => "Folate, gesamt",
    'vitamin_b_12' => "Vitamin B12",
    'choline_total' => "Cholin, gesamt",
    'menaquinone_4' => "Vitamin K2 (Menachinon-4)",
    'dihydrophylloquinone' => "Vitamin K2 (Dihydrophylloquinon)",
    'vitamin_k_phylloquinone' => "Vitamin K (Phylloquinon)",
    'folic_acid' => "Folsäure",
    'folate_food' => "Folate Lebensmittel",
    'folate_dfe' => "Folate, DFE",
    'betaine' => "Betaine",
    'tryptophan' => "Tryptophan",
    'threonine' => "Threonine",
    'isoleucine' => "Isoleucine",
    'leucine' => "Leucine",
    'lysine' => "Lysine",
    'methionine' => "Methionine",
    'cystine' => "Cystine",
    'phenylalanine' => "Phenylalanine",
    'tyrosine' => "Tyrosine",
    'valine' => "Valine",
    'arginine' => "Arginine",
    'histidine' => "Histidine",
    'alanine' => "Alanine",
    'aspartic_acid' => "Aspartic acid",
    'glutamic_acid' => "Glutamic acid",
    'glycine' => "Glycine",
    'proline' => "Proline",
    'serine' => "Serin",
    'hydroxyproline' => "Hydroxyproline",
    'sugars_added' => "Zucker, hinzugefügt",
    'vitamin_e_added' => "Vitamin E, hinzugefügt",
    'vitamin_b_12_added' => "Vitamin B12, hinzugefügt",
    'cholesterol' => "Cholesterin",
    'fatty_acids_total_trans' => "Trans-Fettsäuren, gesamt",
    'fatty_acids_total_saturated' => "Gesättigte Fettsäuren, gesamt",
    '40' => "4:0",
    '60' => "6:0",
    '80' => "8:0",
    '100' => "10:0",
    '120' => "12:0",
    '140' => "14:0",
    '160' => "16:0",
    '180' => "18:0",
    '200' => "20:0",
    '181_undifferentiated' => "18:1 undifferentiated",
    '182_undifferentiated' => "18:2 undifferentiated",
    '183_undifferentiated' => "18:3 undifferentiated",
    '204_undifferentiated' => "20:4 undifferentiated",
    '226_n_3_dha' => "22:6 n-3 (DHA)",
    '220' => "22:0",
    '141' => "14:1",
    '161_undifferentiated' => "16:1 undifferentiated",
    '184' => "18:4",
    '201' => "20:1",
    '205_n_3_epa' => "20:5 n-3 (EPA)",
    '221_undifferentiated' => "22:1 undifferentiated",
    '225_n_3_dpa' => "22:5 n-3 (DPA)",
    'phytosterols' => "Phytosterols",
    'stigmasterol' => "Stigmasterol",
    'campesterol' => "Campesterol",
    'beta_sitosterol' => "Beta-sitosterol",
    'fatty_acids_total_monounsaturated' => "Einfach ungesättigte Fettsäuren, gesamt",
    'fatty_acids_total_polyunsaturated' => "Mehrfach ungesättigte Fettsäuren, gesamt",
    '150' => "15:0",
    '170' => "17:0",
    '240' => "24:0",
    '161_t' => "16:1 t",
    '181_t' => "18:1 t",
    '221_t' => "22:1 t",
    '182_t_not_further_defined' => "18:2 t not further defined",
    '182_i' => "18:2 i",
    '182_tt' => "18:2 t,t",
    '182_clas' => "18:2 CLAs",
    '241_c' => "24:1 c",
    '202_n_6_cc' => "20:2 n-6 c,c",
    '161_c' => "16:1 c",
    '181_c' => "18:1 c",
    '182_n_6_cc' => "18:2 n-6 c,c",
    '221_c' => "22:1 c",
    '183_n_6_ccc' => "18:3 n-6 c,c,c",
    '171' => "17:1",
    '203_undifferentiated' => "20:3 undifferentiated",
    'fatty_acids_total_trans_monoenoic' => "Fatty acids, total trans-monoenoic",
    'fatty_acids_total_trans_polyenoic' => "Fatty acids, total trans-polyenoic",
    '130' => "13:0",
    '151' => "15:1",
    'daidzein' => "Daidzein",
    'genistein' => "Genistein",
    'glycitein' => "Glycitein",
    'total_isoflavones' => "Total isoflavones",
    'biochanin_a' => "Biochanin A",
    'formononetin' => "Formononetin",
    'coumestrol' => "Coumestrol",
    'cyanidin' => "Cyanidin",
    'proanthocyanidin_monomers' => "Proanthocyanidin monomers",
    'proanthocyanidin_dimers' => "Proanthocyanidin dimers",
    'proanthocyanidin_trimers' => "Proanthocyanidin trimers",
    'proanthocyanidin_4_6mers' => "Proanthocyanidin 4-6mers",
    'proanthocyanidin_7_10mers' => "Proanthocyanidin 7-10mers",
    'proanthocyanidin_polymers_10mers' => "Proanthocyanidin polymers (>10mers)",
    'petunidin' => "Petunidin",
    'delphinidin' => "Delphinidin ",
    'malvidin' => "Malvidin",
    'pelargonidin' => "Pelargonidin",
    'peonidin' => "Peonidin",
    'catechin' => "(+)-Catechin",
    'epigallocatechin' => "(-)-Epigallocatechin",
    'epicatechin' => "(-)-Epicatechin",
    'epicatechin_3_gallate' => "(-)-Epicatechin 3-gallate",
    'epigallocatechin_3_gallate' => "(-)-Epigallocatechin 3-gallate",
    'theaflavin' => "Theaflavin",
    'thearubigins' => "Thearubigins",
    'eriodictyol' => "Eriodictyol",
    'hesperetin' => "Hesperetin",
    'naringenin' => "Naringenin",
    'apigenin' => "Apigenin",
    'luteolin' => "Luteolin",
    'isorhamnetin' => "Isorhamnetin",
    'kaempferol' => "Kaempferol",
    'myricetin' => "Myricetin",
    'quercetin' => "Quercetin",
    'theaflavin_33_digallate' => "Theaflavin-3,3'-digallate",
    'theaflavin_3_gallate' => "Theaflavin-3'-gallate",
    'gallocatechin' => "(+)-Gallocatechin",
    'gallocatechin_3_gallate' => "(+)-Gallocatechin 3-gallate",
    '183_n_3_ccc_ala' => "18:3 n-3 c,c,c (ALA)",
    '203_n_3' => "20:3 n-3",
    '203_n_6' => "20:3 n-6",
    '204_n_6' => "20:4 n-6",
    '183i' => "18:3i",
    '215' => "21:5",
    '224' => "22:4",
    '181_11_t_181t_n_7' => "18:1-11 t (18:1t n-7)",
];
