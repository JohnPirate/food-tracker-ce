<?php

return [
    'catalogue' => [
        'word' => 'Lebensmittel Katalog',
    ],
    'consumed_foods' => [
        'word' => 'Tagebuch',
    ],
    'consumed_food' => [
        'word' => 'Tagebuch',
    ],
    'food' => [
        'word' => 'Lebensmittel',
    ],
    'consumed_group' => [
        'word' => 'Konsumierte Gruppe'
    ],
    'group' => [
        'word' => 'Gruppe'
    ],
    'nutrients' => [
        'word' => 'Nährstoffe',
    ],
    'nutrient' => [
        'word' => 'Nährstoff',
    ],
    'nutrition_facts' => [
        'word' => 'Nährwertangaben',
    ],
    'source' => [
        'word' => 'Quelle',
    ],
    'usda' => [
        'word' => 'USDA',
    ],
    'usda_data_central' => [
        'word' => 'USDA - FoodData Central',
    ],
    'template' => [
        'word' => 'Vorlage',
    ],
    'ingredient' => [
        'word' => 'Zutat',
    ],
];
