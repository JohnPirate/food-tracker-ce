<?php

return [
    'reset_password' => [
        'title' => 'Reset Password',
    ],
    'password_update' => [
        'title' => 'Reset Password',
    ],
    'verify_email' => [
        'title' => 'Verify Your Email Address',
        'text' => [
            'verify_1' => 'Before proceeding, please check your email for a verification link.',
            'verify_2' => 'If you did not receive the email',
            'verify_3' => 'click here to request another',
        ],
    ],
    'food_catalouge' => [
        'title' => 'Catalogue',
    ],
    'consume_overview' => [
        'title' => 'Journal',
    ],
    'food_new' => [
        'title' => 'New Food',
    ],
    'food_edit' => [
        'title' => 'Edit Food',
    ],
    'settings' => [
        'title' => 'Settings',
        'block_delete_account' => [
            'title' => 'Delete Account',
            'text' => [
                'general' => 'All your data will be removed.',
            ],
            'subtitle' => [
                'opt' => 'Optional',
            ],
        ],
        'block_save_user_data' => [
            'title' => 'Personal Data',
            'text' => [
                'general' => 'Here you can change your personal data.',
            ],
        ],
    ],
    'templates_overview' => [
        'title' => 'Templates Overview',
    ],
    'template_new' => [
        'title' => 'New Template',
    ],
    'template_edit' => [
        'title' => 'Edit Template',
    ],
    'template_consume' => [
        'title' => 'Consume from :name',
        'breadcrumb' => 'Consume template',
        'text' => [
            'default_portion' => 'Template default portions',
        ],
    ],
    'template_show' => [
        'subtitles' => [
            'meta' => 'Meta Data',
            'ingredients' => 'Ingredients',
            'nutrients' => 'Nutrients',
        ],
        'text' => [
            'portions' => 'Portions',
            'description' => 'Description',
            'edited' => 'Edited',
        ],
    ],
    'dashboard' => [
        'title' => 'Dashboard',
    ],
];
