<?php

return [
    'email' => [
        'title' => 'Email',
        'placeholder' => 'Enter email address',
        'aria_label' => 'Enter email address.',
    ],
    'username' => [
        'title' => 'Username',
        'placeholder' => 'Enter username',
        'aria_label' => 'Enter username.',
    ],
    'password' => [
        'title' => 'Password',
        'placeholder' => 'Enter password',
        'aria_label' => 'Enter password.',
    ],
    'password_confirmation' => [
        'title' => 'Confirm password',
        'placeholder' => 'Confirm password',
        'aria_label' => 'Confirm password.',
    ],
    'remember' => [
        'title' => 'Remember me',
        'aria_label' => 'Remember me.',
    ],
    'agree_terms' => [
        'title' => 'I accept the Terms of Service and Privacy Policy.',
        'aria_label' => 'Terms of Service and Privacy Policy.',
    ],
    'search_foods' => [
        'placeholder' => 'Search for food',
        'placeholder_instant_search' => 'Search for food...',
        'aria_label' => 'Search for food.',
    ],
    'consumed_at' => [
        'title' => 'Consumed at',
        'aria_label' => 'Consumed at.',
    ],
    'food_groups' => [
        'title' => 'Possible Groups',
        'aria_label' => 'Select a consumed food group.',
        'description' => 'Select a group, type a new one or let it empty.',
        'placeholder' => 'Select group',
        'tag_placeholder' => 'Add this as new tag',
        'select_label' => 'Press enter to select',
        'select_group_label' => 'Press enter to select group',
        'selected_label' => 'Selected',
        'deselect_label' => 'Press enter to remove',
        'deselect_group_label' => 'Press enter to deselect group',
        'opt_groups' => [
            'used_groups' => 'Used groups',
            'other_groups' => 'Other Groups',
            'groups' => 'Groups',
        ],
    ],
    'consumed_value' => [
        'title' => 'Consumed value',
        'aria_label' => 'Consumed value.',
    ],
    'date_from' => [
        'title' => 'Select date',
        'aria_label' => 'Select a date.',
    ],
    'select_time' => [
        'title' => 'Select time',
        'aria_label' => 'Select time.',
    ],
    'copy_consumed_group_at' => [
        'title' => 'Copy to',
        'aria_label' => 'Copy group to date.',
    ],
    'food_name' => [
        'title' => 'Name',
        'aria_label' => 'Name of the food.',
    ],
    'food_brand' => [
        'title' => 'Brand',
        'aria_label' => 'Brand of the food.',
    ],
    'food_barcode' => [
        'title' => 'Barcode',
        'aria_label' => 'Barcode.',
    ],
    'food_description' => [
        'title' => 'Description',
        'aria_label' => 'Description of the food.',
    ],
    'food_private' => [
        'title' => 'Is private?',
        'aria_label' => 'Mark the food as private.',
    ],
    'food_ref_value' => [
        'title' => 'Reference Value',
        'aria_label' => 'Reference value of the food.',
    ],
    'food_ref_unit' => [
        'title' => 'Reference Unit',
        'aria_label' => 'Reference unit of the food.',
    ],
    'food_carb_without_fiber' => [
        'title' => 'The carbohydrates value is without fiber',
        'aria_label' => 'The carbohydrates value is without fiber.',
    ],
    'nutrient_name' => [
        'title' => 'Nutrient',
        'aria_label' => 'Name of the Nutrient',
        'description' => 'Select a group, type a new one or let it empty.',
        'placeholder' => 'Pick a nutrient',
        'tag_placeholder' => 'Add this as new tag',
        'select_label' => 'Press enter to select',
        'select_group_label' => 'Press enter to select group',
        'selected_label' => 'Selected',
        'deselect_label' => 'Press enter to remove',
        'deselect_group_label' => 'Press enter to deselect group',
    ],
    'nutrient_value' => [
        'title' => 'Value',
        'aria_label' => 'Value of the nutrient reference.',
    ],
    'nutrient_unit' => [
        'title' => 'Unit',
        'aria_label' => 'Unit of the nutrient reference.',
    ],
    'dont_remove_public_foods' => [
        'title' => 'Do not remove my public food',
        'aria_label' => 'Do not remove my public food.',
        'help' => 'If you confirm this option your public food will not be removed. Other members of this platform are able to find and use your food after your account is removed.',
    ],
    'template_name' => [
        'title' => 'Name',
        'title2' => 'Name of the Template',
        'aria_label' => 'Name of the Template.',
    ],
    'template_description' => [
        'title' => 'Description',
        'aria_label' => 'Description of the Template.',
    ],
    'template_group' => [
        'title' => 'Group name',
        'aria_label' => 'Group name under which the foods are grouped.',
        'help' => 'Use your own group name if the template name doesn\'t match the current selection.',
    ],
    'template_portion' => [
        'title' => 'Portion(s)',
        'aria_label' => 'Number of portions.',
        'help' => 'Number of portions this template contains.',
    ],
    'template_portion_consume' => [
        'title' => 'Consume Portions',
        'aria_label' => 'Number of portions to consume.',
        'help' => 'How many servings do you want to consume?',
    ],
    'ingredient_value' => [
        'title' => 'Value',
        'aria_label' => 'Value of the ingredient reference in :unit.',
    ],
    'search_templates' => [
        'placeholder' => 'Search for templates',
        'aria_label' => 'Search for templates.',
    ],
    'templates' => [
        'title' => 'Template',
        'aria_label' => 'Select a template.',
        'placeholder' => 'Select template',
        'select_label' => 'Press enter to select',
        'select_template_label' => 'Press enter to select template',
        'selected_label' => 'Selected',
        'deselect_label' => 'Press enter to remove',
        'deselect_template_label' => 'Press enter to deselect template',
    ],
    'template_food_value' => [
        'title' => 'Value',
        'aria_label' => 'Food value for template.',
    ],
];
