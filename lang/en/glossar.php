<?php

return [
    'catalogue' => [
        'word' => 'Catalogue',
    ],
    'consumed_foods' => [
        'word' => 'Consumed Food',
    ],
    'consumed_food' => [
        'word' => 'Consumed Food',
    ],
    'food' => [
        'word' => 'Food',
    ],
    'consumed_group' => [
        'word' => 'Consumed Group'
    ],
    'group' => [
        'word' => 'Group'
    ],
    'nutrients' => [
        'word' => 'Nutrients',
    ],
    'nutrient' => [
        'word' => 'Nutrient',
    ],
    'nutrition_facts' => [
        'word' => 'Nutrition facts',
    ],
    'source' => [
        'word' => 'Source',
    ],
    'usda' => [
        'word' => 'USDA',
    ],
    'usda_data_central' => [
        'word' => 'USDA - FoodData Central',
    ],
    'template' => [
        'word' => 'Template',
    ],
    'ingredient' => [
        'word' => 'Ingredient',
    ],
];
