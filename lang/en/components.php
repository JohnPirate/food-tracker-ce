<?php

return [
    /*
    |---------------------------------------------------------------------------
    | Buttons
    |---------------------------------------------------------------------------
    */
    'button' => [
        'new' => [
            'text' => 'New',
            'title' => 'Create new :action.',
        ],
        'save' => [
            'text' => 'Save',
            'title' => 'Save :action.',
        ],
        'add' => [
            'text' => 'Add',
            'title' => 'Add :action.',
        ],
        'create' => [
            'text' => 'Create',
            'title' => 'Create :action.',
        ],
        'edit' => [
            'text' => 'Edit',
            'title' => 'Edit :action.',
        ],
        'back' => [
            'text' => 'Back',
            'title' => 'back.',
        ],
        'abort' => [
            'text' => 'Abort',
            'title' => 'Abort :action.',
        ],
        'cancel' => [
            'text' => 'Cancel',
            'title' => 'Cancel :action.',
        ],
        'remove' => [
            'text' => 'Remove',
            'title' => 'Remove :action.',
            'title_confirm' => 'Confirm remove :action.',
        ],
        'delete' => [
            'text' => 'Delete',
            'title' => 'Delete :action.',
            'title_confirm' => 'Confirm delete :action.',
        ],
        'reset' => [
            'text' => 'Reset',
            'title' => 'Reset :action.',
        ],
        'show' => [
            'text' => 'Show',
            'title' => 'Show :action.',
        ],
        'sign_in' => [
            'text' => 'Sign in',
            'title' => 'Sign in with your :appname account.',
        ],
        'register' => [
            'text' => 'Register',
            'title' => 'Create a new :appname account.',
        ],
        'send_password_reset_link' => [
            'text' => 'Send Password Reset Link',
            'title' => 'Send Password Reset Link.',
        ],
        'password_reset' => [
            'text' => 'Reset Password',
            'title' => 'Reset Password.',
        ],
        'new_food' => [
            'text' => 'New Food',
            'title' => 'Create new Food.',
        ],
        'consume' => [
            'text' => 'Consume',
            'text2' => 'Consume food',
            'title' => 'Consume Food.',
        ],
        'consume_from_template' => [
            'text' => 'Consume from Template',
            'title' => 'Consume from Template.',
        ],
        'add_to_favorites' => [
            'text' => 'Add to favorites',
            'title' => 'Add :something to favorites.',
        ],
        'go_to_date' => [
            'text' => 'Go to date',
            'title' => 'Go to selected date.',
        ],
        'today' => [
            'text' => 'Today',
            'title' => 'Go to today.',
        ],
        'copy_group' => [
            'text' => 'Copy',
            'title' => 'Copy group.'
        ],
        'create_template' => [
            'text' => 'Create template',
            'title' => 'Create template from group.'
        ],
        'copy_group_action' => [
            'text' => 'Copy group',
            'title' => 'Copy group.'
        ],
        'create_template_from_food_grouping' => [
            'text' => 'Create template',
            'title' => 'Create template.'
        ],
        'dont_remove_public_foods' => [
            'text' => 'Delete Account',
            'title' => 'Delete your account.'
        ],
        'new_template' => [
            'text' => 'New Template',
            'text_action' => 'Create a new Template',
            'title' => 'Create a new Template.',
        ],
        'copy_template' => [
            'text' => 'Copy',
            'title' => 'Copy template.'
        ],
        'add_food_to_template' => [
            'text' => 'Add Food',
            'text_mnu' => 'Add to Template',
            'text_action' => 'Add Food to Template',
            'title' => 'Add Food to Template.'
        ],
        'edit_template_to_add_food' => [
            'text' => 'Edit Template to add Food',
            'title' => 'Edit Template to add Food.'
        ],
        'prev_day' => [
            'text' => 'Prev Day',
            'title' => 'Go to previous day.'
        ],
        'next_day' => [
            'text' => 'Next Day',
            'title' => 'Go to next day.'
        ],
        'update_personal_data' => [
            'text' => 'Save Personal Data',
            'title' => 'Save your personal data.',
        ],
    ],

    /*
    |---------------------------------------------------------------------------
    | States
    |---------------------------------------------------------------------------
    */
    'states' => [
        'no_foods_found' => [
            'title' => 'No Food found',
            'message' => [
                'db_empty' => 'There is no food stored in database.',
                'nothing_found' => 'There is no food found for search term \':query\'',
            ],
        ],
        'nothing_consumed' => [
            'title' => 'Nothing Consumed',
            'message' => 'You have nothing consumed today.',
        ],
        'no_templates_found' => [
            'title' => [
                'db_empty' => 'No templates',
                'nothing_found' => 'No Templates found',
            ],
            'message' => [
                'db_empty' => 'No template has been created yet.',
                'nothing_found' => 'There are no templates found for search term \':query\'',
            ],
        ],
        'no_ingredients' => [
            'title' => 'No Ingredients',
            'message' => 'This template does not have any ingredients yet.',
        ],
        'no_foods' => [
            'title' => [
                'no_query' => 'Search for Food',
                'nothing_found' => 'No Food found',
            ],
            'message' => [
                'no_query' => 'Use the search box to search for food',
                'nothing_found' => 'There is no food found for search term \':query\'',
            ],
        ],
    ],

    /*
    |---------------------------------------------------------------------------
    | Modals
    |---------------------------------------------------------------------------
    */
    'modals' => [
        'consume_food_insert_update_modal' => [
            'header' => [
                'new' => 'Store new entry',
                'edit' => 'Update entry',
            ],
            'cancel' => [
                'new' => 'new entry',
                'edit' => 'update',
            ],
        ],
        'copy_consumed_foods_group_modal' => [
            'header' => 'Copy group',
        ],
        'delete_consumed_food' => [
            'message' => 'Do you really want to delete this entry?',
        ],
        'delete_consumed_food_group' => [
            'message' => 'Do you really want to delete the entire grouping?',
        ],
        'delete_food' => [
            'message' => 'Do you really want to delete this food?',
        ],
        'dont_remove_public_foods' => [
            'message' => 'Do you really want to delete your account?',
        ],
        'search_foods' => [
            'header' => 'Food Search',
        ],
        'delete_template' => [
            'message' => 'Do you really want to delete this template?',
        ],
        'copy_template' => [
            'message' => 'Do you really want to create a copy of this template?',
        ],
        'add_food_to_template' => [
            'header' => 'Add Food to Template',
        ],
        'create_template_from_food_grouping' => [
            'header' => 'Create template',
        ],
    ],

    /*
    |---------------------------------------------------------------------------
    | Vue
    |---------------------------------------------------------------------------
    */
    'vue' => [
        'food_meta_form' => [
            'title' => 'Food Meta Data',
        ],
        'template_form' => [
            'title1' => 'Template Meta Data',
            'title2' => 'Food',
        ],
        'frequently_consumed_foods' => [
            'title' => 'Frequently consumed foods',
            'info' => 'The following foods have been consumed most frequently in the past 30 days',
            'btn' => 'Looking for more foods',
        ],
    ],
];
