<?php

return [
    'internal_error' => 'Sorry, an internal error occurred. Please contact the support team.',
    'not_found' => 'Record not found.',
    'authentication_expired' => 'Your session is expired. Please sign in again.',
    'not_authenticated' => 'You do not have an active session. Please log in.',
    'not_authorized' => 'Missing authorizations.',
    'not_allowed_to_delete_food' => 'You are not allowed to edit this food.',
    'fresh_verification_link_has_been_sent' => 'A fresh verification link has been sent to your email address.',
    'no_food_for_id' => 'No food found for ID :id.',
    'no_consumed_data_for_id' => 'No consumed data found for ID :id.',
    'no_group_for_id' => 'No group found for ID :id.',
    'no_food_ownership' => 'You are not the owner of the food.',
    'food_already_exists_as_ingredient' => ':name already exists as ingredient.',
    'success_store' => 'Successful stored.',
    'success_update' => 'Successful updated.',
    'success_deleted' => 'Successful deleted.',
    'success_copied' => 'Successful copied.',
    'success_consumed' => 'Successful consumed.',
    'success_food_added_to_template' => 'Successful added Food to Template.',
    'success_store_data' => ':data successful stored.',
    'success_update_data' => ':data successful updated.',
    'success_deleted_data' => ':data successful deleted.',
    'success_delete_group_entries' => 'Grouping deleted successfully.',
    'success_create_template_from_food_grouping' => 'Template successful created.',

    // Template
    'ask_to_delete_template' => 'Are you sure you want to delete this template?',
];
