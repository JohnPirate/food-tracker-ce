<?php

return [
    'toggle_navigation' => 'Toggle navigation',
    'branded_food' => 'This food has a brand.',
    'imported_food' => 'This food is imported from an external source.',
    'no_group' => 'Consumed food',
    'no_group_2' => 'More food',
    'form' => 'Form',
    'copy' => 'Copy',
    'created_at' => 'Created at',
    'updated_at' => 'Updated at',
    'change_translation' => 'Change language to :name',
    'consumed_calories' => 'Consumed calories',
    'last_meal_ago' => 'Last meal ago',
    'macro_nutrients' => 'Macro nutrients',
    'info' => 'Info',
    'weekdays' => [
        'first_char' => [
            'M', 'T', 'W', 'T', 'F', 'S', 'S',
        ],
        'short' => [
            'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun',
        ],
    ],

    'weekly_overview' => 'Weekly overview',
    'nutrition_facts' => [
        'simple' => 'Simple',
        'full' => 'Full',
    ],
];
