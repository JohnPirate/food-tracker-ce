<?php

return [
    'register' => [
        'text' => 'Register',
        'title' => 'Create new account.',
    ],
    'sign_in' => [
        'text' => 'Sign in',
        'title' => 'Sign in.',
    ],
    'sign_out' => [
        'text' => 'Sign out',
        'title' => 'Sign out.',
    ],
    'forgot_password' => [
        'text' => 'Forgot your password?',
        'title' => 'Here you can reset your password.',
    ],
    'reset_password' => [
        'text' => 'Reset Password',
        'title' => 'Here you can reset your password.',
    ],
    'terms_and_policy' => [
        'text' => 'Terms and Policy',
        'title' => 'Terms and Policy.',
    ],
    'settings' => [
        'text' => 'Settings',
        'title' => 'Account settings.',
    ],
    'catalogue' => [
        'text' => 'Food',
        'title' => 'Go to the food catalogue.',
    ],
    'consumed_foods' => [
        'text' => 'Journal',
        'title' => 'Go to the Journal overview.',
    ],
    'food_detail_view' => [
        'text' => 'Detail view',
        'title' => 'Go to the food detail view.',
    ],
    'repo' => [
        'text' => 'Gitlab',
        'title' => 'Go to the Gitlab repository.',
    ],
    'licence' => [
        'text' => 'MIT',
        'title' => 'Go to the application licence.',
    ],
    'go_to_source' => [
        'title' => 'Go to source.',
    ],
    'verification_resend' => [
        'title' => 'A fresh verification link will be sent to your email address.',
    ],
    'templates_overview' => [
        'text' => 'Templates',
        'title' => 'Go to the food templates overview.',
    ],
    'template_detail_view' => [
        'text' => 'Detail view',
        'title' => 'Go to the template detail view.',
    ],
];
