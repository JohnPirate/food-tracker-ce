#!/bin/ash

echo "Wait for mysql setup 10s ..."
sleep 10
echo "OK"

# Create food-tracker schema
echo "Create food-tracker schema... $(mysql -uroot -p$MYSQL_ROOT_PASSWORD --host=db -e "create schema if not exists foodtracker;" && echo "OK" || echo "FAIL")"

# Start migrations
# php artisan migrate:refresh --seed -v

/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
