<?php

return [
    'default_amount' => 'g',
    'default_energy' => 'kcal',
    'units' => [
        'amount' => [
            'g' => 1.0,
            'mg' => 0.001,
            'µg' => 0.000001,
            'ng' => 0.000000001,
            'ml' => 1.0,
        ],
        'energy' => [
            'kcal' => 1.0,
            'kj' => 4.187,
        ]
    ],
    'groups' => [
        'proximates',
        'others',
        'minerals',
        'vitamins',
        'aminos',
        'lipids',
        'isoflavones',
    ],
    'proximates' => [
        'protein',
        'carb',
        'fat',
    ],
    'default_nutrients' => [
        203, 204, 205, 208, 269, 291,
    ],
];
