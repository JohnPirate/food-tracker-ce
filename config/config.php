<?php

return [
    'consume_datetime_format' => 'Y-m-d H:i',
    'date_format' => 'Y-m-d',
];
